from matplotlib import pyplot as plt
import numpy as np
from frecce import add_arrow    #funzione per fare le frecce

rS=1
r = np.linspace(0,3,6000)

for i in range (-10,15,1):
    x0p = r + 2*rS*np.log(1-r/rS) + i
    x0m = - r + i
    linep = plt.plot(r, x0p, color='royalblue')[0]
    linem = plt.plot(r, x0m, color='blueviolet')[0]     #color='blueviolet' 'slateblue'
    add_arrow(linep, position=0.5, direction='left', size=15, color=None, arrsty='fancy')
    add_arrow(linem, position=1.7, direction='left', size=15, color=None, arrsty='fancy')
plt.xlim(0,3)
plt.ylim(-5,5)
plt.xlabel('r')
plt.ylabel('$x_0$')
plt.grid()
plt.show()
