from matplotlib import pyplot as plt
import numpy as np

rS=1.5
e1=2.55
e2=2.75
e3=2.95
r = np.linspace(0.5,5,5000)
k1 = (e1/np.exp(r))**2-(rS/(np.exp(r)-rS))
k2 = (e2/np.exp(r))**2-(rS/(np.exp(r)-rS))
k3 = (e3/np.exp(r))**2-(rS/(np.exp(r)-rS))
plt.plot(r,k1, color='royalblue')
plt.plot(r,k2, color='royalblue')
plt.plot(r,k3, color='royalblue')
#plt.xlim(0,3)
plt.ylim(-0.8,0.4)
plt.xlabel('$r$')
plt.ylabel('$U(r)$')
#plt.legend((k1, k2, k3), ('label1', 'label2', 'label3'))
plt.grid()
plt.show()
