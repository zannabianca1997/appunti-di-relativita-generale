from matplotlib import pyplot as plt
import numpy as np

xi=1
eta = np.linspace(0,2*np.pi,7000)
k1 = (xi/2)*(1-np.cos(eta))
k2 = (xi/4)*eta**2
k3 = (xi/2)*(np.cosh(eta)-1)
plt.plot(eta,k1)
plt.plot(eta,k2)
plt.plot(eta,k3)
#plt.xlim(0,3)
plt.ylim(-0.2,3.5)
plt.xlabel('$\eta$')
plt.ylabel('$a(\eta)$')
#plt.legend((k1, k2, k3), ('label1', 'label2', 'label3'))
plt.grid()
plt.show()
