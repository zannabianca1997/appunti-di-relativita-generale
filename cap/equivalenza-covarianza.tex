\chapter[Equivalenza e covarianza]{Equivalenza, covarianza e loro conseguenze}

Le equazioni della fisica devono essere scritte in forma covariante, cioè essere invarianti in forma in qualsiasi sistema di coordinate. Per il principio di equivalenza devono ricondursi alle leggi della relatività ristretta se scritte in un sistema localmente inerziale. Queste due condizioni, aggiunte ad un principio di "minimalità", sono spesso sufficienti e si concretizzano nella semplice ricetta di "sostituire le derivate con le derivate covarianti". Il risultato però non è per nulla banale: contiene tutte le interazioni con la metrica, cioè con il campo gravitazionale.


\section{Equazione geodetica}
\subsection{Particelle libere}
Partiamo dal caso più semplice: la legge del moto delle particelle libere (libere da tutto ma non dalla gravità).

\subsubsection{Derivazione dal trasporto parallelo}
In relatività ristretta l'equazione del moto è
\[\frac{du^\mu}{ds}=0 \text{,\quad dove\quad} u^\mu=\frac{dx^\mu}{ds} \text{\quad e \quad } ds=\sqrt{dx^\mu dx_\mu}\]
Quindi il vettore $u^\mu$ è "trasportato parallelamente" (cioè rimane costante nel sistema inerziale) lungo la traiettoria stessa. La forma covariante è quindi
\[u^\nu D_\nu u^\mu=0 \qquad \text{dove} \quad ds=\sqrt{g_{\mu\nu}dx^\mu dx^\nu}\]
Possiamo anche scriverla come
\[0=\frac{dx^\nu}{ds}\left(\partial_\nu u^\mu+\Gamma^\mu_{\nu\lambda}u^\lambda\right)=\frac{d^2x^\mu}{ds^2}+\Gamma^\mu_{\nu\lambda}\frac{dx^\nu}{ds}\frac{dx^\lambda}{ds}\]
quindi $-m\Gamma^\mu_{\nu\lambda}\frac{dx^\nu}{ds}\frac{dx^\lambda}{ds}$ si può interpretare come una \textit{quadri-forza gravitazionale}.

\subsubsection{Derivazione dal principio d'azione}
Possiamo riottenere la stessa equazione in un altro modo che rende manifesta la natura geometrica delle soluzioni. L'equazione del moto in relatività ristretta si ottiene anche partendo da un'azione. La versione covariante è
\[\S=-mc\int ds \text{\quad con \quad} ds=\sqrt{g_{\mu\nu}dx^\mu dx^\nu}\]
essendo $\S$ uno scalare le equazioni saranno automaticamente covarianti.
\[\S=-mc\int ds\sqrt{g_{\mu\nu}\frac{dx^\mu}{ds}\frac{dx^\nu}{ds}}\]
\[\begin{split}-\frac{\var\S}{mc}&=\int ds\frac{1}{2}\frac{\var g_{\mu\nu}\frac{dx^\mu}{ds}\frac{dx^\nu}{ds}+2g_{\mu\nu}\frac{dx^\mu}{ds}\var\frac{dx^\nu}{ds}}{\sqrt{g_{\mu\nu}u^\mu u^\nu}}=\\&=\int ds\,\frac{1}{2}\qty\Bigg(\frac{\p g_{\mu\nu}}{\p x^\rho}\var x^\rho\frac{dx^\mu}{ds}\frac{dx^\nu}{ds}-2\frac{d}{ds}\underbrace{\qty(g_{\mu\nu}\frac{dx^\mu}{ds})\var x^\nu}_{\nu\ra\rho})=\\&
        =\int ds\,\qty(\frac{\p g_{\mu\nu}}{\p x^\rho}\frac{dx^\mu}{ds}\frac{dx^\nu}{ds}-2\frac{\p g_{\mu\rho}}{\p x^\lambda}\frac{dx^\lambda}{ds}\frac{dx^\mu}{ds}-2g_{\mu\rho}\frac{d^2x^\mu}{ds^2})\var x^\rho\end{split}\]
dove nella seconda uguaglianza abbiamo usato che $g_{\mu\nu}u^\mu u^\nu=1$ e integrato per parti, nella terza uguaglianza abbiamo invece rinominato $\nu\ra\rho$ nel secondo termine in parentesi. Otteniamo quindi l'equazione del moto
\[g_{\mu\rho}\frac{d^2x^\mu}{ds^2}+\frac{\p g_{\mu\rho}}{\p x^\lambda}\frac{dx^\lambda}{ds}\frac{dx^\mu}{ds}-\frac{1}{2}\frac{\p g_{\mu\nu}}{\p x^\rho}\frac{dx^\mu}{ds}\frac{dx^\nu}{ds}=0\]
moltiplichiamo per $g^{\gamma\rho}$
\[\frac{d^2x^\gamma}{ds^2}+g^{\gamma\rho}\underbrace{\p_\lambda g_{\mu\rho}\frac{dx^\lambda}{ds}\frac{dx^\mu}{ds}}_{(1)}-\frac{1}{2}g^{\gamma\rho}\p_\rho \underbrace{g_{\mu\nu}\frac{dx^\mu}{ds}\frac{dx^\nu}{ds}}_{\nu\ra\lambda}=0\]
$(1)$: $\frac{1}{2}\left(\p_\lambda g_{\mu\rho}+\p_\mu g_{\lambda\rho}\right)u^\lambda u^\mu$. Quindi arriviamo alla forma finale:
\[\frac{d^2x^\gamma}{ds^2}+\underbrace{g^{\gamma\rho}\frac{1}{2}\qty\Big(\partial_\lambda g_{\mu\rho}+\partial_\mu g_{\lambda\rho}-\partial_\rho g_{\mu\lambda})}_{=\Gamma^\gamma_{\mu\lambda}}\frac{dx^\mu}{ds}\frac{dx^\lambda}{ds}=0\]
Riotteniamo quindi la stessa equazione che avevamo ricavato con il metodo precedente.

\subsubsection{Soluzioni: geodetiche}
Le soluzioni sono anche chiamate \textit{geodetiche}. Abbiamo usato $s$, cioè l'intervallo, per parametrizzare la traiettoria. Se usiamo un'altra funzione $s'(s)$ l'equazione non rimane invariata, a meno che $s'=\alpha s+\beta$ con $\alpha$ e $\beta$ costanti. Un parametro $\lambda$ per cui vale l'\textit{equazione geodetica} nella forma
\begin{equation}
    \boxed{\frac{d^2x^\mu}{d\lambda^2}+\Gamma^\mu_{\nu\rho}\frac{dx^\nu}{d\lambda}\frac{dx^\rho}{d\lambda}=0}
\end{equation}
è detto \textit{parametro affine}. Date le condizioni iniziali $x^\mu(\lambda_0)$ e $\frac{dx^\mu}{d\lambda}(\lambda_0)$ la soluzione e il corrispondente parametro affine sono univocamente determinati.


\subsection{Limite non relativistico}
Vediamo ora il limite non relativistico dell'equazione geodetica
\[u^\mu=\frac{dx^\mu}{ds}=\left(\gamma,\gamma\frac{\vec{v}}{c}\right)\simeq\left(1+\order{\frac{v^2}{c^2}},\frac{\vec{v}}{c}\left(1+\order{\frac{v^2}{c^2}}\right)\right)\]
\[\frac{du^\mu}{ds}=\frac{d^2x^\mu}{ds^2}\simeq\left(0,\frac{\dot{\vec{v}}}{c^2}\right)\]
quindi il termine dominante è $\frac{\dot{v}^i}{c^2}=-\Gamma^i_{00}$ e $-c^2\Gamma^i_{00}$ è l'\textit{accelerazione gravitazionale}.
In teoria newtoniana $\dot{v}^i=-\nabla_i\phi$ quindi dobbiamo identificare $\nabla_i\phi=c^2\Gamma^i_{00}$. Nel limite non relativistico anche la metrica deve discostarsi poco da quella di Minkowski
\[g_{\mu\nu}=\eta_{\mu\nu}+h_{\mu\nu} \qquad \text{con} \quad |h_{\mu\nu}|\ll1\]
\[\Gamma^i_{00}=\frac{1}{2}g^{i0}\partial_0g_{00}+\frac{1}{2}g^{ij}(2\partial_0g_{j0}-\partial_jg_{00})\simeq\frac{1}{2}\partial_ih_{00}\]
Quindi abbiamo
\begin{equation}
    \boxed{g_{00}=1+h_{00}\simeq1+\frac{2\phi}{c^2}}
\end{equation}
Questa è la relazione fra metrica e il potenziale newtoniano nel limite non relativistico.

Nota che ora abbiamo un'interpretazione molto diversa della forza gravitazionale. Essa è infatti un'"illusione", una conseguenza della scelta del sistema di coordinate in cui $\Gamma\sim\partial g\neq0$. Con un cambio di coordinate opportune questa forza si può cancellare.


\section[Elettromagnetismo]{Elettromagnetismo in una metrica generica}
Ora consideriamo le equazioni dell'elettromagnetismo in una metrica $g_{\mu\nu}$ generica. Il tensore dei campi è
\[F_{\mu\nu} = D_\mu A_\nu - D_\nu A_\mu = \partial_\mu A_\nu - \Gamma^\lambda_{\mu\nu}A_\lambda - \partial_\nu A_\mu + \Gamma^\lambda_{\nu\mu}A_\lambda = \partial_\mu A_\nu - \partial_\nu A_\mu\]
siccome $\Gamma^\lambda_{\mu\nu}=\Gamma^\lambda_{\nu\mu}$. Questa particolare operazione di derivazione che porta un covettore $A_\mu$ in un tensore $(0,2)$ antisimmetrico $\partial_\mu A_\nu - \partial_\nu A_\mu$ è ben definita e non dipende dalla connessione o dalla metrica.


\subsection{Equazioni di Maxwell}
Le equazioni di Maxwell senza sorgenti sono
\[\begin{split}&D_\mu F_{\nu\rho}+D_\nu F_{\rho\mu}+D_\rho F_{\mu\nu}=\p_\mu F_{\nu\rho}-\Gamma^\lambda_{\mu\nu}F_{\lambda\rho}-\Gamma^\lambda_{\mu\rho}F_{\nu\lambda}+\\&+\p_\nu F_{\rho\mu}-\Gamma^\lambda_{\nu\rho}F_{\lambda\mu}-\Gamma^\lambda_{\nu\mu}F_{\rho\lambda}+\p_\rho F_{\mu\nu}-\Gamma^\lambda_{\rho\mu}F_{\lambda\nu}-\Gamma^\lambda_{\rho\nu}F_{\mu\lambda}=0\end{split}\]
usando l'antisimmetria di $F_{\mu\nu}$
\[=\p_\mu F_{\nu\rho}+\p_\nu F_{\rho\mu}+\p_\rho F_{\mu\nu}=0\]
Anche questa operazione di derivata che porta un $(0,2)$ antisimmetrico in uno $(0,3)$ antisimmetrico non dipende dalla metrica.


\subsubsection{Forme differenziali e derivata esterna}
Un tensore $(0,j)$ completamente antisimmetrico si chiama anche \textit{forma differenziale}. Una funzione è una $0$-forma, un covettore è una $1$-forma. In $4$ dimensioni possiamo avere al massimo $4$-forme\footnote{se avessimo $5$ indici allora almeno $2$ di questi devono essere uguali, ma andandoli a scambiare almeno questo termine non cambierebbe segno e quindi l'oggetto non sarebbe completamente antisimmetrico}. Il tensore $\sqrt{-g}\epsilon_{\mu\nu\rho\gamma}$ è ad esempio una $4$-forma. L'operazione di \textit{derivata esterna} porta una $j$-forma in una $(j+1)$-forma
\[A_{\nu_1\ldots\nu_s}\longrightarrow\frac{1}{s+1}\sum_{\substack{\text{permutazioni}\\\text{cicliche }\gamma}} (-)^{\text{parità }(\gamma)}\partial_{\nu_{\gamma(1)}} A_{\nu_{\gamma(2)}\ldots\nu_{\gamma(s+1)}}\]
ad esempio
\[\begin{split}
        &f\ra\p_\nu f\\
        &A_{\nu_1}\ra\frac{1}{2}\left(\p_{\nu_1}A_{\nu_2}-\p_{\nu_2}A_{\nu_1}\right)\\
        &F_{\nu_1\nu_2}\ra\frac{1}{3}\left(\p_{\nu_1}F_{\nu_2\nu_3}+\p_{\nu_2}F_{\nu_3\nu_1}+\p_{\nu_3}F_{\nu_1\nu_2}\right)\\
        &H_{\nu_1\nu_2\nu_3}\ra\frac{1}{4}\left(\p_{\nu_1}H_{\nu_2\nu_3\nu_4}-\p_{\nu_2}H_{\nu_3\nu_4\nu_1}+\p_{\nu_3}H_{\nu_4\nu_1\nu_2}-\p_{\nu_4}H_{\nu_1\nu_2\nu_3}\right)\\
        &\ \ \vdots\end{split}\]
sono operazioni indipendenti dalla metrica e ben definite. Per dimostrarlo si fa come prima: si scrive l'operazione con la derivata covariante, poi per via dell'antisimmetria si dimostra che tutti i termini contenenti la connessione si cancellano.

La derivata esterna applicata due volte dà automaticamente $0$, $d^2=0$. Ad esempio
\[f\ra\p_\mu f\ra\frac{1}{2}\qty\Big(\p_\mu\p_\nu f-\p_\nu\p_\mu f)=0\]
\[\begin{split}
        A_\mu &\ra \frac{1}{2}\qty\Big(\p_\mu A_\nu-\p_\nu A_\mu)\ra\\&
        \ra \frac{1}{6}\qty\Big(\p_\rho\p_\mu A_\nu-\p_\rho\p_\nu A_\mu+\p_\mu\p_\nu A_\rho-\p_\mu\p_\rho A_\nu+\p_\nu\p_\rho A_\mu-\p_\nu\p_\mu A_\rho)=0\end{split}\]
Quindi nell'elettromagnetismo il potenziale $A_\mu$ è una $1$-forma, il campo $F_{\mu\nu}$ è la $2$-forma corrispondente e l'equazione $\p_\mu F_{\nu\rho}+\p_\nu F_{\rho\mu}+\p_\rho F_{\mu\nu}=0$ non è altro che $d^2=0$. Vediamolo usando il tensore duale
\[\tensor[^*]{F}{^{\mu\nu}}=\frac{1}{\sqrt{-g}}\epsilon^{\mu\nu\rho\gamma}F_{\rho\gamma}\]

\subsubsection{Equazioni di Maxwell senza sorgenti}
Abbiamo quindi che l'equazione è
\[D_\mu\tensor[^*]{F}{^{\mu\nu}}=0\]
usando la \ref{6.3} questa si può scrivere anche come
\[\frac{1}{\sqrt{-g}}\p_\mu\qty\big(\sqrt{-g}\tensor[^*]{F}{^{\mu\nu}})=0\]
il che è equivalente a
\[\epsilon^{\mu\nu\rho\gamma}\p_\mu F_{\rho\gamma}=0\]
e questa è proprio equivalente a
\[\p_\mu F_{\nu\rho}+\p_\nu F_{\rho\mu}+\p_\rho F_{\mu\nu}=0\]

\subsubsection{Equazioni di Maxwell con sorgenti}
Vediamo ora l'equazione con le sorgenti. In relatività ristretta (o nel sistema localmente inerziale) abbiamo $\p_\mu F^{\mu\nu}=\frac{4\pi}{c}J^\nu$. La forma covariante è
\[D_\mu F^{\mu\nu}=\frac{4\pi}{c}J^\nu\]
dove
\[F^{\mu\nu}=g^{\mu\rho}g^{\nu\gamma}F_{\rho\gamma}=g^{\mu\rho}g^{\nu\gamma}\qty\big(\p_\rho A_\gamma-\p_\gamma A_\rho)\]
siccome $D_\mu g_{\rho\gamma}=0$, possiamo anche riscriverla come
\[g^{\mu\rho}D_\mu F_{\rho\nu}=\frac{4\pi}{c}J_\nu \qquad \text{dove} \quad J_\nu=g_{\nu\lambda}J^\lambda\]
Un'altra forma equivalente è
\[D_\mu F^{\mu\nu}=\left(\p_\mu F^{\mu\nu}+\Gamma^\mu_{\mu\lambda}F^{\lambda\nu}+\Gamma^\nu_{\mu\lambda}F^{\mu\lambda}\right)=\frac{1}{\sqrt{-g}}\p_\mu\left(\sqrt{-g}F^{\mu\nu}\right)=\frac{4\pi}{c}J^\nu\]


\subsection{Principio d'azione}
\subsubsection{Equazione dei campi}
Vediamo ora come ottenere le equazioni dell'elettrodinamica da un principio di azione. La versione covariante dell'azione è:
\[\S_{\text{em}}+\S_{\text{int}}=\int\sqrt{-g}\,d\Omega\left(-\frac{1}{16\pi c}F^{\mu\nu}F_{\mu\nu}-\frac{1}{c^2}A_\mu J^\mu\right)\]
Nota che la metrica entra sia nella forma volume che in $F^{\mu\nu}$ per "alzare" gli indici. Facciamo la variazione rispetto $A_\mu$
\[\begin{split}\var\left(\S_{\text{em}}+\S_{\text{int}}\right)&=\int\sqrt{-g}\,d\Omega\left(\frac{1}{4\pi c}F^{\mu\nu}\var\p_\nu A_\mu-\frac{1}{c^2}J^\mu\var A_\mu\right)=\\&=\int d\Omega\left(-\frac{1}{4\pi c}\p_\nu\left(\sqrt{-g}F^{\mu\nu}\right)-\sqrt{-g}\frac{1}{c^2}J^\mu\right)\var A_\mu+\text{bordo}\end{split}\]
quindi otteniamo l'equazione
\[\p_\nu\left(\sqrt{-g}F^{\mu\nu}\right)=-\sqrt{-g}\frac{4\pi}{c}J^\mu\]
che è equivalente a quella scritta prima. L'azione completa per particelle puntiformi
\[\S=-\frac{1}{16\pi c}\int\sqrt{-g}\,d\Omega F^{\mu\nu}F_{\mu\nu}\underbrace{-\frac{q}{c}\int A_\mu\,dx^\mu-mc\int\sqrt{g_{\mu\nu}\,dx^\mu\,dx^\nu}}_{\text{sommato su tutte le particelle}}\]

\subsubsection{Equazione delle particelle}
Per trovare l'equazione delle particelle prendiamo quella in relatività ristretta scritta con $D_\mu$:
\[mcu^\rho D_\rho u^\mu=\frac{q}{c}g^{\mu\lambda}F_{\lambda\nu}u^\nu\]
che è equivalente a
\[mc\left(\frac{d^2x^\mu}{ds^2}+\Gamma^\mu_{\rho\gamma}\frac{dx^\rho}{ds}\frac{dx^\gamma}{ds}\right)=\frac{q}{c}g^{\mu\lambda}F_{\lambda\nu}\frac{dx^\nu}{ds}\]
Ora vogliamo riottenere la stessa equazione variando l'azione
\[\var\S=\var\S_{\text{mat}}+\var\S_{\text{int}}=\int ds\left(mc\left(\frac{d^2x^\mu}{ds^2}+\Gamma^\mu_{\lambda\gamma}\frac{dx^\lambda}{ds}\frac{dx^\gamma}{ds}\right)g_{\mu\rho}\var x^\rho-\frac{q}{c}F_{\mu\nu}\frac{dx^\nu}{ds}\var x^\mu\right)\]
da cui otteniamo proprio quella sopra.


\section{Conservazione delle correnti}
Studiamo ora le correnti e la loro conservazione in spazio curvo. Nel sistema in cui $g_{\mu\nu}=\eta_{\mu\nu}+\order{dx^2}$
\[N^\mu=\left(n\gamma,n\gamma\frac{\vec{v}}{c}\right)=nu^\mu\]
è la corrente per un fluido di particelle con velocità $u^\mu(x)$. $n$ è la densità nel sistema di quiete. $\p_\mu N^\mu=0$ esprime in forma locale la conservazione del numero di particelle. Quindi se $N^\mu$ è un vettore, in un sistema di riferimento generico $N'^\mu=\pdv{x'^\mu}{x^\nu}N^\nu$ dove $O'$ è il sistema localmente inerziale dove abbiamo definito $N^\mu$, e la conservazione diventa $D'_\mu N'^\mu=0$.
\[\int dS_\mu N^\mu=\int d\Omega\p_\mu N^\mu=0\]
esprime la conservazione in relatività ristretta. Ora l'elemento di volume è $\sqrt{-g}\dd{S_\mu}$ quindi abbiamo
\[\int\sqrt{-g}\dd{S_\mu}N^\mu=\int \dd{\Omega}\p_\mu\left(\sqrt{-g}N^\mu\right)=\int\sqrt{-g}\dd{\Omega}\underbrace{\frac{1}{\sqrt{-g}}\p_\mu\left(\sqrt{-g}N^\mu\right)}_{D_\mu N^\mu}=0\]
nota che $N^0$ non è la densità.

La densità $\undertilde{N}^0$ deve dare il numero di particelle integrato nel volume spaziale, cioè $\int\sqrt{\gamma}\dd{V}\undertilde{N}^0=\#_\text{particelle}$ dove $\gamma=\det\left[\gamma_{ij}\right]$ e $\gamma_{ij}=\frac{g_{0i}g_{0j}}{g_{00}}-g_{ij}$ è la metrica spaziale. Siccome $-g=g_{00}\gamma$, abbiamo
\[\#_{\text{particelle}}=\int\sqrt{-g}\,dV\,\frac{\undertilde{N}^0}{\sqrt{g_{00}}}=\int\sqrt{-g}\,dV\,N^0\]
quindi $N^0=\frac{\undertilde{N}^0}{\sqrt{g_{00}}}$.

Lo stesso vale per la corrente di carica $J^\mu$. $D_\mu J^\mu=0$ è collegato all'invarianza di gauge.
\[A_\mu\ra A_\mu+\p_\mu f\]
se $D_\mu J^\mu=0$:
\[\begin{split}\S_\text{int}&=-\frac{1}{c^2}\int\sqrt{-g}d\Omega A_\mu J^\mu\ra\S_{\text{int}}-\frac{1}{c^2}\int\sqrt{-g}\,d\Omega\,\p_\mu fJ^\mu=\\&=\S_\text{int}+\frac{1}{c^2}\int d\Omega\,f\p_\mu\left(\sqrt{-g}J^\mu\right)=\S_\text{int}+\frac{1}{c^2}\int\sqrt{-g}\,d\Omega\,fD_\mu J^\mu=\S_\text{int}\end{split}\]

Possiamo anche scrivere la conservazione di $T^{\mu\nu}$ in un sistema di riferimento arbitrario. $\p_\mu T^{\mu\nu}=0$ nel sistema localmente inerziale diventa $D_\mu T^{\mu\nu}=0$.
\[D_\mu T^{\mu\nu}=\p_\mu T^{\mu\nu}+\Gamma^\mu_{\lambda\mu}T^{\lambda\nu}+\Gamma^\nu_{\lambda\mu}T^{\mu\lambda}=\frac{1}{\sqrt{-g}}\p_\mu\qty(\sqrt{-g}T^{\mu\nu})+\underbrace{\Gamma^\nu_{\lambda\mu}T^{\mu\lambda}}_{(2)}\]
Quindi a causa di $(2)$ l'energia e l'impulso totale non si conservano
\[c\mathcal{P}^\mu=\int\sqrt{-g}\dd{V}T^{0\mu}\]
\[\begin{split}c\dv{\mathcal{P}^\mu}{x^0}&=\int\sqrt{-g}\dd{V}\p_0\qty(\sqrt{-g}T^{0\mu})=-\int\sqrt{-g}\dd{V}\qty(\frac{1}{\sqrt{-g}}\p_i\qty(\sqrt{-g}T^{i\mu})+\Gamma^\mu_{\lambda\rho}T^{\rho\lambda})=\\&
        =-\underbrace{\int ds^i\sqrt{-g}T^{i\mu}}_{(3)}-\int\sqrt{-g}\,dV\,\Gamma^\mu_{\lambda\rho}T^{\rho\lambda}\end{split}\]
$(3)$: questo si annulla mandando il bordo all'infinito. $\dv{\mathcal{P}^\mu}{x^0}\neq0$ è dovuta all'effetto del campo gravitazionale.

\subsubsection{Fluido di particelle libere}
Vediamolo per un fluido di particelle libere:
\[T^{\mu\nu}=mc^2nu^\mu u^\nu=mc^2N^\mu u^\nu\]
Inoltre
\[u^\nu D_\nu u^\mu=u^\nu\p_\nu u^\mu+u^\nu\Gamma^\mu_{\nu\lambda}u^\lambda=0\]
verifichiamo che
\[D_\mu T^{\mu\nu}=\underbrace{mc^2D_\mu N^\mu u^\nu}_{\text{\parbox{5em}{$=0$ perch\'{e} $D_\mu N^\mu=0$}}}+\underbrace{mc^2N^\mu D_\mu u^\nu}_{\text{\parbox{6em}{$=0$ perch\'{e} $u^\mu D_\mu u^\nu=0$}}}\]
Il quadrimpulso totale è $c\mathcal{P}^\mu=\int\sqrt{-g}\,dV\,mc^2N^0u^\mu$
\[\begin{split}
        c\dv{\mathcal{P}^\mu}{x^0}&=\int\dd{V}mc^2\qty\Big(\p_0\left(\sqrt{-g}N^0\right)u^\mu+\sqrt{-g}N^0\p_0u^\mu)=\\&=\int\dd{V}mc^2\qty\Big(-\p_i\left(\sqrt{-g}N^i\right)u^\mu+\sqrt{-g}N^0\p_0u^\mu)
    \end{split}\]
usando l'equazione geodetica
\[N^0\p_0u^\mu=-N^i\p_iu^\mu-N^\nu\Gamma^\mu_{\nu\lambda}u^\lambda\]
\[\begin{split}
        c\dv{\mathcal{P}^\mu}{x^0}&=\int\dd{V}mc^2\qty\Big(\p_i\left(\sqrt{-g}N^iu^\mu\right)-\sqrt{-g}N^\nu\Gamma^\mu_{\nu\lambda}u^\lambda)=\\&
        =\int\sqrt{-g}\dd{V}mc^2\Gamma^\mu_{\nu\lambda}N^\nu u^\lambda=-\int\sqrt{-g}\dd{V}\Gamma^\mu_{\nu\lambda}T^{\nu\lambda}
    \end{split}\]

Vediamo ora il tensore $T^{\mu\nu}$ per il campo elettromagnetico. In relatività ristretta avevamo trovato
\[T^{\mu\nu}=-\frac{1}{4\pi}F^\mu_{\ \rho}F^{\nu\rho}+\frac{1}{16\pi}\eta^{\mu\nu}F^{\rho\gamma}F_{\rho\gamma}\]
in uno spazio con metrica $g_{\mu\nu}$ abbiamo quindi
\[T^{\mu\nu}=-\frac{1}{4\pi}F^\mu_{\ \rho}F^{\nu\rho}+\frac{1}{16\pi}g^{\mu\nu}F^{\rho\gamma}F_{\rho\gamma}\]
esplicitando tutte le inserzioni della metrica
\[T^{\mu\nu}=-\frac{1}{4\pi}g^{\mu\lambda}g^{\nu\gamma}g^{\rho\xi}F_{\lambda\rho}F_{\gamma\xi}+\frac{1}{16\pi}g^{\mu\nu}g^{\lambda\rho}g^{\tau\gamma}F_{\lambda\tau}F_{\rho\gamma}\]
verifichiamo che $D_\mu F^{\mu\nu}=0$ usando l'equazioni di Maxwell in spazio curvo con $J^\mu=0$ trovate sopra
\[\begin{split}
        D_\mu T^{\mu\nu}&=-\frac{1}{4\pi}\left(D_\mu F^\mu_{\ \rho}F^{\nu\rho}+F^\mu_{\ \rho}D_\mu F^{\nu\rho}\right)+\frac{1}{8\pi}g^{\mu\nu}D_\mu F^{\rho\gamma}F_{\rho\gamma}=\\
        &=-\frac{1}{4\pi}F^\mu_{\ \rho}D_\mu F^{\nu\rho}-\frac{1}{8\pi}\left(D^\rho F^{\gamma\nu}+D^{\gamma} F^{\nu\rho}\right)F_ {\rho\gamma}=\\
        &=-\frac{1}{4\pi}F_{\mu\rho}D^\mu F^{\nu\rho}+\frac{1}{8\pi}F_{\rho\gamma}D^\rho F^{\nu\gamma}+\frac{1}{8\pi}F_{\gamma\rho}D^\gamma F^{\nu\rho}=\\
        &=0\end{split}\]
