\chapter[Gravità, equivalenza e covarianza]{Gravità, principi di equivalenza e covarianza}

Consideriamo come punto di partenza la gravità così come formulata da Newton. Le particelle si muovono con la legge non relativistica $m\frac{d\va{v}}{dt}=\va{F}$, e la forza gravitazionale è $\va{F}=-m\grad{\phi}$ con $\phi$ potenziale gravitazionale. Il potenziale a sua volta è determinato dalla distruzione di massa, data $\rho$ la densità di massa l'equazione di Poisson determina $\phi$:
\[\Delta\phi=4\pi G_N\rho\]

Per una particella puntiforme di massa $M$ si ha $\rho=M\delta^{(3)}(\va{r}-\va{r}_M)$ quindi $\phi=-\frac{G_NM}{\abs{\va{r}-\va{r}_M}}$. La forza subita dalla particella di massa $m$ a causa di $M$ è quindi
\[\va{F}_{mM}=G_N\,m\,M\,\grad_{\va{r}}\frac{1}{\abs{\va{r}-\va{r}_M}}=-G_NmM\frac{\va{r}-\va{r}_M}{\abs{\va{r}-\va{r}_M}^3}\]

\section{Analisi dimensionale}

\textbf{Nota}. (Richiamo sulle dimensioni) $[L]$: lunghezza, $[T]$: tempo, $[M]$: massa.
\[[G_N]=\frac{[F][L]^2}{[M]^2}=\frac{[L]^3}{[M][T]^2} \qquad\qquad [\phi]=\frac{[F][L]}{[M]}=\frac{[L]^2}{[T]^2}\]

La teoria soddisfa le simmetrie di Galileo. In particolare esiste un tempo assoluto e l'interazione si propaga istantaneamente dalla sorgente alla massa che subisce la forza. La teoria deve quindi essere modificata per soddisfare l'invarianza sotto trasformazioni di Lorentz. Siccome $\Delta\phi=4\pi\rho G_N$, e $\rho\propto T^{00}$, questo suggerisce che la gravità sarà in qualche modo descritta da un oggetto con due indici. Suggerisce inoltre che non solo la massa, ma ogni forma di energia, e in generale tutto ciò che entra nel tensore $T^{\mu\nu}$, sarà in qualche modo una sorgente di gravità.

Alcuni aspetti possono essere discussi a partire solo dall'analisi dimensionale. Una teoria relativistica della gravità conterrà 2 costanti fondamentali: $G_N$, $c$.
\[[G_N]=\frac{[L]^3}{[M][T]^2} \qquad\qquad [c]=\frac{[L]}{[T]}\]
Questo ci dice ad esempio che delle tre quantità $[L]$, $[M]$, $[T]$ solo una rimane indipendente. Ad esempio una massa può essere trasformata in una lunghezza:
\[L=\frac{G_N}{c^2}M\]


\section{Esempi classici}
\subsubsection{Buco nero}
Prendiamo il concetto newtoniano di buco nero. La velocità di fuga è definita da
\[\frac{1}{2}mv^2=G_N\frac{mM}{r}\implies v=\sqrt{\frac{2G_NM}{r}}\]
quando $v=c$ abbiamo un oggetto dalla cui superficie non può scappare la luce, troviamo quindi una relazione tra massa e lunghezza
\[R=2\frac{G_N}{c^2}M\]
questo sarà il \textit{raggio di Schwarzschild} (il coefficiente viene casualmente giusto).

\subsubsection{Orbita circolare}
Vediamolo ancora da un altro punto di vista. In teoria newtoniana un'orbita circolare uniforme attorno ad un oggetto di massa $M$ deve soddisfare
\[m\frac{v^2}{r}=G_N\frac{mM}{r^2}\implies v=\sqrt{\frac{G_NM}{r}}\]
gli effetti relativistici sono importanti quando $v\simeq c$ cioè proprio quando $r\simeq G_NM/c^2$. Per avere effetti gravitazionali e relativistici dobbiamo quindi avere una massa sufficientemente densa tale che $M/L$ sia dell'ordine di $c^2/G_N$.

\section{Costruzione della teoria}

Per costruire la teoria abbiamo bisogno di 3 concetti principali:
\begin{enumerate}
    \item Il limite non relativistico deve riprodurre i risultati della teoria newtoniana.
    \item L'invarianza relativistica (anche se vedremo assumerà una forma un po' diversa da come l'abbiamo vista finora).
    \item Il principio di equivalenza (dobbiamo ancora discuterlo).
\end{enumerate}
%Il primo è che il limite non relativistico deve riprodurre i risultati della teoria newtoniana. Il secondo è l'invarianza relativistica (anche se vedremo assumerà una forma un po' diversa da come l'abbiamo vista finora). Il terzo concetto dobbiamo ancora discuterlo, ed è il principio di equivalenza.
La teoria newtoniana può essere formalizzata in maniera più generale come segue
\[m_I\frac{d\va{v}}{dt}=\va{F}\]
\[\va{F}_\text{grav}=-m_G\grad{\phi}\]
\[\Delta\phi=4\pi\rho_GG_N\]
dove abbiamo indicato con $m_I$ la "massa inerziale", con $m_G$ la "massa gravitazionale" e con $\rho_G$ la densità di massa gravitazionale.

Ora si vede sperimentalmente che $m_I/m_G$ è uguale per tutti i corpi, quale che sia la loro composizione e forma. Possiamo quindi scegliere unità in cui $m_I=m_G\equiv m$ e abbiamo la teoria come siamo più abituati a vederla. La massa gravitazionale misura l'accoppiamento alla gravità. L'equivalente per l'elettromagnetismo sarebbe la carica elettrica. In elettromagnetismo $m/q$ non è certo una costante universale. Il fatto che sia universale, cioè l'equivalenza della massa inerziale con la massa gravitazionale, è un fatto sperimentale misurato con estrema precisione. La conseguenza più significativa è la seguente
\[m_I\frac{d\va{v}}{dt}=\va{F}_G=-m_G\grad{\phi}\xRightarrow{\text{siccome }m_I=m_G}\frac{d\va{v}}{dt}=-\grad{\phi}\]
Quindi dato un campo gravitazionale $-\grad{\phi}$, tutti i corpi si muovono in esso con la stessa equazione del moto. Ne segue che i campi gravitazionali costanti sono equivalenti a sistemi di riferimento accelerati, cioè non inerziali.

Vediamolo in maniera concreta. Per un campo costante $\va{g}=-\grad{\phi}$ costante in $\va{x}$ e $t$.  Il moto di ogni tipo di corpo soddisfa $\frac{d\va{v}}{dt}=\va{g}$, cioè un moto accelerato uniforme. Andiamo ora in un altro sistema di riferimento che chiamiamo di \textit{caduta libera}
\[t'=t, \quad \va{x}'=\va{x}-\frac{1}{2}\va{g}t^2\]
Scriviamo ora l'equazione del moto nel sistema $O'$.
\[\frac{d\va{v}'}{dt'}=\frac{d^2}{dt^2}\left(\va{x}-\frac{1}{2}\va{g}t^2\right)=\frac{d\va{v}}{dt}-\va{g}\]
Quindi $\frac{d\va{v}'}{dt'}=\va{g}-\va{g}=0$. Nel sistema $O'$ il moto è rettilineo uniforme. Se il campo $-\grad{\phi}$ non è costante, possiamo comunque cancellarlo "localmente" andando in un opportuno $O'$. $O'$ sarà localmente inerziale.

Il \textit{principio di equivalenza}, nella sua forma più debole, è proprio questo: ogni campo gravitazionale si può "cancellare", almeno localmente, andando in un sistema di riferimento di caduta libera. $m_I=m_G$ e il principio di equivalenza debole sono essenzialmente la stessa cosa. Lo chiamiamo "principio" perché d'ora in poi lo assumeremo come punto di partenza per costruire la teoria. Ma ricordandoci comunque che $m_I=m_G$ è un fatto sperimentale che noi ora assumeremo come principio. Noi assumeremo una forma ancora più stringente chiamata \textit{principio} di equivalenza forte o \textit{di Einstein}.

\begin{principio}[di Einstein]
	Ogni legge della fisica in un sistema di riferimento in caduta libera è localmente equivalente alle leggi in assenza di gravità.
\end{principio}

Già solo partendo da questo principio possiamo dedurre delle importanti conseguenze fisiche, in particolare sull'effetto delle gravità sulla luce.


\subsection{Red-shift gravitazionale}
Prendiamo un campo gravitazionale costante $-\va{\nabla}\phi=\va{g}$. Un raggio di luce con frequenza $\nu$ viene emesso in $A$ e assorbito in $B$ a una differenza di altezza. Vogliamo trovare la frequenza $\nu'$ con cui viene osservato. $A$ e $B$ sono solidali al sistema $O$ in cui è presente il campo gravitazionale. Usiamo ora il principio di equivalenza. Andiamo nel sistema $O'$ di caduta libera. Le traiettorie di $A$ e $B$ in $O'$ sono
\[\begin{cases}z'^A=\frac{1}{2}gt^2\\z'^B=h+\frac{1}{2}gt^2\end{cases}\]
La luce arriva in $B$ al tempo $t^*$
\[h+\frac{1}{2}gt^{*2}=ct^*, \quad t^*\simeq\frac{h}{c}\qty(1-\order{\frac{gh}{c^2}})\]
al tempo di arrivo $B$ ha una velocità $v_B(t^*)\simeq\frac{gh}{c}$ la frequenza misurata da $B$ è quindi minore per effetto Doppler
\[\nu'\simeq\nu\left(1-\frac{gh}{c^2}\right)\]
In particolare se un fotone ha energia $h\nu$, l'energia di osservazione è $h\nu'=h\nu\left(1-\frac{gh}{c^2}\right)$. Risolveremo in dettaglio questo problema quando avremo formulato la teoria. Questo effetto si chiama \textit{red-shift gravitazionale}, l'esistenza di questo effetto dipende unicamente dal principio di equivalenza.


\section{Effetti relativistici}
Torniamo a discutere le costanti fondamentali. Dato $G_N=\SI[per-mode=fraction]{6.67e-11}{\cubic\metre\per\kilogram\per\square\second}$ e $c=\SI[per-mode=symbol]{3e8}{\metre\per\second}$ possiamo costruire una relazione fra massa e lunghezza
\[r_S=\frac{2G_N}{c^2}M \text{, \quad con \quad} \frac{2G_N}{c^2}=\SI[per-mode=fraction]{1.5e-27}{\metre\per\kilogram}\]
Ad esempio per una scala "umana", $\SI{100}{\kilogram}\implies r_S=\SI{1.5e-25}{\metre}$ quindi completamente trascurabile rispetto alla dimensione tipica $\sim\SI{1}{\metre}$. Per il sole abbiamo $M_\odot=\SI{2e30}{\kilogram}\rightarrow r_{S,\odot}=\SI{3e3}{\metre}$. Considerando che il raggio solare è $R_\odot=\SI{7e8}{\metre}$, abbiamo $r_{S,\odot}/R_\odot=4\times10^{-6}$, piccolo ma potenzialmente osservabile. L'anomalia sulla precessione dell'orbita di Mercurio è dovuta proprio a questo effetto. Dato ogni oggetto con una massa e una dimensione, possiamo metterlo in un grafico e $M,R$ e vedere quanto è vicino alla retta $M=\frac{L^2}{2G_N}R$ così da avere un'idea di quanto possano essere grandi gli effetti combinati di gravità e relatività.

IMMAGINE PER CHIARIRE

Per un protone $m_p=\SI{1.7e-27}{\kilogram}\implies r_{S,p}=\SI{2e-54}{\metre}$, totalmente trascurabile rispetto al raggio $R_p\approx\SI{e-15}{\metre}$. Per poter vedere effetti significativi per una massa $M$, dobbiamo raggiungere densità dell'ordine di $\rho=\frac{M}{r_S^3}=\left(\frac{c^2}{2G_N}\right)^3\frac{1}{M^2}$. Ad esempio per una stella $M\approx M_\odot$, abbiamo $\rho\approx\SI[per-mode=symbol]{e20}{\kilogram\per\cubic\metre}$ nota che questa è vicino all'ordine della densità nucleare $\frac{M_p}{R_p^3}\approx\SI[per-mode=symbol]{e18}{\kilogram\per\cubic\metre}$. Questo è proprio quello che succede nelle stelle di neutroni, in cui gli effetti gravitazionali e relativistici sono rilevanti. Nota che siccome $\rho\approx\frac{1}{M^2}$, per oggetti più massivi non abbiamo bisogno di grandissime densità. Un esempio sono i buchi neri al centro delle galassie con una massa $\approx10^{11}M_\odot$.

Con $G_N$ e $c$ si possono trasformare masse ed energie in quantità puramente geometriche. Le equazioni di Einstein che studieremo hanno proprio questa struttura. Trasformano una densità di energia $\E$ e in una "curvatura", un oggetto geometrico delle dimensioni $1/L^2$.
\[\underbrace{\frac{1}{L^2}}_{\text{curvatura}}=\frac{G_N}{c^4}\underbrace{\frac{M}{LT^2}}_{\text{\parbox{4.4em}{densità di energia}}}\]
%SI{2.98}{\meter\kilogram\per\cubic\second\per\ampere}