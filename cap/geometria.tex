\chapter[Geometria dello spazio tempo]{Geometria dello spazio tempo}
Dobbiamo trovare il giusto linguaggio matematico. Vogliamo la possibilità di studiare la teoria in qualsiasi sistema di riferimento. Il principio di equivalenza implica che i sistemi globalmente inerziali non si possono costruire: non esistono osservatori per cui si possa "spegnere" la gravità.

Nella sua forma più generale, un sistema di riferimento è un'assegnazione di coordinate, in cui ad ogni punto dello spazio-tempo cioè ad ogni evento, assegniamo quattro "numeri" $x^\mu(p)$. Consideriamo quindi lo spazio-tempo $M$ come l'insieme degli eventi. Un \textit{sistema di coordinate} è una mappa $M\xrightarrow{\phi}\mathbb{R}^4$. Un sistema di coordinate può anche essere limitato, e coprire solo una parte di $M$. Cambiare sistema di riferimento significa scegliere un'altra mappa $M\xrightarrow{\phi'}\mathbb{R}^4$ in cui lo stesso evento $p$ è ora contrassegnato dalla quaterna $x'^\mu(p)$. L'evento $p$ rimane sempre lo stesso, semplicemente viene contrassegnato diversamente dai due sistemi di riferimento. Questo è proprio il concetto matematico di \textit{varietà differenziale}. Una mappa tra due sistemi di riferimento diversi è la funzione che, data la quaterna $x^\nu(p)$ di un certo evento $p$, associa la quaterna $x'^\mu(p)$ dello stesso evento, è la mappa $\phi'\circ\phi^{-1}:x^\nu\ra x'^\nu$. Lo spazio tempo è quindi definito in questo modo da un insieme di mappe, che lo ricoprono tutto, e dalle relative trasformazioni di coordinate $x'^\mu(x^\nu)$. Noi considereremo queste mappe sempre continue e differenziabili.


\section{Cambi di coordinate (esempi)}

I cambi di coordinate si usano spesso anche in geometria euclidea e meccanica non-relativistica.

\subsection{Coordinate sferiche}
\begin{center}
	\tdplotsetmaincoords{70}{110}
	\begin{tikzpicture}[tdplot_main_coords]
	\draw[thick,->] (0,0,0) -- (3,0,0) node[anchor=north east]{$x$};
	\draw[thick,->] (0,0,0) -- (0,3,0) node[anchor=north west]{$y$};
	\draw[thick,->] (0,0,0) -- (0,0,3) node[anchor=south]{$z$};
	\pgfmathsetmacro{\ax}{1}
	\pgfmathsetmacro{\ay}{1}
	\pgfmathsetmacro{\az}{1}
	\draw[->,red] (0,0,0) -- (\ax,\ay,\az);
	\draw[dashed,red] (0,0,0) -- (\ax,\ay,0) -- (\ax,\ay,\az);
	\tdplotgetpolarcoords{\ax}{\ay}{\az}
	\tdplotsetthetaplanecoords{\tdplotresphi}
	\tdplotdrawarc[tdplot_rotated_coords]{(0,0,0)}{1}{0}{\tdplotrestheta}{anchor=west}{$\theta$}%{$\theta = \tdplotrestheta$}
	%\tdplotsetthetaplanecoords{\tdplotrestheta}
	\tdplotdrawarc[tdplot_rotated_coords]{(0,0,0)}{0}{1}{\tdplotresphi}{anchor=north}{$\vp$}%{$\phi = \tdplotresphi$}
	\end{tikzpicture}
\end{center}
%FINEIRE DISEGNO
Un esempio spesso usato è quello delle coordinate sferiche.
\[\begin{cases}r=\sqrt{x_1^2+x_2^2+x_3^2}\\\theta=\arccos\frac{x_3}{\sqrt{x_1^2+x_2^2+x_3^2}}\\\vp=\arccos\frac{x_1}{\sqrt{x_1^2+x_2^2}}\end{cases}\]
Nel linguaggio di prima $x^\mu=(x_1,x_2,x_3)$ e $x'^\mu=(r,\theta,\vp)$. Il cambio di coordinate corrisponde alla mappa $(x_1,x_2,x_3)\rightarrow(r,\theta,\vp)$. La trasformazione inversa è la seguente:
\[\begin{cases}x_1=r\sin\theta\cos\vp\\x_2=r\sin\theta\sin\vp\\x_3=r\cos\theta\end{cases}\]
Questa è la mappa $(r,\theta,\vp)\rightarrow(x_1,x_2,x_3)$.

Sappiamo bene che un moto rettilineo uniforme viene visto in maniera diversa. Prendiamo un esempio:
\[\begin{cases}x_1 &= d\\ x_2 &= vt\\ x_3 &= 0\\\end{cases} \qquad\qquad \begin{cases}r &= \sqrt{d^2+v^2t^2}\\ \theta &= \pi/2\\ \vp &=\arccos\frac{d}{\sqrt{d^2+v^2t^2}}\end{cases}\]
\begin{center}
	\begin{tikzpicture}[scale=0.7]
	\begin{axis}[xmin=-1,xmax=5,ymin=-1,ymax=5,axis x line=middle,axis y line=middle,xlabel={$t$},ylabel={$x_1$},xticklabels=\empty, yticklabels=\empty]
	\addplot [red,thick] {2};
	\end{axis}
	\end{tikzpicture}
	\hspace{10pt}
	\begin{tikzpicture}[scale=0.7]
	\begin{axis}[xmin=-1,xmax=5,ymin=-1,ymax=5,axis x line=middle,axis y line=middle,xlabel={$t$},ylabel={$x_2$},xticklabels=\empty, yticklabels=\empty]
	\addplot [red,thick] {x};
	\end{axis}
	\end{tikzpicture}
	\hspace{10pt}
	\begin{tikzpicture}[scale=0.7]
	\begin{axis}[xmin=-1,xmax=5,ymin=-1,ymax=5,axis x line=middle,axis y line=middle,xlabel={$t$},ylabel={$x_3$},xticklabels=\empty, yticklabels=\empty]
	\addplot [red,thick] {0};
	\end{axis}
	\end{tikzpicture}
\end{center}
\begin{center}
	\begin{tikzpicture}[scale=0.7]
	\begin{axis}[xmin=-2.8,xmax=3.2,ymin=-1,ymax=5,axis x line=middle,axis y line=middle,xlabel={$t$},ylabel={$v$},xticklabels=\empty, yticklabels=\empty]
	\addplot [red,thick] {sqrt(2.25+x^2)};
	\addplot [thick,dashed,domain=0:3.2] {x};
	\addplot [thick,dashed,domain=-2.8:0] {-x};
	\end{axis}
	\end{tikzpicture}
	\hspace{10pt}
	\begin{tikzpicture}[scale=0.7]
	\begin{axis}[xmin=-1,xmax=5,ymin=-1,ymax=5,axis x line=middle,axis y line=middle,xlabel={$t$},ylabel={$\theta$},xticklabels=\empty, yticklabels=\empty]
	\addplot [red,thick] {2};
	%\addplot [red,thick,samples=500] {cos(deg(x))}; %deg(x)
	\end{axis}
	\end{tikzpicture}
	\hspace{10pt}
	\begin{tikzpicture}[scale=0.7]
	\begin{axis}[trig format plots=rad,
	samples=101,
	unbounded coords=jump,
	xmin=-3,xmax=3,ymax=pi/2+0.5, axis x line=middle,axis y line=middle,xlabel={$t$},ylabel={$\vp$},xticklabels=\empty, yticklabels=\empty]
	\addplot[red,thick,variable=\t,domain=-10:10] (\t,{atan(\t)});
	\end{axis}
	\end{tikzpicture}
\end{center}
%FINIRE DISEGNO
Quindi non è certo una traiettoria rettilinea in $(r,\theta,\vp)$, ma vedremo come generalizzare il concetto di "moto rettilineo uniforme" in un sistema arbitrario di coordinate. Tutto dipende dalla metrica, e la metrica cambia aspetto se cambiano le coordinate. La metrica euclidea è $\dd{l}^2=\dd{x_1}^2+\dd{x_2}^2+\dd{x_3}^2$, dove $\dd{l}$ è l'elemento infinitesimo di lunghezza. In coordinate sferiche la metrica è $dl^2=dr^2+r^2d\theta^2+r^2\sin^2\theta d\vp^2$. La lunghezza $dl$ è sempre la stessa, quello che cambia è la forma nei due sistemi di coordinate.

Consideriamo un esempio in cui entra in gioco anche il tempo. In relatività ristretta l'intervallo invariante è
\[ds^2=c^2dt^2-dx_1^2-dx_2^2-dx_3^2=\eta_{\mu\nu}dx^\mu dx^\nu\]
La metrica $\eta_{\mu\nu}$ abbiamo visto che rimane invariata se cambiamo le coordinate andando in un altro sistema inerziale $x'^\mu(x^\nu)=\Lambda^\mu_{\ \nu} x^\nu$. Se invece facciamo un cambio di coordinate qualsiasi, la metrica cambierà forma.


\subsection{Sistema rotante}
Prendiamo un sistema rotante rispetto a quello inerziale.% FARE DISEGNO
\[\begin{cases}t=t'\\x_1=x'_1\cos(\omega t')-x'_2\sin(\omega t')\\x_2=x'_1\sin(\omega t')+x'_2\cos(\omega t')\\x_3=x'_3\\\end{cases}\]
con $\omega$ costante. Il sistema $x'^\mu$ sarà ruotato rigidamente con velocità angolare $\omega$ attorno all'asse $x_3$. Vediamo come la metrica viene vista nel sistema $O'$:
\[\begin{split} ds^2 &= c^2dt^2-dx^2_1-dx^2_2-dx^2_3=\\
&= cdt'^2-\left(\frac{\partial x_1}{\partial t'}dt'+\frac{\partial x_1}{\partial x'_1}dx'_1+\frac{\partial x_1}{\partial x'_2}dx'_2\right)^2-\left(\frac{\partial x_2}{\partial t'}dt'+\frac{\partial x_2}{\partial x'_1}dx'_1+\frac{\partial x_2}{\partial x'_2}dx'_2\right)^2-dx'^2_3 =\\
&= \left(1-\frac{\omega^2}{c^2}(x'^2_1+x'^2_2)\right)c^2dt'^2-dx'^2_1-dx'^2_2+2\omega x'_2dt'dx'_1-2\omega x'_1dt'dx'^2_2-dx'^2_3\end{split}\]
Possiamo quindi scriverlo come
\[ds^2=g'_{\mu\nu}dx'^\mu dx'^{\nu}\]
dove $g'_{\mu\nu}$ è la metrica nel sistema $O'$
\[[g'_{\mu\nu}]=\begin{pmatrix} 1-\frac{\omega^2}{c^2}(x'^2_1+x'^2_2) & \frac{\omega}{c} x'_2 & \frac{\omega}{c} x'_1 & 0\\ \frac{\omega}{c} x'_2 & -1 & 0 & 0\\ \frac{\omega}{c} x'_1 & 0 & -1 & 0\\ 0 & 0 & 0 & -1\end{pmatrix}\]
Nota che se prendiamo un osservatore a $x'_i$ fissato ($\dd{x_i}=0$), il tempo proprio vale $d\tau=\frac{\dd{s}}{c}=\sqrt{1-\frac{\omega^2}{c^2}(x'^2_1+x'^2_2)}dt'$. Siccome per un osservatore fisico $d\tau$ deve essere reale (non immaginario) abbiamo $1\geq\frac{\omega^2}{c^2}(x'^2_1+x'^2_2)=\frac{v^2}{c^2}$, dove $v=\omega\sqrt{x'^2_1+x'^2_2}$. Questo torna perché $v$ è la velocità di rotazione nel sistema inerziale. Comunque nulla ci vieta di considerare $x'^{\mu}$ come coordinate anche per $1<\frac{\omega^2}{c^2}\qty(x'^2_1+x'^2_2)$, semplicemente non ci possono essere oggetti fisici che hanno come traiettoria $x'^i=\text{costante}$ in questa regione. Dobbiamo quindi sviluppare un formalismo matematico che ci permetta di scrivere le equazioni della fisica in qualsiasi sistema di coordinate dove $ds^2=g_{\mu\nu}dx^\mu dx^\nu$\footnote{dove $g$ è una metrica qualsiasi, in generale diversa da quella di Minkowski $\eta$.}.

\section{Vettori}
Nei due esempi sopra eravamo partiti da un sistema globalmente inerziale, in cui $g_{\mu\nu}=\eta_{\mu\nu}$ ovunque. Per prima cosa dobbiamo ripensare il concetto di vettore. In relatività ristretta dati due eventi $p_1$ e $p_2$ possiamo definire un "vettore distanza"
\[\Delta x^\mu=x^\mu(p_1)-x^\mu(p_2)\]
dove $x^\mu$ sono le coordinate di un sistema $O$ inerziale. $\Delta x^\mu$ è un quadrivettore (controvariante). La struttura di spazio vettoriale significa che possiamo sommare i vettori e moltiplicarli per costanti. Inoltre sotto trasformazioni di Lorentz $\Delta x'^\mu=\Lambda^\mu_{\ \nu}\Delta x^\nu$ quindi possiamo definire il vettore in un sistema inerziale a piacere, le trasformazioni assicurano che la struttura di spazio vettoriale è intrinseca, non dipende dal sistema inerziale scelto. Se ammettiamo sistemi di coordinate qualsiasi non possiamo più fare la "differenza" di due eventi. "$\Delta x^\mu=x^\mu(p_1)-x^\mu(p_2)$" non ha alcun significato intrinseco in un sistema di coordinate arbitrario. Se però ci limitiamo a differenze infinitesime possiamo ritrovare una struttura lineare.

Prendiamo due sistemi di coordinate $x^\mu$ e $x'^\mu$ e la mappa $x'^\mu(x^\nu)$. Facciamo un'espansione in serie attorno a $p_1$:
\[x'^\mu(x^\nu(p_1))=x'^\mu(p_1)\]
\[x'^\mu(x^\nu(p_1)+dx^\nu)=x'^\mu(p_1)+\frac{\partial x'^\mu}{\partial x^\nu}(p_1)dx^\nu+\order{dx^{\nu2}}\]
$dx^\nu$ è lo spostamento infinitesimo, ma $x'^\mu(x^\nu(p_1)+dx^\nu)-x'^\mu(p_1)=dx'^\mu$, quindi
\[dx'^\mu=\frac{\partial x'^\mu}{\partial x^\nu}(p_1)dx^\nu+\order{dx^{\nu2}}\]
Per cui gli spostamenti infinitesimi trasformano linearmente, quindi conservano la struttura di spazio vettoriale. Possiamo quindi dare la definizione di vettore (controvariante).
\begin{definition}[Vettore controvariante]
	Un vettore $A^\mu$, associato ad un certo evento $p$, è una quaterna di numeri che trasforma nel seguente modo sotto cambio di coordinate:
	\[A'^\mu(p)=\frac{\partial x'^\mu}{\partial x^\nu}(p)A^\nu(p)\]
\end{definition}
Gli spostamenti infinitesimi sono vettori, a meno di $\order{dx^2}$. Comunque un vettore non deve essere infinitesimo: basta che soddisfi la definizione sopra. Nota che un vettore per essere definito deve essere associato ad un evento. Viceversa ogni evento dello spazio-tempo ha associato uno spazio vettoriale. Questo spazio vettoriale si chiama anche \textit{spazio tangente} e spesso si identifica con $T_p(M)$ ($T$: tangente, $p$: il punto $p$ dove è definito, $M$: lo spazio-tempo). Se potessimo immergere $M$ in uno spazio più grande con metrica $\eta_{\mu\nu}$, $T_p(M)$ è proprio il piano tangente ad $M$ nel punto $p$. Il vettore $A^\mu(p)$ può essere visualizzato come una freccia, cioè una direzione di spostamento con un certo modulo, "attaccata" al punto $p$.

\subsection{Vettore derivata}
Vediamo un esempio importante di vettore. Consideriamo una curva nello spazio-tempo cioè una funzione $f:\mathbb{R}\rightarrow M$, scegliendo un sistema di coordinate si esprime come 4 funzioni $x^\mu(\lambda)$. La derivata $\frac{dx^\mu}{d\lambda}$, è proprio un vettore, cioè un elemento dello spazio tangente $T_p(M)$ con $p=x^\mu(\lambda)$. Vediamo infatti che trasforma correttamente sotto cambio di coordinate.
\[\frac{dx'^\mu}{d\lambda}=\frac{\partial x'^\mu}{\partial x^\nu}\frac{dx^\nu}{d\lambda}\]
IMMAGINE
In realtà abbiamo "tanti" vettori, uno per ogni punto dello spazio-tempo toccato dalla curva. Un \textit{campo vettoriale} è una funzione che associa un vettore ad ogni punto dello spazio-tempo. Dato un sistema di coordinate abbiamo $A^\mu:\mathbb{R}^4\rightarrow\mathbb{R}^4$, cioè dato un punto $x^\nu$ gli associamo il vettore $A^\mu(x^\nu)$. $A^\mu(x^\nu)$ appartiene allo spazio tangente del punto in cui viene calcolato. Dato un campo vettoriale possiamo studiare il suo flusso, risolvendo l'equazione $\frac{dx^\mu}{d\lambda}=A^\mu(x^\nu)$ dato un punto di partenza, ad esempio $x^\mu(0)$, la soluzione ci fornisce una traiettoria $x^\mu(\lambda)$ la cui proprietà è quella di avere come tangente proprio il vettore $A^\mu(x^\nu(\lambda))$. L'equazione sopra è ben definita:
\[\frac{dx'^\mu}{d\lambda}=\frac{\partial x'^\mu}{\partial x^\nu}\frac{dx^\nu}{d\lambda}=\pdv{\xmp}{\xn}A^\nu(x^\nu)\]
ma questo è proprio il campo nel sistema $O'$, quindi $\frac{dx'^\mu}{d\lambda}=A'^\nu(x'^\nu)$. Quindi la soluzione del flusso vettoriale e intrinseca, cioè non dipende dal particolare sistema di coordinate che usiamo.


\section{Covettori}
Un altro oggetto che dobbiamo introdurre è il \textit{covettore}, o vettore covariante. Partiamo dal concetto di funzione scalare. Una funzione $F:M\rightarrow\mathbb{R}$ è una associazione di un numero reale $F(p)$ ad ogni punto dello spazio-tempo. Il numero associato a $p$ deve essere lo stesso, indipendentemente dal sistema di riferimento. Ad esempio se in $O$ vale
\[F(p)=f(x^\mu(p))\]
allora nel sistema $O'$ abbiamo
\[F(p)=f(x^\mu(x'^\nu(p)))=f'(x'^\nu(p))\]
Abuseremo un po' il linguaggio e indicheremo $f(p)$, $f(x^\mu)$, $f(x'^\mu)$ sempre con lo stesso simbolo $f$.

Il gradiente di $f$ è un oggetto che trasforma linearmente sotto cambio di coordinate. In un sistema $O$ il gradiente è definito dalla quaterna di numeri $\frac{\partial f}{\partial x^\mu}$. Nel sistema $O'$:
\[\frac{\partial f}{\partial x'^\mu}=\frac{\partial f}{\partial x^\nu}\frac{\partial x^\nu}{\partial x'^\mu}\]
La trasformazione è diversa da quella dei vettori vista prima. Definiamo quindi un nuovo oggetto lineare, covettore o vettore covariante

\begin{definition}[Vettore covariante]
	Un covettore è una quaterna $A_\mu$, associata ad un punto $p$, che trasforma come
	\[A'_\mu=\frac{\partial x^\nu}{\partial x'^\mu}A_\nu\]
\end{definition}

Visti come vettori colonna, i controvarianti trasformano come
\[A'^\mu=\frac{\partial x'^\mu}{\partial x^\nu}A^\nu \implies \left[A'^\mu\right]=L\left[A_\nu\right] \text{,\qquad} L=\left[\frac{\partial x'^\mu}{\partial x^\nu}\right]\]
i covarianti invece
\[A'_\mu=\frac{\partial x^\nu}{\partial x'^\mu}A_\nu \implies \left[A'_\mu\right]={L^{-1}}^t\left[A_\nu\right] \text{,\qquad} {L^{-1}}^t=\left[\frac{\partial x^\nu}{\partial x'^\mu}\right]\]
quindi trasformano con ${L^{-1}}^t$.

\subsection{Scalari}
Dato con vettore $B_\mu$ e un vettore $A^\mu$ possiamo costruire uno scalare $B_\mu A^\mu$. Dimostriamolo:
\[B'_\mu A'^\mu=B_\rho\frac{\p x^\rho}{\p x'^\mu}\frac{\p x'^\mu}{\p x^\nu}A^\nu=B_\rho\delta^\rho_{\ \nu}A^\nu=B_\mu A^\mu\]
I covettori sono quindi associabili allo spazio duale di $T_p(M)$, cioè lo spazio delle applicazioni lineari da $T_p(M)\rightarrow\mathbb{R}$. Spesso viene usata questa come definizione di covettori, da cui poi segue la proprietà di trasformazione. Indichiamo con $T_p^*(M)$ lo spazio vettoriale dei covettori associati al punto $p$. Un campo di covettori è una funzione che associa ad ogni punto di $M$ un covettore $A_\mu(x^\nu)$. Quindi data una funzione scalare $f$, $\partial_\mu f$ è un campo di covettori. Non tutti i campi $A_\mu(x^\nu)$ possono però scriversi nella forma $\partial_\mu f$.


\section{Tensori}
Possiamo ora dare la definizione generale di tensore. Un tensore $(r,s)$ è definito in un sistema $O$ dallo spazio vettoriale $\mathbb{R}^4$ con le sue componenti $\tensor{T}{^{\mu_1\ldots\mu_r}_{\rho_1\ldots\rho_s}}$. Sotto cambio di coordinate il tensore trasforma come
\[\tensor{{T'}}{^{\mu_1\ldots\mu_r}_{\rho_1\ldots\rho_s}}=\frac{\p x'^{\mu_1}}{\p x^{\nu_1}}\cdots\frac{\p x'^{\mu_r}}{\p x^{\nu_r}}\frac{\p x^{\gamma_1}}{\p x'^{\rho_1}}\cdots\frac{\p x^{\gamma_s}}{\p x'^{\rho_s}}\tensor{T}{^{\nu_1\ldots\nu_r}_{\gamma_1\ldots\gamma_s}}\]
La metrica $g_{\mu\nu}$ è un particolare tensore di tipo (0,2). Infatti
\[ds'^2=g'_{\mu\nu}dx'^\mu dx'^\nu\]
ma dall'invarianza di $ds$ abbiamo anche
\[ds=ds' \implies g_{\rho\tau}dx^\rho dx^\tau=g_{\rho\tau}\frac{\partial x^\rho}{\partial x'^\mu}\frac{\partial x^\tau}{\partial x'^\nu}dx'^\mu dx'^\nu\]
quindi deve essere
\[g'_{\mu\nu}=\frac{\partial x^\rho}{\partial x'^\mu}\frac{\partial x^\tau}{\partial x'^\nu}g_{\rho\tau}\]
e questa è proprio la trasformazione dei tensori di tipo $(0,2)$. La metrica è un tensore simmetrico $g_{\mu\nu}=g_{\nu\mu}$. In forma matriciale, se chiamiamo $\left[\frac{\partial x'^\mu}{\partial x^\nu}\right]=L$
\[\left[g'_{\mu\nu}\right]=L^{-1t}\left[g_{\mu\nu}\right]L^{-1}\]
$\left[\frac{\partial x'^\mu}{\partial x^\nu}\right]=L$ può essere una qualsiasi matrice non singolare, $\det L \neq 0$. Quindi possiamo sempre diagonalizzare $\left[g_{\mu\nu}\right]$ con una $L$ ortogonale, e poi con opportune dilatazioni o contrazioni portarla in una delle forme
\begin{gather*}\begin{pmatrix}1&0&0&0\\0&1&0&0\\0&0&1&0\\0&0&0&1\end{pmatrix}\quad\begin{pmatrix}1&0&0&0\\0&1&0&0\\0&0&1&0\\0&0&0&-1\end{pmatrix}\quad\begin{pmatrix}1&0&0&0\\0&1&0&0\\0&0&-1&0\\0&0&0&-1\end{pmatrix}\\\begin{pmatrix}1&0&0&0\\0&-1&0&0\\0&0&-1&0\\0&0&0&-1\end{pmatrix}\quad\begin{pmatrix}-1&0&0&0\\0&-1&0&0\\0&0&-1&0\\0&0&0&-1\end{pmatrix}\end{gather*}
Deve essere la quarta se vale il principio di equivalenza.


\subsection{Tensori invarianti}
Discutiamo ora i tensori invarianti. $\tensor{\delta}{^\mu_\nu}$, l'identità $\qty[\tensor{\delta}{^\mu_\nu}]=\mathbb{I}_{4\times4}$, è un tensore invariante di tipo $(1,1)$.
\[\tensor{{\delta'}}{^\mu_\nu}=\pdv{\xmp}{\xr}\pdv{\xg}{\xnp}\tensor{\delta}{^\rho_\gamma}=\pdv{\xmp}{\xr}\pdv{\xr}{\xnp}=\tensor{\delta}{^\mu_\nu}\]
Quindi rimane l'identità in ogni sistema di riferimento.
Il tensore completamente antisimmetrico $\epsilon^{\mu\nu\rho\gamma}$ non è invariante, ma quasi
\[\epsilon'^{\mu_1\mu_2\mu_3\mu_4}=\frac{\partial x'^{\mu_1}}{\partial x^{\nu_1}}\frac{\partial x'^{\mu_2}}{\partial x^{\nu_2}}\frac{\partial x'^{\mu_3}}{\partial x^{\nu_3}}\frac{\partial x'^{\mu_4}}{\partial x^{\nu_4}}\epsilon^{\nu_1\nu_2\nu_3\nu_4}=\det\left[\frac{\partial x'^\mu}{\partial x^\nu}\right]\epsilon^{\mu_1\mu_2\mu_3\mu_4}\]
se abbiamo una metrica $g_{\mu\nu}$, possiamo costruire un tensore completamente antisimmetrico e invariante. 

Nota che dalla trasformazione della metrica, prendendone il determinante
\[\det\left[g'_{\mu\nu}\right]=\det\left[\frac{\partial x}{\partial x'}\right]^2\det\left[g_{\mu\nu}\right]\]
chiamiamo $g\equiv\det\left[g_{\mu\nu}\right]$ per semplicità. Possiamo quindi dimostrare che
\[E^{\mu\nu\rho\gamma}=\frac{1}{\sqrt{-g}}\epsilon^{\mu\nu\rho\gamma}\]
è un tensore invariante:
\[E'^{\mu_1\mu_2\mu_3\mu_4}=\frac{1}{\sqrt{-g}}\frac{\p x'^{\mu_1}}{\p x^{\nu_1}}\cdots\frac{\p x'^{\mu_4}}{\p x^{\nu_4}}\epsilon^{\nu_1\ldots\nu_4}=\frac{1}{\sqrt{-g}}\det\left[\frac{\p x'}{\p x}\right]\epsilon^{\mu_1\mu_2\mu_3\mu_4}=\frac{1}{\sqrt{-g'}}\epsilon^{\mu_1\mu_2\mu_3\mu_4}\]
Allo stesso modo possiamo dimostrare che $E_{\mu\nu\rho\gamma}=\sqrt{-g}\epsilon_{\mu\nu\rho\gamma}$ è invariante.

L'elemento di volume $c\dd{t}\dd{x_1}\dd{x_2}\dd{x_3}$ non è più invariante perché $\det\left[\frac{\partial x'}{\partial x}\right]$ può essere anche $\neq1$. Come prima possiamo usare il determinante della metrica per costruire un elemento di volume invariante
\[d\Omega=\sqrt{-g}\dd{x^0}\dd{x^1}\dd{x^2}\dd{x^3}\]
è invariante, infatti:
\[d\Omega'=\sqrt{-g}\det\left[\frac{\partial x}{\partial x'}\right]\dd{x'^0}\dd{x'^1}\dd{x'^2}\dd{x'^3}=\sqrt{-g'}\dd{x'^0}\dd{x'^1}\dd{x'^2}\dd{x'^3}\]
Gli integrali del tipo $\int\sqrt{-g}\dd{x^0}\dd{x^1}\dd{x^2}\dd{x^3}f$ con $f$ una funzione scalare sono quindi invarianti per ogni cambiamento di coordinate. Scrivendo un'azione in questo modo abbiamo automaticamente l'invarianza delle equazioni per ogni cambio di sistema di riferimento.

\subsubsection{Campi costanti}
Una classe particolare di metriche dello spazio-tempo è quella in cui è possibile trovare un sistema di coordinate in cui $g_{\mu\nu}(x^i)$, cioè la metrica non dipende da $x^0$. Questo si chiama \textit{campo gravitazionale costante}. Se in un sistema $O$ vale $\partial_0g_{\mu\nu}=0\ \forall x^\mu$, allora questo rimane valido in tutti i sistemi $O'$ tali che
\[\begin{cases}x^0 = \alpha x'^0 + \beta(x'^i)\\x^i = x^i(x'^s)\end{cases}\qquad\left[\frac{\partial x^\mu}{\p x'^\nu}\right]=\left(\begin{array}{c|c c c} \alpha & & \p_j\beta & \\ \hline & & & \\ 0 & & \p_jx^i & \\ & & & \end{array}\right)\]%\[=\begin{pmatrix}\alpha&\partial_1\beta&\partial_2\beta&\partial_3\beta\\0&\partial_1x^1&\partial_2x^1&\partial_3x^1\\0&\partial_1x^2&\partial_2x^2&\partial_3x^2\\0&\partial_1x^3&\partial_2x^3&\partial_3x^3\end{pmatrix}\]
quindi $g'_{\mu\nu}$ non ha nessuna dipendenza da $x'^0$.

\subsubsection{Campi statici}
Fra tutti i sistemi in cui $\p_0g'_{\mu\nu}=0$ vorremmo vedere se è possibile imporre anche $g'_{0i}=g'_{i0}=0$.
\[g'_{\mu\nu}=\frac{\p x^\rho}{\p x'^\mu}\frac{\p x^\gamma}{\p x'^\nu}g_{\rho\gamma}\qquad\left[g'\right]=\left[\frac{\p x}{\p x'}\right]\cdot\left[g\right]\cdot\left[\frac{\p x}{\p x'}\right]^t\]
\[g'_{0i}=\frac{\p x^0}{\p x'^0}\frac{\p x^0}{\p x'^i}g_{00}+\frac{\p x^0}{\p x'^0}\frac{\p x^j}{\p x'^i}g_{0j}+\frac{\p x^j}{\p x'^0}\frac{\p x^0}{\p x'^i}g_{j0}+\frac{\p x^j}{\p x'^0}\frac{\p x^k}{\p x'^i}g_{jk}=\alpha\frac{\p\beta}{\p x'^i}g_{00}+\alpha\frac{\p x^j}{\p x'^i}g_{0j}\]
quindi per avere $g'_{0i}=0$ dobbiamo risolvere $\frac{\p\beta}{\p x'^i}=-\frac{\p x^j}{\p x'^i}g_{0j}\frac{1}{g_{00}}$. Questo non è sempre possibile (non tutti i vettori possono scriversi come gradiente). Se è possibile il campo gravitazionale si dice anche statico e la metrica
\[g_{\mu\nu}=\left(\begin{array}{c|c c c} g_{00} & & 0 & \\ \hline & & & \\ 0 & & g_{ij} & \\ & & & \end{array}\right)\]
dipende solo da $x^i$. I campi statici sono un sottoinsieme dei campi costanti (o stazionari).


\section{Shift gravitazionale}
\subsection{Metrica costante}
Per i campi costanti la coordinata $x^0$, nel sistema in cui $\p_0g_{\mu\nu}=0$, è detta \textit{tempo universale}. Prendiamo in tale campo due osservatori fermi in $x_{iA}$ e $x_{iB}$. Il \textit{tempo proprio} misurato dagli osservatori fermi ($\dd{x^j}=0$) è
\[\dd{\tau}=\frac{1}{c}\sqrt{\dd{s}^2}=\frac{1}{c}\sqrt{g_{00}{\dd{x}^0}^2}=\frac{\sqrt{g_{00}}}{c}\dd{x}^0\]
quindi se $g_{00}$ dipende da $x^i$ anche il tempo proprio dipende da $x^i$. Se un raggio di luce con frequenza $\nu_A$ parte da $x^i_A$ e arriva a $x^i_B$ possiamo calcolare $\nu_B$, indipendentemente da come si propaga la luce (cosa che ancora dobbiamo studiare). Le traiettorie di due fronti d'onda, siccome $\p_0g_{\mu\nu}=0$, devono essere identiche, a meno di una traslazione del tempo universale $x^0$. Quindi se i due fronti partono con $\Delta x^0$ da $x^i_A$, devono anche arrivare con lo stesso $\Delta x^0$ in $x^i_B$. Però i due osservatori misurano la frequenza con il loro tempo proprio
\[\nu_A\propto\frac{1}{\sqrt{g_{00}(x^i_A)}}\frac{1}{\Delta x_0}\qquad\nu_B\propto\frac{1}{\sqrt{g_{00}(x^i_B)}}\frac{1}{\Delta x_0}\]
quindi
\[\frac{\nu_B}{\nu_A}=\frac{\sqrt{g_{00}(x^i_A)}}{\sqrt{g_{00}(x^i_B)}}\]
questo è lo \textit{shift gravitazionale} della frequenza.

\subsection{Metrica generica}
\subsubsection{Tempo locale}
Data una metrica generale $g_{\mu\nu}(x^\rho)$ possiamo definire un \textit{tempo locale} per un osservatore fermo in $x^i=\text{cost}$ dato da
\[\tau=\int\frac{1}{c}\sqrt{g_{00}}\dd{x^0}\]
Solo se $g_{00}>0$ possiamo avere un oggetto fisico con traiettoria $x^i=\text{cost}$. 

\subsubsection{Metrica spaziale locale}
Possiamo anche definire una nozione di distanza spaziale, ma questa in genere ha senso solo localmente. Per questo mandiamo un raggio di luce da $x^i$ a $x^i+dx^i$ e poi lo facciamo tornare indietro. Definiamo la distanza
\[\dd{l}=\frac{c}{2}\dd{\tau}\]
dove $d\tau$ è la differenza di tempo proprio in $x^i$ fra la partenza e l'arrivo del raggio di luce. Ovviamente questa definizione operativa di distanza è equivalente alla metrica euclidea nel caso di spazio-tempo di Minkowski.

Vediamo nel caso di $g_{\mu\nu}$ generica. $g_{\mu\nu}\dd{x^\mu}\dd{x^\nu}=0$ visto che il raggio ha velocità $c$.
\[g_{00}d{x_1^0}^2+2g_{0i}dx_1^0\,dx^i+g_{ij}dx^i\,dx^j=0\]
risolvendo per ottenere $dx^0$ abbiamo
\[dx_1^0=\frac{1}{g_{00}}\left(-g_{0i}dx^i+\sqrt{\left(g_{0i}g_{0j}-g_{00}g_{ij}\right)dx^i\,dx^j}\right)\]
abbiamo preso $g_{\mu\nu}$ uguale al valore in $\qty(x^0,x^i)$. (Variazioni di $g_{\mu\nu}$ contribuiscono per termini superiori in $dx^i$.)

Ora consideriamo il ritorno del raggio di luce. Siccome trascuriamo le variazioni di $g_{\mu\nu}$
\[dx_2^0=\frac{1}{g_{00}}\left(g_{0i}dx^i+\sqrt{\left(g_{0i}g_{0j}-g_{00}g_{ij}\right)\,dx^i\,dx^j}\right)\]
quindi
\[dx^0=dx_1^0+dx_2^0=\frac{2}{g_{00}}\sqrt{\left(g_{0i}g_{0j}-g_{00}g_{ij}\right)dx^i\,dx^j}\qquad d\tau=\frac{\sqrt{g_{00}}}{c}\,dx^0\]
\begin{equation}
	\dd{l}^2=\gamma_{ij}\dd{x^i}\dd{x^j}=\qty(\frac{g_{0i}g_{0j}}{g_{00}}-g_{ij})\dd{x^i}\dd{x^j}
\end{equation}
$\gamma_{ij}$ la interpretiamo come \textit{metrica spaziale locale}. Se $g_{\mu\nu}=\eta_{\mu\nu}$ abbiamo $\gamma_{ij}=\delta_{ij}$, cioè la metrica euclidea. Per un campo gravitazionale \textit{statico}, $\p_0g_{\mu\nu}=0$ e $g_{0i}=0$, abbiamo $\gamma_{ij}=-g_{ij}$
\[\left[g_{\mu\nu}\right]=\left(\begin{array}{c|c c c} g_{00} & & 0 & \\ \hline & & & \\ 0 & & -\gamma_{ij} & \\ & & & \end{array}\right)\]
Se il campo è costante, $\p_0g_{\mu\nu}=0$, possiamo definire una lunghezza spaziale anche se non è infinitesima:
\[\Delta l=\int\sqrt{\gamma_{ij}\,dx^i\,dx^j}\]
con l'integrale fatto su una linea $x^i(\lambda)$.

Verifichiamo che $\gamma_{ij}$ è invariante per un cambio di coordinate che lascia $\p_0g_{\mu\nu}=0$.
\[g'_{00}=\alpha^2g_{00}\qquad g'_{0i}=\alpha\frac{\p\beta}{\p x'^i}g_{00}+\alpha\frac{\p x^k}{\p x'^i}g_{0k}\]
\[g'_{ij}=\frac{\p\beta}{\p x'^i}\frac{\p\beta}{\p x'^j}g_{00}+\frac{\p\beta}{\p x'^i}\frac{\p x^k}{\p x'^j}g_{0k}+\frac{\p\beta}{\p x'^j}\frac{\p x^k}{\p x'^i}g_{0k}+\frac{\p x^k}{\p x'^i}\frac{\p x^s}{\p x'^j}g_{ks}\]
\[\begin{split}
\gamma'_{ij}&=\frac{g'_{0i}-g'_{0j}}{g'_{00}}-g'_{ij}=\frac{1}{g_{00}}\left(\frac{\p\beta}{\p x'^i}g_{00}+\frac{\p x^k}{\p x'^i}g_{0k}\right)\left(\frac{\p\beta}{\p x'^j}g_{00}+\frac{\p x^s}{\p x'^j}g_{0s}\right)-g'_{ij}=\\
&=\frac{\p x^k}{\p x'^i}\frac{\p x^s}{\p x'^j}\left(\frac{g_{0k}g_{0s}}{g_{00}}-g_{ks}\right)=\frac{\p x^k}{\p x'^i}\frac{\p x^s}{\p x'^j}\gamma_{ks}\end{split}\]
quindi $\gamma_{ij}$ trasforma proprio come una metrica spaziale.