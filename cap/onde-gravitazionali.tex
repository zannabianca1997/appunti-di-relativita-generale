\chapter[Onde gravitazionali]{Onde gravitazionali}

Il campo è debole se la metrica è "quasi" quella di Minkowsky a meno di una piccola fluttuazione. Quindi si può portare la metrica nella forma
\[g_{\mu\nu}=\eta_{\mu\nu}+h_{\mu\nu}\qquad\text{con} \quad \abs{h_{\mu\nu}}\ll1\]
Possiamo quindi approssimare le equazioni di Einstein tenendo solo i termini lineari in $h_{\mu\nu}$.
\[R^{(1)}_{\mu\nu\rho\gamma}=\frac{1}{2}\qty\Big(\p_\mu\p_\gamma h_{\nu\rho}+\p_\nu\p_\rho h_{\mu\gamma}-\p_\mu\p_\rho h_{\nu\gamma}-\p_\nu\p_\gamma h_{\mu\rho})\]
è utile usare ora la convenzione di alzare e abbassare gli indici con $\eta_{\mu\nu}$. Quindi interpretiamo $h_{\mu\nu}$ come un campo che vive nello spazio piatto.
\[R^{(1)}_{\nu\gamma}=\frac{1}{2}\qty\Big(\p^\mu\p_\gamma h_{\nu\mu}+\p^\mu\p_\nu h_{\gamma\mu}-\p_\mu\p^\mu h_{\nu\gamma}-\p_\nu\p_\gamma h)\qquad h\equiv\eta^{\mu\nu}h_{\mu\nu}\]
\[R^{(1)}=\p^\mu\p^\gamma h_{\mu\gamma}-\p_\mu\p^\mu h\]
L'equazione di Einstein \ref{9.1}, al primo ordine in $h$, diventa:
\[G^{(1)}_{\mu\nu}=\frac{8\pi G_N}{c^4}T_{\mu\nu}+\order{h^2}\]
trascurando i termini non lineari in $h_{\mu\nu}$
\[\frac{1}{2}\p^\rho\p_\mu h_{\rho\nu}+\frac{1}{2}\p^\rho\p_\nu h_{\rho\mu}-\frac{1}{2}\p_\rho\p^\rho h_{\mu\nu}-\frac{1}{2}\p_\mu\p_\nu h-\frac{1}{2}\eta_{\mu\nu}\qty\big(\p^\rho\p^\gamma h_{\rho\gamma}-\p_\rho\p^\rho h)=\frac{8\pi G_N}{c^4}T_{\mu\nu}\]
è conveniente usare
\[\hb_{\mu\nu}=h_{\mu\nu}-\frac{1}{2}\eta_{\mu\nu}h\]
\begin{equation}\label{12.1}
	\frac{1}{2}\p^\rho\p_\mu \hb_{\rho\nu}+\frac{1}{2}\p^\rho\p_\nu \hb_{\rho\mu}-\frac{1}{2}\p_\rho\p^\rho \hb_{\mu\nu}-\frac{1}{2}\eta_{\mu\nu}\p^\rho\p^\gamma \hb_{\rho\gamma}=\frac{8\pi G_N}{c^4}T_{\mu\nu}
\end{equation}
per semplificare l'equazione possiamo usare l'invarianza sotto cambio di coordinate.


\section{Soluzioni delle equazioni di Einstein}

\subsection{Gauge di Lorenz}

\subsubsection{Elettromagnetismo}
È utile vedere prima il caso dell'elettromagnetismo. Ora abbiamo già dall'inizio un'equazione lineare
\[\p_\mu F^{\mu\nu}=\p_\mu\p^\mu A^\nu-\p_\mu\p^\nu A^\mu=\frac{4\pi}{c}J^\nu\]
Usiamo l'invarianza di gauge $A_\mu\ra A_\mu-\p_\mu f$ per portare $A_\mu$ nella forma in cui $\p_\mu A^\mu=0$. Questa scelta è chiamata gauge di Lorenz. L'equazione diventa
\begin{equation}\label{12.2}
	\p_\mu\p^\mu A^\nu=\frac{4\pi}{c}J^\nu
\end{equation}
questa scelta ancora lascia una libertà di gauge $A_\mu\ra A_\mu-\p_\mu f$ con $f$ che soddisfa $\p_\mu\p^\mu f=0$.

\subsubsection{Gravitazione}
Vogliamo ora fare la stessa cosa per il caso gravitazionale. Ora la trasformazione di gauge corrisponde al cambio di coordinate. Per un diffeomorfismo infinitesimo avevamo visto che vale l'equazione \ref{9.2}:
\[g'_{\mu\nu}(x^\rho)=g_{\mu\nu}(x^\rho)-D_\mu\xi_\nu-D_\nu\xi_\mu \qquad \xmp(\xr)=\xm+\xi\qty(\xr)\]
per un campo debole questa diventa
\[h'_{\mu\nu}=h_{\mu\nu}-\p_\mu\xi_\nu-\p_\nu\xi_\mu\]
Questa corrisponde alla trasformazione di gauge $A'_\mu=A_\mu-\p_\mu f$ in elettromagnetismo. Ora però abbiamo $4$ funzioni $\xi_\mu$ da poter scegliere.
\[\begin{split}\hb'_{\mu\nu}&=h'_{\mu\nu}-\frac{1}{2}\eta_{\mu\nu}h'=h_{\mu\nu}-\p_{\mu}\xi_\nu-\p_{\nu}\xi_\mu-\frac{1}{2}\eta_{\mu\nu}(h-2\p_\rho\xi^\rho)=\\&=\hb_{\mu\nu}-\p_\mu\xi_\nu-\p_\nu\xi_\mu+\eta_{\mu\nu}\p_\rho\xi^\rho\end{split}\]
L'equivalente della gauge di Lorenz è ora $\p^\mu\hb_{\mu\nu}=0$ con questa scelta l'equazione \ref{12.1} diventa
\begin{equation}\label{12.3}
	\boxed{\p_\rho\p^\rho\hb_{\mu\nu}=-\frac{16\pi G_N}{c^4}T_{\mu\nu}}
\end{equation}
quindi otteniamo proprio l'equazione delle onde per $\hb_{\mu\nu}$ con $T_{\mu\nu}$ come sorgente.

Se $\hb_{\mu\nu}$ è generica, per ottenere un $\hb'_{\mu\nu}$ che soddisfi $\p^\mu\hb'_{\mu\nu}=0$ dobbiamo scegliere $\xi^\mu$ tale che
\[0=\p^\mu\hb_{\mu\nu}-\p^\mu\p_\mu\xi_\nu-\p^\mu\p_\nu\xi_\mu+\p^\mu\eta_{\mu\nu}\p_\rho\xi^\rho\]
quindi $\p^\mu\hb_{\mu\nu}=\p^\mu\p_\mu\xi_\nu$ questa si può risolvere e così portare $\hb'_{\mu\nu}$ nella gauge a divergenza nulla. Ancora abbiamo una libertà residua nella trasformazione di gauge se usiamo $\xi^\mu$ con $\p^\mu\p_\mu\xi_\nu=0$.


\subsection{Soluzioni nel vuoto}
Ora studiamo le soluzioni in assenza di sorgenti ($J_\nu=0$ e $T_{\mu\nu}=0$)

\subsubsection{Onde elettromagnetiche}
Nel caso dell'elettrodinamica, nella gauge di Lorenz, dalla \ref{12.2}, abbiamo
\[\p_\mu\p^\mu A_\nu=0\]
la cui soluzione generica si può esprimere come sovrapposizione lineare di onde piane monocromatiche\footnote{Questo si può fare perchè le onde piane monocromatiche sono una base completa, inoltre esprimere la soluzione come sovrapposizione lineare di onde piane equivale a passare in trasformata di Fourier.}. Per quest'ultime scegliamo un vettore d'onda $k^\mu$ con $k^\mu k_\mu=0$, $k^\mu=\qty(\frac{\omega}{c},\va{k})$, $\omega^2=c^2\abs{\va{k}}^2$
\[A_\mu(x^\rho)=\Re\qty(a_\mu e^{ik_\mu x^\mu})=\Re\qty(a_\mu e^{i(\omega t-\va{k}\vdot\va{x})})\]
dove $a_\mu$ costante. D'ora in poi per semplicità omettiamo di scrivere $\Re\qty(\,\cdot\,)$, comunque dobbiamo ricordarci che alla fine dei conti occorre sempre prendere la parte reale. La gauge di Lorenz $\p_\mu A^\mu=0\implies k_\mu a^\mu=0$ questa ha $3$ soluzioni linearmente indipendenti $a^\mu\propto k^\mu$ e $a^\mu\propto\qty(0,\va{k}_\perp)$ dove $\va{k}_\perp$ è un generico vettore spaziale ortogonale a $\va{k}$, $\va{k}_\perp\vdot\va{k}=0$. Ancora possiamo usare la gauge residua $A_\mu\ra A_\mu-\p_\mu f$ con $\p_\mu\p^\mu f=0$, scegliendo $f\propto e^{ik_\mu x^\mu}$ possiamo eliminare tutte le soluzioni del tipo $a^\mu\propto k^\mu$. Abbiamo quindi $2$ possibili soluzioni fisiche $a^\mu\propto\qty(0,\va{k}_\perp)$. $a^\mu\in\mathbb{C}^2$, lo spazio vettoriale $\mathbb{C}^2$ tiene conto delle due possibili polarizzazioni e delle loro fasi. Se prendiamo $k^\mu=\qty(\frac{\omega}{c},k,0,0)$ le due soluzioni sono $a^\mu=\qty(0,0,a^2,a^3)$. La polarizzazione lungo l'asse $x^2$ ($a^3=0$):
\[A_2=a_2e^{ik(x^0-x^1)}\qquad F_{02}=E_2=ika_2e^{ik(x^0-x^1)}\qquad F_{21}=B_3=ika_2e^{ik(x^0-x^1)}\]
ruotando di $\pi/2$ attorno all'asse $x^1$ otteniamo l'altra polarizzazione. Ruotando di $\pi$ ritorniamo alla stessa soluzione ma con un segno meno.
\begin{center}
	\begin{tikzpicture}[x={(-10:1cm)},y={(90:1cm)},z={(210:1cm)}]
	% Axes
	\draw (-1,0,0) node[above] {$x$} -- (5,0,0);
	\draw (0,0,0) -- (0,2,0) node[above] {$y$};
	\draw (0,0,0) -- (0,0,2) node[left] {$z$};
	% Propagation
	\draw[->,ultra thick] (5,0,0) -- node[above] {$c$} (6,0,0);
	% Waves
	\draw[thick] plot[domain=0:4.5,samples=200] (\x,{cos(deg(pi*\x))},0);
	\draw[gray,thick] plot[domain=0:4.5,samples=200] (\x,0,{cos(deg(pi*\x))});
	% Arrows
	\foreach \x in {0.1,0.3,...,4.4} {
		\draw[->,help lines] (\x,0,0) -- (\x,{cos(deg(pi*\x))},0);
		\draw[->,help lines] (\x,0,0) -- (\x,0,{cos(deg(pi*\x))});
	}
	% Labels
	\node[above right] at (0,1,0) {$\va{E}$};
	\node[below] at (0,0,1) {$\va{B}$};
	\end{tikzpicture}
\end{center}


\subsubsection{Onde gravitazionali}
Ora vediamo le onde gravitazionali nel vuoto (soluzioni della \ref{12.3} con $T_{\mu\nu}=0$)
\[\p_\rho\p^\rho\hb_{\mu\nu}=0\implies\hb_{\mu\nu}=H_{\mu\nu}e^{ik_\mu x^\mu}\]
come prima $k_\mu k^\mu=0$ e $k^\mu=\qty(\frac{\omega}{c},\va{k})$. Imponendo l'equivalente della gauge di Lorenz, $\p^\mu\hb_{\mu\nu}=0$, otteniamo la condizione $k^\mu H_{\mu\nu}=0$. Abbiamo quindi $10-4=6$ scelte linearmente indipendenti\footnote{È importante ricordare che $H_{\mu\nu}$ è simmetrica, quindi $H_{\mu\nu}=H_{\nu\mu}$. Questo significa che se scriviamo $H_{\mu\nu}=A_\mu B_\nu$, per la condizione $k^\mu H_{\mu\nu}=0$, $A_\mu$ deve appartenere allo spazio di dimensione $W_\mu=\text{span}\qty{k_\mu,k^1_{\perp\mu},k^2_{\perp\mu}}$ di dimensione $3$ e, per simmetria, anche $B_\nu$ deve appartenere a $W_\nu$. La dimensione del sottospazio simmetrico del prodotto tensore di $W_\mu$ e $W_\nu$ è data dalle combinazioni con ripetizione di $3$ elementi di classe $2$, $\binom{3+2-1}{2}=6$. I vettori di base sono: $k_\mu k_\nu$, $k_\mu k^1_{\perp\nu}+k_\nu k^1_{\perp\mu}$, $k_\mu k^2_{\perp\nu}+k_\nu k^2_{\perp\mu}$, $k^1_{\perp\mu}k^1_{\perp\nu}$, $k^1_{\perp\mu}k^2_{\perp\nu}+k^1_{\perp\nu}k^2_{\perp\mu}$, $k^2_{\perp\mu}k^2_{\perp\nu}$, dove abbiamo simmetrizzato i termini opportuni e chiamato $k^1_{\perp\mu}$ e $k^2_{\perp\mu}$ due vettori di base dello spazio bidimensionale a cui appartiene $k_{\mu\perp}=\qty(0,-\va{k}_\perp)$.}.
Se chiamiamo $k^\mu_\perp=\qty(0,\va{k}_\perp)$, dove $\va{k}\vdot\va{k}_\perp=0$, $3$ delle $6$ sono $H_{\mu\nu}\propto k_\mu k_\nu$, $H_{\mu\nu}\propto k_\mu k_{\perp\nu}+k_\nu k_{\perp\mu}$. Le altre $3$ sono puramente spaziali $H_{\mu\nu}\propto k^{(1)}_{\perp\mu}k^{(2)}_{\perp\nu}+k^{(1)}_{\perp\nu}k^{(2)}_{\perp\mu}$, ovvero una matrice $2\times2$ simmetrica nel sottospazio $\va{k}_\perp$.

Ancora possiamo semplificare con una trasformazione
\[\hb'_{\mu\nu}=\hb_{\mu\nu}-\p_\mu\xi_\nu-\p_\nu\xi_\mu+\eta_{\mu\nu}\p_\rho\xi^\rho\qquad\text{con}\quad\p^\mu\p_\mu\xi_\nu=0\]
Se prendiamo $\xi_\mu=\E_\mu e^{ik_\mu x^\mu}$ con $\E_\mu$ arbitrario
\[H'_{\mu\nu}=H_{\mu\nu}-ik_\mu\E_\nu-ik_\nu\E_\mu+i\eta_{\mu\nu}k_\rho\E^\rho\]
se prendiamo $\E_\mu\propto k_\mu$ o $k_{\perp\mu}$ possiamo eliminare le prime tre sopra. Inoltre possiamo sempre annullare la traccia, basta scegliere (ricordando $\tensor{\eta}{^\nu_\nu}=4$) \[\tensor{{H'}}{^\mu_\mu}=0=\tensor{H}{^\mu_\mu}+2ik_\mu\E^\mu=0\]
Quindi rimangono $6-3-1=2$ gradi di libertà. Se prendiamo $k^\mu=\qty(\frac{\omega}{c},k,0,0)$
\[\hb_{\mu\nu}=h_{\mu\nu}=H_{\mu\nu}e^{ik_\rho x^\rho}\qquad\qty[H_{\mu\nu}]=\mqty(0&0\\0&\mqty{H_+&H_x\\H_x&-H_+})\]
abbiamo due polarizzazioni trasverse, come nell'elettromagnetismo.

Ora però la trasformazione rispetto alle rotazioni è diversa. Una rotazione di $\pi/4$ attorno all'asse di propagazione $x^1$ trasforma le polarizzazioni una nell'altra. (Quantisticamente il quanto di onda elettromagnetica, il fotone, ha spin 1 mentre il quanto di onda gravitazionale, il gravitone, ha spin 2).

Calcoliamo la curvatura per l'onda $+$
\[R^{(1)}_{(+)0202}=-\frac{1}{2}\p_0^2h_{22}=\frac{1}{2}k^2H_+e^{ik(x^0-x^1)}=-R^{(1)}_{(+)0303}\]
\[R^{(1)}_{(+)0212}=-\frac{1}{2}\p_0\p_1h_{22}=\frac{1}{2}k^2H_+e^{ik(x^0-x^1)}=-R^{(1)}_{(+)0313}\]
\[R^{(1)}_{(+)1212}=-\frac{1}{2}\p_1^2h_{22}=\frac{1}{2}k^2H_+e^{ik(x^0-x^1)}=-R^{(1)}_{(+)1313}\]
Abbiamo quindi verificato che sono onde "fisiche", la curvatura è non nulla e si propaga come un'onda.


\subsection{Soluzioni con sorgenti}
Studiamo ora la generazione delle onde dalle sorgenti. Per questo ci serve la soluzione di
\[\p_\mu\p^\mu\psi=\qty(\p_0^2-\Delta)\psi=f\]
Nel caso statico ($\p_0\psi=0$) la soluzione è
\[\psi(\va{r})=\frac{1}{4\pi}\int\frac{f(\va{r}')}{\qty|\va{r}-\va{r}'|}\dd{V'}\qquad\Delta\psi(\va{r})=\frac{1}{4\pi}\int f(\va{r}')(-4\pi)\delta^{(3)}\qty(\va{r}-\va{r}')\dd{V'}=-f(\va{r})\]
Nel caso dinamico dobbiamo usare il potenziale ritardato
\[\psi(x^0,\va{r})=\frac{1}{4\pi}\int\frac{f(x'^0,\va{r}')}{\qty|\va{r}-\va{r}'|}\dd{V'}\qquad\text{dove}\quad x'^0=x^0-\qty|\va{r}-\va{r}'|\]
\[\begin{split}\p_\mu\p^\mu\psi&=\frac{1}{4\pi}\int\qty(\p_0^2-\Delta)\frac{f(x'^0,\va{r}')}{\qty|\va{r}-\va{r}'|}\dd{V'}=\\&=\frac{1}{4\pi}\int\qty(\frac{\ddot{f}}{\qty|\va{r}-\va{r}'|}-\div\qty(-\frac{\va{r}-\va{r}'}{\qty|\va{r}-\va{r}'|^3}f-\frac{\va{r}-\va{r}'}{\qty|\va{r}-\va{r}'|^2}\dot{f}))\dd{V'}=\\&=\frac{1}{4\pi}\int\qty(\bcancel{\frac{\ddot{f}}{\qty|\va{r}-\va{r}'|}}+4\pi\delta^{(3)}\qty(\va{r}-\va{r}')-\bcancel{\frac{1}{\qty|\va{r}-\va{r}'|^2}\ddot{f}}+\bcancel{\frac{1}{\qty|\va{r}-\va{r}'|^2}\ddot{f}}-\bcancel{\frac{\ddot{f}}{\qty|\va{r}-\va{r}'|}})=f\end{split}\]


\subsubsection{Caso elettromagnetico}
Nel caso dell'elettrodinamica $\p_\mu\p^\mu A_\nu=\frac{4\pi}{c}J_\nu$
\[A_\nu\qty(x^0,\va{r})=\frac{1}{c}\int\frac{J_\nu(x'^0,\va{r}')}{\qty|\va{r}-\va{r}'|}\dd{V'}\]
dobbiamo anche verificare $\p_\nu A^\nu=0$
\[\p_\nu A^\nu=\frac{1}{c}\int\qty(\frac{\p_0J^0}{\qty|\va{r}-\va{r}'|}+\div\qty(\frac{\va{J}}{\qty|\va{r}-\va{r}'|}))\dd{V'}=\frac{1}{c}\int(-)\grad'\cdot\qty(\frac{\va{J}}{\qty|\va{r}-\va{r}'|})\dd{V'}\]
dove abbimo usato $\p_\mu J^\mu=0$\footnote{e in particolare la conservazione della carica. Indicando $\div$ con $\pdv{\va{r}}$ abbiamo che $\pdv{\va{r}}=\pdv{\va{r}'}\pdv{\va{r}'}{x'^0}\pdv{x'^0}{\va{r}}=\pdv{\va{r}'}\qty(\pdv{x'^0}{\va{r}'})^{-1}\pdv{x'^0}{\va{r}}=\pdv{\va{r}'}\qty(\frac{\va{r}-\va{r}'}{\abs{\va{r}-\va{r}'}})^{-1}\qty(\frac{\va{r}-\va{r}'}{\abs{\va{r}-\va{r}'}})=-\pdv{\va{r}'}$
quindi $\div = -\grad'\cdot$}. Quindi $\p_\nu A^\nu=-\frac{1}{c}\int\dd{\va{S}'}\frac{\va{J}}{\qty|\va{r}-\va{r}'|}=0$ per sorgenti localizzate\footnote{se infatti prendiamo una superficie sufficientemente grande $\va{J}$ sulla superficie è nulla e quindi l'integrale vale $0$}.


\subsubsection{Caso gravitazionale}
Nel caso gravitazionale $\p_\rho\p^\rho\hb_{\mu\nu}=-\frac{16\pi G_N}{c^4}T_{\mu\nu}$
\[\hb_{\mu\nu}(x^0,\va{r})=-\frac{4G_N}{c^4}\int\frac{T_{\mu\nu}(x'^0,\va{r}')}{\qty|\va{r}-\va{r}'|}\dd{V'}\]
dobbiamo verificare $\p^\mu\hb_{\mu\nu}=0$
\[\p_\mu\hb^{\mu\nu}=-\frac{4G_N}{c^4}\int\qty(\frac{\p_0T^{00}}{\qty|\va{r}-\va{r}'|})+\p_i\qty(\frac{T^{0i}}{\qty|\va{r}-\va{r}'|})\dd{V'}=\frac{4G_N}{c^4}\int\p_i'\qty(\frac{T^{0i}}{\qty|\va{r}-\va{r}'|})\dd{V'}=0\]
abbiamo usato $\p_\mu T^{\mu\nu}=0$ e $T^{\mu\nu}$ localizzato.

In realtà $D_\mu T^{\mu\nu}=\p_\mu T^{\mu\nu}+\Gamma^\mu_{\lambda\mu}T^{\lambda\nu}+\Gamma^\nu_{\mu\lambda}T^{\mu\lambda}=0$ quindi $\p_\mu T^{\mu\nu}$ non è esattamente nullo. Usando $\Gamma^{\mu(1)}_{\nu\rho}=\frac{1}{2}\qty(\p_\nu\tensor{h}{^\mu_\rho}+\p_\rho\tensor{h}{^\mu_\nu}-\p^\mu h_{\nu\rho})$ abbiamo al prim'ordine in $h$
\[\p_\mu T^{\mu\nu}=-\frac{1}{2}\p_\mu h T^{\mu\nu}-\p_\mu\tensor{h}{^\nu_\lambda}T^{\mu\lambda}+\frac{1}{2}\p^\nu h_{\lambda\mu}T^{\lambda\mu}\]
comunque la correzione a $\p_\mu T^{\mu\nu}=0$ è di $\order{h}$ quindi possiamo trascurarlo nella equazione linearizzata.


\section{Zona di radiazione}
Ci sono $3$ scale di lunghezza nel problema della radiazione:
\begin{itemize}
	\item $l$: la dimensione della sorgente radiativa
	\item $\lambda$: la lunghezza d'onda prodotta
	\item $\abs{\va{r}-\va{r}'}$: la distanza dalla sorgente al punto in cui misuriamo la radiazione
\end{itemize}
Per sorgenti relativistiche $l\simeq\lambda$ altrimenti in genere $l\simeq\lambda\frac{v}{c}$. La \textit{zona di radiazione} è $\qty|\va{r}-\va{r}'|\gg\lambda,l$. In questo regime possiamo approssimare $\qty|\va{r}-\va{r}'|\simeq r-\va{r}'\vdot\vu{r}$. Facciamo inoltre una decomposizione spettrale della sorgente% (DISEGNO)
\[f(x'^0,\va{r}')=\int\hat{f}(\omega,\va{r}')e^{-i\omega t'}\dd{\omega}\]
La soluzione diventa quindi
\[\begin{split}\psi(x^0,\va{r})&=\frac{1}{4\pi}\int\dd{V'}\int\dd{\omega}\hat{f}(\omega,\va{r}')e^{-ikx'^0}\frac{1}{\qty|\va{r}-\va{r}'|}=\\&\simeq\frac{1}{4\pi}\int\dd{\omega}\int\dd{V'}\hat{f}(\omega,\va{r}')e^{-ik(x^0-r+\va{r}'\vdot\vu{r})}\frac{1}{r-\va{r}'\vdot\vu{r}}=\\&\simeq\frac{1}{4\pi}\int\dd{\omega}\frac{e^{-ik(x^0-r)}}{r}\int\dd{V'}\hat{f}(\omega,\va{r}')e^{-ik\va{r}'\vdot\vu{r}}\end{split}\]
il termine dominante decade come $\propto\frac{1}{r}$ dalla sorgente; $e^{ik\qty(x^0-r)}/r$ è un'onda radiale uscente. La dipendenza angolare è contenuta in $e^{-ik\va{r}'\vdot\vu{r}}$.


\subsection{Approssimazione non relativistica}
Per sorgenti non relativistiche $l\ll\lambda$ possiamo espandere\footnote{$k=\frac{\omega}{T}=\frac{2\pi}{\lambda}$, $r'\sim l$ $\implies$ $kr'\sim\frac{l}{\lambda}\ll1$} $e^{-ik\va{r}'\vdot\vu{r}}\simeq1-ik\va{r}'\vdot\vu{r}$. Abbiamo quindi che
\[\psi(x^0,\va{r})\simeq\frac{1}{4\pi}\int\dd{\omega}\frac{e^{-ik(x^0-r)}}{r}\int\dd{V'}\hat{f}(\omega,\va{r}')=\frac{1}{4\pi}\frac{1}{r}\int\dd{V'}f(x^0-r,\va{r}')\]

\subsubsection{Radiazione di dipolo elettrico}
Applicando questa approssimazione all'elettromagnetismo
\[A_i(x^0,\va{r})\simeq\frac{1}{c}\frac{1}{r}\int\dd{V'}J_i(x^0-r,\va{r}')\]
Trasformiamo l'integrale usando $\int\dd{V}J_i=\int\dd{V}\qty(\p^k(x_i J_k)-x_i\p^k J_k)$. Il primo pezzo diventa\footnote{usando anche $\p^kx_i=\delta_i^k$} $\int\dd{S}^k x_i J_k$ ed è nullo per sorgenti localizzate. Per il secondo usiamo $\p^\mu J_\mu=\p^0J_0+\p^kJ_k=0$ quindi ($\p^0=\p_0$)
\[A_i(x^0,\va{r})=\frac{1}{cr}\int\dd{V'}x'_i\p_0J_0=\frac{1}{r}\p_0P_i\]
dove $P_i=\frac{1}{c}\int\dd{V'}x'_iJ_0$ è il dipolo elettrico. Questa si chiama infatti \textit{radiazione di dipolo}. A grande distanza l'onda diventa approssimabile localmente da un'onda piana e l'onda "fisica" è data dalla componente di $A_i$ ortogonale a $r_i$. Prendiamo $P_i=Px_3$ e $\theta$ l'angolo azimutale $\tan\theta=\frac{\sqrt{{x^1}^2+{x^2}^2}}{x^3}$. Le componenti dominanti dei campi elettromagnetici sono
\[\va{E}\simeq-\vu*{\theta}\frac{1}{r}\sin\theta\p_0^2P\qquad\va{B}\simeq-\vu*{\vp}\frac{1}{r}\sin\theta\p_0^2P\]

\begin{center}
	\begin{tikzpicture}[scale=1.2]
	\begin{axis}[view/h=45,axis lines = none,unit vector ratio=1 1 1]
	\addplot3[domain=0:360,domain y=0:360,samples=31,
	colormap/blackwhite,surf,%mesh,point meta=1, %<-if you want a mesh
	z buffer=sort]
	({(sin(x+90)*sin(x+90))*cos(y)}, 
	{(sin(x+90)*sin(x+90))*sin(y)}, 
	{(sin(x+90)*sin(x+90))*sin(x)});
	\end{axis}
	\path(current bounding box.south west) rectangle (current bounding box.north
	east);
	\end{tikzpicture}
	\hspace{25pt}
	\begin{tikzpicture}[scale=1.2]
	\begin{polaraxis}[axis lines = none]
		\addplot[domain=0:360,samples=73,smooth] (x+90,{sin(x)*sin(x)});
	\end{polaraxis}
	\draw[-stealth] ([yshift=2cm]current axis.south) -- ([yshift=-2cm]current axis.north);
	\draw[-stealth] (current axis.west) -- (current axis.east);
\end{tikzpicture}
\end{center}


\subsubsection{Radiazione di quadrupolo gravitazionale}
Ora applichiamo l'approssimazione $r\gg\lambda\gg l$ alla gravità
\[\hb_{ij}(x^0,\va{r})\simeq-\frac{4G_N}{c^4}\frac{1}{r}\int\dd{V'}T_{ij}(x^0-r,\va{r}')\]
riscriviamo l'integrale spaziale
\[\int\dd{V}T_{ij}=\int\dd{V}\qty\Big(\p^k\qty\big(x_iT_{jk})-x_i\p^kT_{jk})=\int\dd{V}x_i\p_0T_{j0}\]
dove abbiamo usato $T_{ij}$ localizzato e $\p^\mu T_{\mu\nu}=0$. Simmetrizzando otteniamo:
\[\hb_{ij}\simeq-\frac{2G_N}{c^4}\frac{1}{r}\p_0\qty(\int\dd{V'}\qty\big(x_i'T_{j0}+x_j'T_{i0}))\]
Ancora possiamo modificare l'integrale
\[\int\dd{V}\qty\big(x_iT_{j0}+x_jT_{i0})=\int\dd{V}\qty\Big(\p^k\qty\big(x_ix_jT_{k0})-x_ix_j\p^kT_{k0})=\int\dd{V}x_ix_j\p_0T_{00}\]
quindi
\[\hb_{ij}\simeq-\frac{2G_N}{c^4}\frac{1}{r}\p_0^2I_{ij}\qquad\text{dove}\quad I_{ij}=\int\dd{V'}x'_ix'_jT_{00}\]
A grande distanza l'onda è localmente piana e l'onda "fisica" è contenuta nelle componenti di $\hb_{ij}$ trasverse e a traccia nulla
\begin{equation}
	\hb_{ij}=h_{ij}\simeq-\frac{2G_N}{3c^4}\frac{1}{r}\p_t^2Q_{ij}
\end{equation}
dove $Q_{ij}$ è il momento di quadrupolo della distribuzione di $\text{energia}/c^2$:
\[Q_{ij}=\int\dd{V'}\qty(3x'_ix'_j-r'^2\delta_{ij})\frac{T_{00}}{c^2}\]
Questa si chiama infatti \textit{radiazione di quadrupolo}.


\subsection{Esempio: sistema binario}
Prendiamo ad esempio due masse $M$ che ruotano in un'orbita circolare a distanza $R$ nel piano $x^3=0$. Abbiamo $F=\frac{G_N M^2}{(2R)^2}=\omega^2RM$. Quindi la frequenza è $\omega=\frac{G_N^{1/2}M^{1/2}}{2R^{3/2}}$.

\begin{wrapfigure}{r}{0pt}%{0.25\textwidth}
	\centering
	\vspace{-10pt}
	\begin{tikzpicture}[scale=1.1]
	%Orbita
	\draw[red,thick,dashed] (0,0) circle (1cm);
	%Masse
	\fill (-1,0) circle (0.1) node[anchor=east] {$M$};
	\fill (1,0) circle (0.1) node[anchor=west] {$M$};
	%Frecce
	\draw [->] (0,0) -- (60:1) node[anchor=north east] {$R\ $};
	\draw [->] (-1,0) -- (225:{sqrt(2)});
	\draw [->] (1,0) -- (45:{sqrt(2)});
	\end{tikzpicture}
	%\vspace{-10pt}
\end{wrapfigure}
\noindent
\[T_{00}=Mc^2\delta^{(3)}\qty(\va{r}-\va{r}_M)+Mc^2\delta^{(3)}\qty(\va{r}+\va{r}_M)\]
con $\va{r}_M=\qty(R\cos\omega t,R\sin\omega t,0)$
\[\qty[Q_{ij}]=2MR^2\qty(\mqty{3\cos^2\omega t-1&3\cos\omega t\sin\omega t&0\\3\cos\omega t\sin\omega t&3\sin^2\omega t-1&0\\0&0&-1})\]
Possiamo innanzitutto fare una stima dell'ordine di grandezza di $h_{ij}$.
\[\order{h_{ij}}\simeq2\frac{G_N}{c^4}\frac{1}{r}\omega^2MR^2=\frac{1}{2}\frac{G_N^2M^2}{c^4rR}\]
Prendiamo ad esempio $M=\alpha M_\odot$, $R=\beta r_S=\beta\frac{2G_NM}{c^2}$ e una distanza $r=\gamma\SI{100}{\mega\parsec}$. (Megaparsec è una tipica distanza tra le galassie, $\SI{100}{\mega\parsec}$ una tipica distanza cosmologica, $\SI{1}{\parsec}\simeq\SI{3e16}{\metre}$.)
\[\order{h_{ij}}\simeq\frac{1}{8}\frac{\alpha}{\gamma\beta}\frac{2G_NM_\odot}{c^2\SI{100}{\mega\parsec}}\simeq\frac{1}{8}\frac{\alpha}{\gamma\beta}\frac{\SI{3e3}{\metre}}{\SI{3e23}{\metre}}\simeq\frac{1}{8}10^{-20}\frac{\alpha}{\gamma\beta}\]


\section{Energia e impulso nelle onde}%[Energia e impulso]
Studiamo ora l'energia l'impulso nelle onde.

\subsection{Caso elettromagnetico}
Per il caso elettromagnetico conosciamo già il tensore energia-impulso.
\[T^{\mu\nu}=-\frac{1}{4\pi}F^{\mu\rho}\tensor{F}{^\nu_\rho}+\frac{1}{16\pi}\eta^{\mu\nu}F^{\rho\gamma}F_{\rho\gamma}\]
Se prendiamo un'onda monocromatica piana $k^\mu=(\frac{\omega}{c},k,0,0)$, con polarizzazione lineare lungo $\vu{x}^2$
\[A_2=a_2\cos\underbrace{\qty(k(x^0-x^1)-\vp)}_{\textcircled{\scriptsize H}}\]
Le uniche componenti non nulle del tensore $F_{\mu\nu}=\p_\mu A_\nu-\p_\nu A_\mu$ sono ($A_i=-A^i$)
\[F_{02}=E_2=-ka_2\sin\textcircled{\scriptsize H} \qquad F_{21}=B_3=-ka_2\sin\textcircled{\scriptsize H}\]
da cui otteniamo\footnote{tensore indice alto e indice basso}
\[F^{\mu\nu}F_{\mu\nu}=-2\qty(\va{E}^2-\va{B}^2)=0\qquad T^{\mu\nu}=-\frac{1}{4\pi}F^{\mu\rho}\tensor{F}{^\nu_\rho}\]
\[\resizebox{\hsize}{!}{$T^{00}=-\frac{1}{4\pi}F^{02}\tensor{F}{^0_2}=\frac{k^2a_2^2}{4\pi}\sin^2\textcircled{\scriptsize H}=T^{11} \qquad T^{01}=-\frac{1}{4\pi}F^{02}\tensor{F}{^1_2}=\frac{k^2a_2^2}{4\pi}\sin^2\textcircled{\scriptsize H}=T^{10}$}\]
quindi
%\[\qty[T^{\mu\nu}]=\frac{k^2a_2^2}{4\pi}\sin^2\textcircled{\scriptsize H}\mqty(\mqty{\xmat{1}{2}{2}}&\mqty{\xmat{0}{2}{2}}\\\mqty{\xmat{0}{2}{2}}&\mqty{\xmat{0}{2}{2}})\] %matrice grande
\[\qty[T^{\mu\nu}]=\frac{k^2a_2^2}{4\pi}\sin^2\textcircled{\scriptsize H}\mqty(\mqty{\xmat{1}{2}{2}}&\mqty{0}\\\mqty{0}&\mqty{0})\qquad\ev{\qty[T^{\mu\nu}]}=\frac{k^2a_2^2}{8\pi}\mqty(\mqty{\xmat{1}{2}{2}}&\mqty{0}\\\mqty{0}&\mqty{0})\]
possiamo quindi interpretarlo anche come un flusso di particelle a massa nulla $\mathcal{P}^\mu\propto\qty(\frac{E}{c},\frac{E}{c},0,0)$ che si propagano nella direzione $x^1$. Per $k^\mu$ generico vale invece $\ev{T^{\mu\nu}}=k^\mu k^\nu\abs{a}^2/(8\pi)$.


\subsection{Caso gravitazionale}
Per ottenere l'analogo gravitazionale dobbiamo innanzitutto introdurre una nozione di $T^{\mu\nu}$ per il campo gravitazionale.

Per questo dobbiamo studiare l'equazione di Einstein oltre l'approssimazione lineare:
\[G_{\mu\nu}=G_{\mu\nu}^{(1)}+G_{\mu\nu}^{(2)}+\ldots=\frac{8\pi G_N}{c^4}T_{\mu\nu}\qquad G_{\mu\nu}-G_{\mu\nu}^{(1)}=G_{\mu\nu}^{(2)}+G_{\mu\nu}^{(3)}+\ldots\]
tutti i termini di ordine $h^2$ o superiore sono contenuti in $G_{\mu\nu}-G_{\mu\nu}^{(1)}$. Possiamo quindi riscrivere l'equazione di Einstein nel seguente modo
\[G_{\mu\nu}^{(1)}=\frac{8\pi G_N}{c^4}\qty\big(T^{\mu\nu}+t^{\mu\nu})\qquad\text{dove}\quad t^{\mu\nu}\equiv\frac{c^4}{8\pi G_N}\qty(G_{\mu\nu}^{(1)}-G_{\mu\nu})\]
$t^{\mu\nu}$ è la nostra nozione di "tensore" energia-impulso associato al campo gravitazionale. Vediamo le proprietà più importanti che giustificano questa scelta. La divergenza di $G_{\mu\nu}^{(1)}$ è:
\[\begin{split}\p^\mu G_{\mu\nu}^{(1)}&=\p^\mu\qty(\frac{1}{2}\p^\rho\p_\mu\hb_{\rho\nu}+\frac{1}{2}\p^\rho\p_\nu\hb_{\mu\mu}-\frac{1}{2}\p^\rho\p_\rho\hb_{\mu\nu}+\frac{1}{2}\eta_{\mu\nu}\p^\rho\p^\gamma\hb_{\rho\gamma})=\\&=\cancel{\frac{1}{2}\p^\rho\p^\mu\p_\mu\hb_{\rho\nu}}+\bcancel{\frac{1}{2}\p_\nu\p^\mu\p^\rho\hb_{\rho\mu}}-\cancel{\frac{1}{2}\p^\mu\p^\rho\p_\rho\hb_{\mu\nu}}-\bcancel{\frac{1}{2}\p_\nu\p^\rho\p^\gamma\hb_{\rho\gamma}}=0\end{split}\]
potevamo ottenerlo anche usando $D^\mu G_{\mu\nu}=0$ e dal fatto che all'ordine lineare $D^\mu G_{\mu\nu}=\p^\mu G_{\mu\nu}^{(1)}+\order{h^2}$, quindi deve valere $\p^\mu G_{\mu\nu}^{(1)}=0$. Applicando la divergenza all'equazione di Einstein ($0=\p^\mu G_{\mu\nu}^{(1)}=\frac{8\pi G_N}{c^4}\p^\mu\qty\big(T_{\mu\nu}+t_{\mu\nu})$) ricaviamo
\[\p^\mu\qty\big(T_{\mu\nu}+t_{\mu\nu})=0\]
otteniamo quindi una quantità conservata. Se chiamiamo
\[T^{\mu\nu}_\text{tot}=T^{\mu\nu}+t^{\mu\nu}\qquad cP^\mu_\text{tot}=\int\dd[3]{x}T^{\mu 0}_\text{tot}\]
abbiamo
\[c\p_0P^\mu_\text{tot}=\int\dd[3]{x}\p_0T^{\mu0}_\text{tot}=-\int\dd[3]{x}\p_iT^{\mu i}_\text{tot}=0\]
quindi $P^\mu_\text{tot}=\qty(\frac{E_\text{tot}}{c},\va{P}_\text{tot})$ è una quantità conservata. Questa è una forma "debole" di tensore energia-impulso, funziona solo per calcolare un $P^\mu_\text{tot}$ globale conservato. In relatività generale questo è il massimo che si può fare, non si possono definire in maniera univoca energia e impulso localizzati. Per campo debole possiamo approssimare $t^{\mu\nu}$ all'ordine quadratico in $h$.
\[g_{\mu\nu}=\eta_{\mu\nu}+h_{\mu\nu}\qquad g^{\mu\nu}=\eta^{\mu\nu}-h^{\mu\nu}+\order{h^2}\]\footnote{si verifica la forma di $g^{\mu\nu}$ dal fatto che $g_{\mu\nu}g^{\mu\nu}=1+\order{h^2}$}
\[\Gamma^\mu_{\nu\rho}\simeq\frac{1}{2}\eta^{\mu\lambda}\qty\Big(\p_\nu h_{\lambda\rho}+\p_\rho h_{\lambda\nu}-\p_\lambda h_{\nu\rho})-\frac{1}{2}h^{\mu\lambda}\qty\Big(\p_\nu h_{\lambda\rho}+\p_\rho h_{\lambda\nu}-\p_\lambda h_{\nu\rho})\]
\[R_{\mu\nu}^{(1)}=\frac{1}{2}\qty\Big(\p^\rho\p_\mu h_{\nu\rho}+\p^\rho\p_\nu h_{\mu\rho}-\p_\rho\p^\rho h_{\mu\nu}-\p_\mu\p_\nu h)\qquad R^{(1)}=\p^\mu\p^\gamma h_{\mu\gamma}-\p_\mu\p^\mu h\]
\[\begin{split}
R_{\mu\nu}^{(2)}&%=...=\\&
%=...+\\&
%+...+\\&
%+...=\\&
=\frac{1}{2}h^{\lambda\rho}\qty\Big(\p_\nu\p_\mu h_{\lambda\rho}+\p_\lambda\p_\rho h_{\mu\nu}-\p_\nu\p_\rho h_{\mu\lambda}-\p_\lambda\p_\mu h_{\nu\rho})+\frac{1}{2}\p_\nu h^{\lambda\rho}\p_\mu h_{\lambda\rho}+\\&
+\frac{1}{2}\qty(\frac{1}{2}\p_\rho h-\p_\lambda\tensor{h}{^\rho_\lambda})\qty\Big(\p_\mu\tensor{h}{_\nu^\rho}+\p_\nu\tensor{h}{_\mu^\rho}-\p^\rho h_{\mu\nu})+\\&
-\frac{1}{4}\qty\Big(\p_\rho\tensor{h}{_\nu^\lambda}+\p_\nu\tensor{h}{_\rho^\lambda}-\p^\lambda_{\rho\nu})\qty\Big(\p_\mu\tensor{h}{_\lambda^\rho}+\p_\lambda\tensor{h}{_\mu^\rho}-\p^\rho h_{\mu\lambda})
\end{split}\]

\[\begin{split}
G_{\mu\nu}&\simeq R_{\mu\nu}^{(1)}+R_{\mu\nu}^{(2)}-\frac{1}{2}\qty(\eta_{\mu\nu}+h_{\mu\nu})\qty(\eta^{\rho\gamma}-h^{\rho\gamma})\qty(R_{\rho\gamma}^{(1)}+R_{\rho\gamma}^{(2)})=\\&
=\underbrace{R_{\mu\nu}^{(1)}-\frac{1}{2}\eta_{\mu\nu}R^{(1)}}_{G_{\nu\mu}^{(1)}}+\underbrace{R_{\mu\nu}^{(2)}-\frac{1}{2}h_{\mu\nu}R^{(1)}+\frac{1}{2}\eta_{\mu\nu}\eta^{\rho\gamma}R_{\rho\gamma}^{(1)}-\frac{1}{2}\eta_{\mu\nu}\eta^{\rho\gamma}R_{\rho\gamma}^{(2)}}_{G_{\mu\nu}^{(2)}}
\end{split}\]
quindi
\[t_{\mu\nu}=\frac{c^4}{8\pi G_N}\qty(G_{\mu\nu}^{(1)}-G_{\mu\nu})\simeq-\frac{c^2}{8\pi G_N}G_{\mu\nu}^{(2)}\]
abbiamo un espressione di $t^{\mu\nu}$ quadratica in $h$ e anche quadratica nelle derivate. L'equivalente elettromagnetico è pure di questo tipo rispetto ad $A_\mu$.

\subsubsection{Soluzioni nel vuoto}
Nel caso di una soluzione nel vuoto possiamo semplificare usando $R_{\mu\nu}^{(1)}=0$ e quindi
\[t_{\mu\nu}\simeq\frac{c^4}{8\pi G_N}\qty(\frac{1}{2}\eta_{\mu\nu}\eta^{\rho\gamma}R_{\rho\gamma}^{(2)}-R_{\mu\nu}^{(2)})\]
Inoltre se scegliamo la gauge $\p^\mu h_{\mu\nu}=0$ e $h=0$.
\[R_{\mu\nu}^{(2)}=\frac{1}{2}h^{\lambda\rho}\qty\Big(\p_\nu\p_\mu h_{\lambda\rho}+\p_\lambda\p_\rho h_{\mu\nu}-\p_\nu\p_\rho h_{\mu\lambda}-\p_\mu\p_\rho h_{\nu\lambda})\]
Prendiamo un'onda gravitazionale monocromatica piana
\[h_{\mu\nu}=H_{\mu\nu}\cos\underbrace{\qty(k_\mu x^\mu+\vp)}_{\textcircled{\scriptsize H}}\qquad\text{con}\quad k_\mu k^\mu=0\quad k^\mu H_{\mu\nu}=0\]
\[\begin{split}
R_{\mu\nu}^{(2)}&=\frac{1}{2}h^{\lambda\rho}\p_\mu\p_\nu h_{\lambda\rho}+\frac{1}{4}\p_\mu h^{\rho\gamma}\p_\nu h_{\rho\gamma}=\\&
=-\frac{1}{2}H^{\lambda\rho}H_{\lambda\rho}k_\mu k_\nu\cos^2\Hc+\frac{1}{4}k_\mu k_\nu H^{\rho\lambda}H_{\rho\lambda}\sin^2\Hc=\\&
=k_\mu k_\nu H^{\lambda\rho}H_{\lambda\rho}\qty(\frac{3}{4}\sin^2\Hc-\frac{1}{2})
\end{split}\]
$t^{\rho\gamma}R_{\rho\gamma}^{(2)}=0$ quindi otteniamo
\[t_{\mu\nu}\simeq\frac{c^4}{8\pi G_N}k_\mu k_\nu H^{\lambda\rho}H_{\lambda\rho}\qty(\frac{1}{2}-\frac{3}{4}\sin^2\textcircled{\scriptsize H})\]
proprio come per l'onda elettromagnetica $t_{\mu\nu}\propto k_\mu k_\nu$.

Se facciamo una media temporale
\[\ev{t_{\mu\nu}}\simeq\frac{c^4}{64\pi G_N}k_\mu k_\nu H^{\lambda\rho}H_{\lambda\rho}\qquad\ev{t_{\mu\nu}}\simeq\frac{c^4}{32\pi G_N}\ev{\p_\mu h^{\rho\gamma}\p_\nu h_{\rho\gamma}}\]
Quindi anche le onde gravitazionali portano energia e impulso come un flusso di particelle a massa zero.

Un'altra proprietà di $P^\mu_\text{tot}$ è che si può esprimere come un integrale sul bordo spaziale. Abbiamo che
\[T^{\mu\nu}+t_{\mu\nu}=\frac{c^4}{8\pi G_N}G_{\mu\nu}^{(1)}\]
inoltre possiamo scrivere $G_{\mu\nu}^{(1)}$ come una derivata totale
\[\begin{split}
G_{\mu\nu}^{(1)}&=\frac{1}{2}\p^\rho\p_\mu\hb_{\rho\nu}+\frac{1}{2}\p^\rho\p_\nu\hb_{\rho\mu}-\frac{1}{2}\p_\rho\p^\rho\hb_{\mu\nu}-\frac{1}{2}\eta_{\mu\nu}\p^\rho\p^\gamma\hb_{\rho\gamma}=\\&
=\frac{1}{2}\p^\rho\p_\mu\hb_{\rho\nu}-\frac{1}{2}\p_\rho\p^\rho\hb_{\mu\nu}+\frac{1}{2}\p^\rho\eta_{\rho\nu}\p^\gamma\hb_{\mu\gamma}-\frac{1}{2}\eta_{\mu\nu}\p^\rho\p^\gamma\hb_{\rho\gamma}=\\&
=\p^\rho Q_{\rho\mu\nu}\qquad\text{dove}\quad Q_{\rho\mu\nu}=\frac{1}{2}\p_\mu\hb_{\rho\nu}-\frac{1}{2}\p_\rho\hb_{\mu\nu}+\frac{1}{2}\eta_{\rho\nu}\p^\gamma\hb_{\mu\gamma}-\frac{1}{2}\eta_{\mu\nu}\p^\gamma\hb_{\rho\gamma}
\end{split}\]
nota che $Q_{\rho\mu\nu}=-Q_{\mu\rho\nu}$. Con questa proprietà possiamo riverificare $\p^\mu G_{\mu\nu}^{(1)}=\p^\mu\p^\rho Q_{\rho\mu\nu}=0$%\footnote{\[\p^\mu\p^\rho\qty(-Q_{\mu\rho\nu})=-\p^\mu\p^\rho Q_{\mu\rho\nu}=-\p^\rho\p^\mu Q_{\mu\rho\nu}\implies\p^\mu\p^\rho Q_{\rho\mu\nu}=0\]}.

Usando $Q_{\rho\mu\nu}$ possiamo riscrivere $P^\mu_\text{tot}$
\[\begin{split}
P^\mu_\text{tot}&=\int\dd{V}T^{\mu0}_\text{tot}=\frac{c^4}{8\pi G_N}\int\dd{V}\p_\rho Q^{\rho\mu0}=\frac{c^4}{8\pi G_N}\int\dd{V}\p_iQ^{i\mu0}=\\&=\frac{c^4}{8\pi G_N}\int\dd{s^i}Q^{i\mu0}
\end{split}\]
$P^\mu_\text{tot}$ risulta invariante per cambi di coordinate che diventano l'identità all'infinito.
$x'^\mu=x^\mu+\xi^\mu(\xi)$ con $\xi^\mu\ra0$ all'infinito. ($\xi^\mu$ può anche non essere piccolo in generale, basta che lo sia asintoticamente.)
\[\hb'_{\mu\nu}=\hb_{\mu\nu}-\p_\mu\xi_\nu-\p_\nu\xi\mu+\eta_{\mu\nu}\p^\rho\xi_\rho\]
quindi\footnote{\[\begin{split}
Q'_{\rho\mu\nu}&=\resizebox{.9\hsize}{!}{$Q_{\rho\mu\nu}-\frac{1}{2}\p_\mu\p_\nu\xi_\rho+\frac{1}{2}\p_\rho\p_\nu\xi_\mu+\frac{1}{2}\eta_{\rho\nu}\qty(-\p^\lambda\p_\lambda\xi_\mu+\p_\mu\p^\lambda\xi_\lambda)-\frac{1}{2}\eta_{\mu\nu}\qty(-\p^\lambda\p_\lambda\xi_\rho+\p_\rho\p^\lambda\xi_\lambda)=$}\\&
=\resizebox{.9\hsize}{!}{$Q_{\rho\mu\nu}+\p^\lambda\qty(-\frac{1}{2}\eta_{\lambda\nu}\p_\mu\xi_\rho+\frac{1}{2}\eta_{\lambda\nu}\p_\rho\xi_\mu-\frac{1}{2}\eta_{\rho\nu}\p_\lambda\xi_\mu+\frac{1}{2}\eta_{\rho\nu}\p_\mu\xi_\lambda+\frac{1}{2}\eta_{\mu\nu}\p_\lambda\xi_\rho-\frac{1}{2}\eta_{\mu\nu}\p_\rho\xi_\lambda)=$}\end{split}\]}
\[Q'_{\rho\mu\nu}=Q_{\rho\mu\nu}+\p^\lambda D_{\lambda\rho\mu\nu}\qquad\text{dove}\quad D_{\lambda\rho\mu\nu}=-D_{\rho\lambda\mu\nu}\]
%\[\begin{split}
%Q'_{\rho\mu\nu}&=\resizebox{.9\hsize}{!}{$Q_{\rho\mu\nu}-\frac{1}{2}\p_\mu\p_\nu\xi_\rho+\frac{1}{2}\p_\rho\p_\nu\xi_\mu+\frac{1}{2}\eta_{\rho\nu}\qty(-\p^\lambda\p_\lambda\xi_\mu+\p_\mu\p^\lambda\xi_\lambda)-\frac{1}{2}\eta_{\mu\nu}\qty(-\p^\lambda\p_\lambda\xi_\rho+\p_\rho\p^\lambda\xi_\lambda)=$}\\&
%=\resizebox{.9\hsize}{!}{$Q_{\rho\mu\nu}+\p^\lambda\qty(-\frac{1}{2}\eta_{\lambda\nu}\p_\mu\xi_\rho+\frac{1}{2}\eta_{\lambda\nu}\p_\rho\xi_\mu-\frac{1}{2}\eta_{\rho\nu}\p_\lambda\xi_\mu+\frac{1}{2}\eta_{\rho\nu}\p_\mu\xi_\lambda+\frac{1}{2}\eta_{\mu\nu}\p_\lambda\xi_\rho-\frac{1}{2}\eta_{\mu\nu}\p_\rho\xi_\lambda)=$}\\&
%=Q_{\rho\mu\nu}+\p^\lambda D_{\lambda\rho\mu\nu}\qquad\text{dove}\quad D_{\lambda\rho\mu\nu}=-D_{\rho\lambda\mu\nu}
%\end{split}\]
quindi
\[P'^\mu_\text{tot}=P^\mu_\text{tot}+\frac{c^4}{8\pi G_N}\int\dd{V}\p_\rho\p_\lambda D^{\lambda\rho\mu0}=P^\mu_\text{tot}\]
Quindi $P^\mu_\text{tot}$ ha tutte le proprietà per essere interpretato come un quadri-vettore per trasformazioni di Lorentz di uno spazio-tempo asintoticamente Minkowski. Inoltre è conservato, come ogni quadri-impulso in una teoria su Minkowsky invariante per traslazioni.


\subsection{Quadrupolo oscillante}
Discutiamo ora l'energia irraggiata da un quadrupolo oscillante. Nel limite $r\gg\lambda\gg l$ abbiamo visto che
\[h_{ij}\simeq-\frac{2}{3}\frac{G_N}{c^4}\frac{1}{r}\p_t^2Q_{ij}\qquad\text{con}\quad Q_{ij}=\int\dd{V'}\qty(3x'_ix'_j-r'^2\delta_{ij})\frac{T_{00}}{c^2}\]
se prendiamo ad esempio la radiazione lungo la direzione $x^1$ a grande distanza abbiamo un'onda praticamente piana con flusso di energia
\[\begin{split}c\ev{t^{01}}&=\frac{c^5}{16\pi G_N}\ev{\qty\big(\p_0h_{23})^2+\frac{\qty\big(\p_0h_{22}-\p_0h_{33})^2}{4}}=\\&
=\frac{c^5}{16\pi G_N}\qty(\frac{2}{3}\frac{G_N}{c^4}\frac{1}{r})^2\frac{1}{c^2}\ev{\qty\Big(\p_t^3Q_{23})^2+\frac{1}{4}\qty\Big(\p_t^3Q_{22}-\p_t^3Q_{33})^2}=\\&
=\frac{G_N}{36\pi c^5r^2}\ev{\qty\Big(\p_t^3Q_{23})^2+\frac{1}{4}\qty\Big(\p_t^3Q_{22}-\p_t^3Q_{33})^2}\end{split}\]
dove $\ev{\,\cdot\,}$ è la media temporale. Il flusso totale di energia è
\[W=\int\dd{S_i}c\ev{t^{0i}}=4\pi r^2\frac{G_N}{36\pi c^5 r^2}\frac{1}{5}\ev{\p_t^3Q_{ij}\p_t^3Q^{ij}}=\frac{G_N}{45c^5}\ev{\p_t^3Q_{ij}\p_t^3Q^{ij}}\]








