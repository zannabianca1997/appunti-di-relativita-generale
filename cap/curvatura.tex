\chapter[Curvatura]{Curvatura}

Abbiamo visto che dato uno spazio-tempo $M$ con una metrica generica $g_{\mu\nu}$ non è sempre possibile cambiare le coordinate per rendere $g'_{\mu\nu}=\eta_{\mu\nu}$ ovunque. Possiamo sempre rendere $g'_{\mu\nu}$ come $g'_{\mu\nu}(x^\rho+dx^\rho)=\eta_{\mu\nu}+\order{dx^{\rho2}}$, dove $x'^\mu$ è un sistema localmente inerziale (o di caduta libera) in $x^\rho$. Le derivate seconde $\frac{\partial^2g_{\mu\nu}}{\partial x'^\rho \partial x'^\nu}$ non possono sempre essere tutte annullate. Queste contengono informazioni circa la \textit{curvatura} della metrica. Il problema è che le derivate seconde della metrica non sono un tensore. Noi vogliamo invece trovare un tensore che descrive in maniera intrinseca quanto una metrica si discosta da $\eta_{\mu\nu}$. Questo sarà il \textit{tensore di Riemann}.

\section[Tensore di Riemann]{Trasporto parallelo}
Partiamo dalla connessione $\Gamma^\mu_{\nu\rho}$ e dalla derivata covariante $D_\mu A^\nu = \partial_\mu A^\nu + \Gamma^\nu_{\mu\lambda}A^\lambda$. Data una curva $x^\mu(\lambda)$ abbiamo visto che si può definire, partendo da $D_\mu$, una nozione di trasporto parallelo $\frac{dx^\mu}{d\lambda}D_\mu A^\nu=0$. Questo non dipende dalla parametrizzazione della curva. Localmente per uno spostamento $dx^\mu$ possiamo scrivere
\[dx^\mu D_\mu A^\nu=dx^\mu\left(\frac{dA^\nu}{dx^\mu}+\Gamma^\nu_{\mu\lambda}A^\lambda\right)=dA^\nu+\Gamma^\nu_{\mu\lambda}A^\lambda dx^\mu\]
Quindi il vettore è trasportato parallelamente fra $x^\mu$ e $x^\mu+dx^\mu$ se\footnote{il termine sopra è nullo}
\[A^\nu\left(x^\mu+dx^\mu\right)-A^\nu\left(x^\mu\right)=dA^\nu=-\Gamma^\nu_{\mu\lambda}A^\lambda dx^\mu\]
Il trasporto parallelo dipende in genere dal cammino scelto. Prendiamo un punto di partenza $x_A^\mu$ e uno di arrivo $x_B^\mu$. Prendiamo due curve $f_1^\mu(\lambda)$ e $f_2^\mu(\lambda)$ tali che $f_1^\mu(0)=f_2^\mu(0)=x_A^\mu$ e $f_1^\mu(1)=f_2^\mu(1)=x_B^\mu$. Le due curve hanno in genere traiettorie diverse ma stessi punti di partenza e arrivo. Dato un vettore $A^\nu(x_A^\mu)$ possiamo trasportarlo parallelamente lungo le due curve e arrivare a $A_1^\nu(x_B^\mu)$ o $A_2^\nu(x_B^\mu)$. Questi due vettori sono in genere diversi. Se lo spazio-tempo fosse esattamente Minkowski, cioè $g_{\mu\nu}=\eta_{\mu\nu}$ ovunque in un sistema di coordinate, avremmo $A_1^\nu(x_B^\mu)=A_2^\nu(x_B^\mu)$. La differenza fra $A_1^\nu(x_B^\mu)$ e $A_2^\nu(x_B^\mu)$ è quindi un indicatore di quanto lo spazio si discosta da Minkowski.

\subsubsection{Esempio}
\begin{wrapfigure}{r}{0.35\textwidth}
    \vspace{-15pt}
    \centering
    \includegraphics[width=0.35\textwidth]{immagini/Connection-on-sphere.png}
    \vspace{-20pt}
\end{wrapfigure}
% \noindent
L'esempio classico è quello di una sfera e di due cammini fatti da un triangolo di cerchi massimi e fra $A$ e $B$: prendiamo circuito chiuso $x^\mu(\lambda)$, $x^\mu(0)=x_A^\mu$ e $x^\mu(1)=x_A^\mu$. Prendiamo un vettore in $x_A^\mu$, $A^\mu(x_A^\nu)$ e lo trasportiamo parallelamente lungo il circuito. Abbiamo quindi $A^\mu(x^\nu(\lambda))$ che risolve l'equazione:
\[\frac{dx^\mu}{d\lambda}D_\mu A^\nu(\lambda)=\frac{dA^\nu}{d\lambda}+\Gamma^\nu_{\mu\rho}A^\rho\frac{dx^\mu}{d\lambda}=0\]
Al ritorno nel punto $x_A^\mu$ abbiamo un vettore $A^\mu(x^\nu(1))$ che in genere è diverso da quello di partenza:
\[\Delta A^\mu = A^\mu(x^\nu(1))-A^\mu(x^\nu(0))\]
Nota che siccome $x^\mu_A=x^\mu(0)=x^\mu(1)$ i due vettori sono nello stesso punto, quindi appartengono allo stesso spazio vettoriale $T_{x_A^\mu}(M)$. $\Delta A^\mu$ è quindi un vettore.


\subsection{Tensore di Riemann}
\[\begin{split}\Delta A^\mu  &=\int_0^1\frac{dA^\mu}{d\lambda}d\lambda=-\int_0^1\Gamma^\mu_{\nu\rho}A^\nu\frac{dx^\rho}{d\lambda}d\lambda \\&=-\oint\Gamma^\mu_{\nu\rho}A^\nu dx^\rho\end{split}\]
non dipende dalla parametrizzazione ma solo dal circuito chiuso. Per definire $A^\mu(x^\nu)$ abbiamo bisogno di una traiettoria da $x_A^\nu$ a $x_B^\nu$. Traiettorie diverse danno risultati diversi. Comunque per spostamenti infinitesimi si può definire un vettore ovunque nell'intorno di $x_A^\nu$.
\[A^\mu(x_A^\nu+dx^\nu)=A^\mu(x^\nu_A)-\Gamma^\mu_{\rho\nu}A^\rho(x^\nu_A)dx^\nu+\order{dx^2}\]
La differenza dal circuito scelto compare solo all'ordine $\order{dx^2}$. Ora che abbiamo $A^\mu$ in tutto l'intorno di $x_A^\mu$ possiamo usare il teorema di Stokes per un circuito infinitesimo.
\[\begin{split}
        \Delta A^\mu &= -\oint\Gamma^\mu_{\nu\rho}A^\nu\dd{x^\rho}=\\&
        =\frac{1}{2}\qty\Big(-\partial_\tau\left(\Gamma^\mu_{\nu\rho}A^\nu\right)+\partial_\rho\left(\Gamma^\mu_{\nu\tau}A^\nu\right))da^{\tau\rho}=\\&
        =\frac{1}{2}\qty\Big(-\p_\tau\Gamma^\mu_{\nu\rho}A^\nu(x_A)+\Gamma^\mu_{\nu\rho}\Gamma^\nu_{\lambda\tau}A^\lambda(x_A)+\p_\rho\Gamma^\mu_{\nu\tau}A^\nu(x_A)-\underbrace{\Gamma^\mu_{\nu\tau}\Gamma^\nu_{\lambda\rho}A^\lambda(x_A)}_{\lambda\leftrightarrow\nu})\dd{a^{\tau\rho}}=\\&
        =\frac{1}{2}\qty\Big(\p_\rho\Gamma^\mu_{\nu\tau}-\p_\tau\Gamma^\mu_{\nu\rho}+\Gamma^\mu_{\lambda\rho}\Gamma^\lambda_{\nu\tau}-\Gamma^\mu_{\lambda\tau}\Gamma^\lambda_{\nu\rho})A^\nu(x_A)\dd{a}^{\tau\rho}\end{split}\]
abbiamo quindi
\[\Delta A^\mu=-\frac{1}{2}\tensor{R}{^\mu_{\nu\tau\rho}}A^\nu\dd{a^{\tau\rho}}\]
$\tensor{R}{^\mu_{\nu\tau\rho}}$ è il \textit{tensore di Riemann}:
\begin{equation}
    \boxed{\tensor{R}{^\mu_{\nu\tau\rho}}\equiv\partial_\tau\Gamma^\mu_{\nu\rho}-\partial_\rho\Gamma^\mu_{\nu\tau}+\Gamma^\mu_{\lambda\tau}\Gamma^\lambda_{\nu\rho}-\Gamma^\mu_{\lambda\rho}\Gamma^\lambda_{\nu\tau}}
\end{equation}
\[\tensor{R}{^\mu_{\nu\tau\rho}}=-\tensor{R}{^\mu_{\nu\rho\tau}}\qquad\text{per costruzione}\]
Il tensore $(0,4)$ $R_{\mu\nu\rho\gamma}=g_{\mu\delta}\tensor{R}{^\delta_{\nu\rho\gamma}}$ è più utile per vedere tutte le simmetrie.

\subsubsection{Sistema localmente inerziale}
Scriviamo il tensore di un sistema localmente inerziale ($\p g=0\implies\Gamma=0$). Possiamo azzerare le derivate prime $\partial g$, e quindi anche la connessione $\Gamma$, ma non le derivate seconde $\partial^2g$. Quindi se $x^\mu$ è un sistema localmente inerziale
\[\tensor{R}{^\mu_{\nu\gamma\rho}}=\partial_\gamma\Gamma^\mu_{\nu\rho}-\partial_\rho\Gamma^\mu_{\nu\gamma}\]
\[\Gamma^\mu_{\nu\rho}=\frac{1}{2}g^{\mu\lambda}\qty\Big(\partial_\nu g_{\lambda\rho}+\partial_\rho g_{\lambda\nu}-\partial_\lambda g_{\nu\rho})=0\]
\[\partial_\gamma\Gamma^\mu_{\nu\rho}=\frac{1}{2}g^{\mu\lambda}\qty\Big(\p_\gamma\p_\nu g_{\lambda\rho}+\p_\gamma\p_\rho g_{\lambda\nu}-\p_\gamma\p_\lambda g_{\nu\rho})\]
Quindi
\[\tensor{R}{^\mu_{\nu\gamma\rho}}=g^{\mu\lambda} R_{\lambda\nu\gamma\rho}\]
\begin{equation}
    \boxed{R_{\lambda\nu\gamma\rho}=\frac{1}{2}\qty\Big(\p_\gamma\p_\nu g_{\lambda\rho}-\p_\gamma\p_\lambda g_{\nu\rho}-\p_\rho\p_\nu g_{\lambda\gamma}+\p_\rho\p_\lambda g_{\nu\gamma})}
\end{equation}
Tutte le simmetrie sono:
\[\begin{cases}
        R_{\lambda\nu\gamma\rho}=-R_{\lambda\nu\rho\gamma}\\
        R_{\lambda\nu\gamma\rho}=-R_{\nu\lambda\gamma\rho}\\
        R_{\lambda\nu\gamma\rho}=R_{\gamma\rho\lambda\nu}
    \end{cases}\]
Inoltre vale
\[R_{\lambda\nu\gamma\rho}+R_{\lambda\gamma\rho\nu}+R_{\lambda\rho\nu\gamma}=0\]
che è consistente con le altre e sufficiente.

\subsection{Gradi di libertà di \texorpdfstring{$R_{\mu\nu\rho\gamma}$}{}}
Possiamo ora contare i gradi di libertà del tensore di Riemann.
\begin{itemize}
    \item In una dimensione $R_{0000}=0$, infatti ogni spazio unidimensionale può essere "rettificato" con un cambio di coordinate.
    \item In due dimensioni abbiamo un solo elemento linearmente indipendente $R_{0101}$, tutti gli altri sono $0$ $\pm R_{0101}$. Inoltre $R_{0101}+R_{0011}+R_{0110}=0$ automaticamente.
    \item In tre dimensioni $R_{0101}$, $R_{0102}$, $R_{0112}$, $R_{0202}$, $R_{0212}$, $R_{1212}$. Ancora la somma ciclica è automaticamente soddisfatta perché ci sono sempre due uguali\footnote{se due indici sono uguali la somma ciclica è rispettata automaticamente}.
    \item Ora veniamo a $4$ dimensioni. Abbiamo $\frac{4\cdot3}{2}=6$ coppie per i primi due indici e $6$ per i secondi due\footnote{Sappiamo che i primi due indici devono essere diversi, quindi abbiamo 4 scelte per il primo indice e 3 per il secondo. Sappiamo inoltre che scambiando due indici otteniamo meno il tensore, quindi i tensori con due indici scambiati sono tra loro dipendenti. In conclusione l'ordine degli indici non conta e abbiamo $\binom{4}{2}=6$ coppie. La stessa cosa vale per gli ultimi due indici.}. Quindi $6$ con le coppie uguali, tipo $R_{0101}$, e $\frac{6\cdot5}{2}=15$ con le coppie diverse tipo $R_{0102}$ o $R_{0123}$. La somma ciclica impone una sola relazione $R_{0123}+R_{0231}+R_{0312}=0$. Quindi in totale $21-1=20$ erano proprio le derivate seconde della metrica $\p_\mu\p_\nu g_{\rho\gamma}$ ($100$) meno le derivate terze del cambio di coordinate $\p_\mu\p_\nu\p_\rho x'^\gamma$ ($80$). Se portiamo $g_{\mu\nu}=\eta_{\mu\nu}$ abbiamo sempre $6$ trasformazioni di Lorentz, quindi $14$ spazi curvi "intrinsecamente" differenti.
\end{itemize}

Dal tensore di Riemann possiamo costruire il \textit{tensore di Ricci}
\[R_{\mu\nu}=\tensor{R}{^\gamma_{\mu\gamma\nu}}\qquad\text{simmetrico}\quad R_{\mu\nu}=R_{\nu\mu}\]
e la \textit{curvatura scalare}
\[R=g^{\mu\nu}R_{\mu\nu}\]


\section{Spazi massimamente simmetrici}
\subsubsection{2 dimensioni}
Nel caso di $2$ dimensioni tutte le informazioni sono contenute\footnote{si può notare dal fatto che l'unico elemento indipendente è $R_{0101}$, abbiamo quindi il tensore di Ricci che è proporzionale all'identità ($R_{11}=R_{00}$ e $R_{01}=R_{10}=0$), quindi la curvatura è $R=R_{00}\qty(g^{00}+g^{11})$} in $R$. Chiamando $d$ il numero di dimensioni, possiamo infatti scrivere%\footnote{$Rg_{\mu\nu}=\underbrace{g_{\mu\nu}g^{\mu\nu}}_{\Tr\mathbb{I}_{d\times d}=d}R_{\mu\nu}=dR_{\mu\nu}$}
, con $d=2$:
\[R_{\mu\nu}=\frac{1}{d}Rg_{\mu\nu}\]
\[R_{\mu\nu\rho\gamma}=\frac{R}{d(d-1)}\qty\Big(g_{\mu\rho}g_{\nu\gamma}-g_{\mu\gamma}g_{\nu\rho})\]
Spazi di questo tipo, con $R$ costante, sono gli spazi \textit{massimamente simmetrici}.

% \subsection{Esempi}
Studiamo ora due esempi, lo spazio $S^2$ e lo spazio $H^2$.
\subsection{Sfera \texorpdfstring{$S^2$}{}}
Prendiamo la metrica spaziale della sfera $S^2$ in coordinate polari
\[\dd{s}^2=c^2\dd{t}^2-l^2\qty(\dd{\theta}^2+\sin^2\!\theta\dd{\varphi}^2)\]
dove $l$ è il raggio di $S^2$. Quindi limitiamoci alla parte spaziale
\[\qty[g_{\mu\nu}]=-l^2\mqty(\dmat{1,\sin^2\!\theta})\qquad\qty[g^{\mu\nu}]=-\frac{1}{l^2}\mqty(\dmat{1,\frac{1}{\sin^2\!\theta}})\]
la connessione è diversa da zero solo se due indici sono $\varphi$ e uno $\theta$, l'unica derivata non nulla della metrica è $\p_\theta g_{\varphi\varphi}$
\[\begin{split}&\Gamma^\theta_{\varphi\varphi}=\frac{1}{2}g^{\theta\theta}\qty(-\p_\theta g_{\varphi\varphi})=-\sin\theta\cos\theta\\&\Gamma^\varphi_{\theta\varphi}=\frac{1}{2}g^{\varphi\varphi}\qty(\p_\theta g_{\varphi\varphi})=\frac{1}{\tan\theta}\end{split}\]
Per la curvatura basta che calcoliamo
\[\begin{split}\tensor{R}{^\theta_{\varphi\varphi\theta}}&=\underbrace{\p_\varphi\Gamma^\theta_{\varphi\theta}}_{0}-\p_\theta\Gamma^\theta_{\varphi\varphi}+\underbrace{\Gamma^\theta_{\vp\mu}\Gamma^\mu_{\varphi\theta}}_{\mu=\vp}-\underbrace{\Gamma^\theta_{\theta\mu}\Gamma^\mu_{\varphi\varphi}}_{0}=\\&
        =\p_\theta\qty(\sin\theta\cos\theta)-\sin\theta\cos\theta\frac{1}{\tan\theta}=\cos^2\!\theta-\sin^2\!\theta-\cos^2\!\theta=\\&
        =-\sin^2\!\theta\end{split}\]
Quindi
\[R_{\theta\varphi\theta\varphi}=g_{\theta\theta}\tensor{R}{^\theta_{\varphi\theta\varphi}}=-l^2\sin^2\!\theta\]
essendo in due dimensioni, da questo otteniamo tutte le componenti di $R_{\mu\nu\rho\gamma}$ diverse da zero con le necessarie permutazioni!

Il tensore di Ricci è
\[R_{\vp\vp}=\sin^2\!\theta\qquad R_{\ta\ta}=1\qquad R_{\ta\vp}=R_{\vp\ta}=0\]
e la curvatura scalare
\[R=g^{\ta\ta}R_{\ta\ta}+g^{\vp\vp}R_{\vp\vp}=-\frac{2}{l^2}\]
In $2$ dimensioni euclidee $S^2$ è lo spazio massimamente simmetrico con curvatura (riemanniana) positiva\footnote{il valore di $R$ è negativo perchè usiamo la convenzione di prendere la parte spaziale delle metrica con il segno meno}. L'analogo con curvatura negativa è lo spazio iperbolico $H^2$.

\subsection{Spazio iperbolico \texorpdfstring{$H^2$}{}}
Per lo spazio iperbolico
\[\dd{s}^2_\text{spazio}=-l^2\qty\Big(\dd{\ta}^2+\sinh^2\ta\dd{\vp}^2)\qquad\text{ora }0\leq\ta<\infty\]
ripetiamo i conti di prima ma con $\sinh\theta$ al posto di $\sin\theta$
\[\begin{split}&\Gamma^\theta_{\varphi\varphi}=-\sinh\theta\cosh\theta\qquad\Gamma^\varphi_{\theta\varphi}=\frac{1}{\tanh\theta}\\
        &\tensor{R}{^\theta_{\varphi\varphi\theta}}=\p_\theta(\sinh\theta\cosh\theta)-\sinh\theta\cosh\theta\frac{1}{\tanh\theta}=\\&\hspace{27pt}=\cosh^2\theta+\sinh^2\theta-\cosh^2\theta=\sinh^2\theta\\
        &R_{\theta\varphi\theta\varphi}=-l^2\sinh^2\theta\\
        &R_{\vp\vp}=-\sinh^2\theta \qquad R_{\ta\ta}=-1 \qquad R=+\frac{2}{l^2}\end{split}\]
spesso si usa la coordinata $r=\tanh\qty(\theta)$, $0\leq r<1$
\[\dd{s}^2_\text{spazio}=-l^2\qty(\qty(\dv{\theta}{r})^2\dd{r}^2+\sinh^2(\theta(r))\dd{\vp}^2)=\underbrace{-\frac{l^2}{1-r^2}\qty\big(\dd{r}^2+r^2\dd{\vp}^2)}_{\textit{disco di Poincaré}}\]
Per la sfera si può usare $r=\tan(\theta)$
\[\dd{s}^2_\text{spazio}=-l^2\qty(\qty(\dv{\theta}{r})^2\dd{r}^2+\sin^2(\theta(r))\dd{\vp}^2)=-\frac{l^2}{1+r^2}\qty\big(\dd{r}^2+r^2\dd{\vp}^2)\]


\subsection{Tensore di Weyl}
Consideriamo ora il caso in cui tutte le informazioni siano contenute nel tensore di Ricci (questo è sempre vero in $d=3$).

\subsubsection{3 dimensioni}
Vogliamo quindi costruire $R_{\mu\nu\rho\gamma}$ usando solo $R_{\mu\nu}$ e $g_{\mu\nu}$. Prendiamo questa combinazione
\[R_{\mu\rho}g_{\nu\gamma}-R_{\mu\gamma}g_{\nu\rho}+R_{\nu\gamma}g_{\mu\rho}-R_{\nu\rho}g_{\mu\gamma}\]
moltiplicando per $g^{\mu\rho}$ e contraendo gli indici otteniamo\footnote{ricordando che $g^{\mu\rho}g_{\mu\rho}=\Tr\mathbb{I}_{d\times d}=d$}
\[Rg_{\nu\gamma}-R_{\nu\gamma}+dR_{\nu\gamma}-R_{\nu\gamma}=Rg_{\nu\gamma}+(d-2)R_{\nu\gamma}\]
Quindi possiamo costruire il tensore di Riemann
\[\begin{split}R_{\mu\nu\rho\gamma}=&\frac{1}{d-2}\qty\Big(R_{\mu\rho}g_{\nu\gamma}-R_{\mu\gamma}g_{\nu\rho}+R_{\nu\gamma}g_{\mu\rho}-R_{\nu\rho}g_{\mu\gamma})+\\&-\frac{R}{(d-1)(d-2)}\qty\Big(g_{\mu\rho}g_{\nu\gamma}-g_{\mu\gamma}g_{\nu\rho})\end{split}\]
quindi abbiamo $g^{\mu\rho}R_{\mu\nu\rho\gamma}=R_{\nu\gamma}$ per costruzione.

\subsubsection{4 dimensioni}
In generale ($d\geq4$) ci sono informazioni che non possono essere contenute nel tensore di Ricci. Possiamo comunque decomporre sempre il sensore di Riemann nel seguente modo:
\[\begin{split}R_{\mu\nu\rho\gamma}=&C_{\mu\nu\rho\gamma}+\frac{1}{d-2}\qty\Big(R_{\mu\rho}g_{\nu\gamma}-R_{\mu\gamma}g_{\nu\rho}+R_{\nu\gamma}g_{\mu\rho}-R_{\nu\rho}g_{\mu\gamma})+\\&-\frac{R}{(d-1)(d-2)}\qty\Big(g_{\mu\rho}g_{\nu\gamma}-g_{\mu\gamma}g_{\nu\rho})\end{split}\]
dove $C_{\mu\nu\rho\gamma}$ così definito è il \textit{tensore di Weyl}.


\section{Esempi}
\subsection{Tensore di Riemann dal commutatore}
Possiamo ottenere il tensore di Riemann anche facendo il commutatore delle derivate covarianti: $\qty(D_\mu D_\nu-D_\nu D_\mu)A^\rho$ è un tensore $(1,2)$
\[\begin{split}&\qty(D_\mu D_\nu-D_\nu D_\mu)A^\rho=\\&\qquad
        =D_\mu\qty\Big(\p_\nu A^\rho+\Gamma^\rho_{\nu\lambda}A^\lambda)-D_\nu\qty(\p_\mu A^\rho+\Gamma^\rho_{\mu\lambda}A^\lambda)=\\&\qquad
        =\p_\mu\p_\nu A^\rho+\p_\mu\qty\Big(\Gamma^\rho_{\nu\lambda}A^\lambda)+\Gamma^\rho_{\mu\gamma}\qty(\p_\nu A^\gamma+\Gamma^\gamma_{\nu\lambda}A^\lambda)-\Gamma^\gamma_{\mu\nu}\qty\Big(\p_\gamma A^\rho+\Gamma^\rho_{\gamma\lambda}A^\lambda)+\\&\qquad
        -\p_\nu\p_\mu A^\rho+\p_\nu\qty(\Gamma^\rho_{\mu\lambda}A^\lambda)-\Gamma^\rho_{\nu\gamma}\qty(\p_\mu A^\gamma+\Gamma^\gamma_{\mu\lambda}A^\lambda)+\Gamma^\gamma_{\nu\mu}\qty(\p_\gamma A^\rho+\Gamma^\rho_{\gamma\lambda}A^\lambda)=\\&\qquad
        =\qty(\p_\mu\Gamma^\rho_{\nu\lambda}-\p_\nu\Gamma^\rho_{\mu\lambda}+\Gamma^\rho_{\mu\gamma}\Gamma^\gamma_{\nu\lambda}-\Gamma^\rho_{\nu\gamma}\Gamma^\gamma_{\mu\lambda})A^\lambda=\\&\qquad
        =-\tensor{R}{^\rho_{\lambda\nu\mu}}A^\lambda\end{split}\]

\subsection{Esempio di geodetiche}
Studiamo le geodetiche nella metrica di $\mathbb{R}\times S^2$: $\dd{s}^2=c^2\dd{t}^2-l^2\qty(\dd{\theta}^2+\sin^2\!\theta\dd{\vp}^2)$. L'equazione per $x^\mu(s)=\qty(t(s),\theta(s),\vp(s))$ è $\dv[2]{x^\mu}{s}+\Gamma^\mu_{\lambda\nu}\dv{x^\lambda}{s}\dv{x^\nu}{s}=0$:
\[\begin{dcases}
        \dv[2]{t}{s}=0\implies t=\frac{1}{c}\sqrt{1+l^2\qty(\dv{\theta}{s} (0))^2+l^2\sin^2\theta\qty(\dv{\vp}{s} (0))^2}s\\
        \dv[2]{\theta}{s}=-\Gamma^\theta_{\vp\vp}\qty(\dv{\vp}{s})^2=\sin\theta\cos\theta\qty(\dv{\vp}{s})^2\\
        \dv[2]{\vp}{s}=-2\Gamma^\vp_{\theta\vp}\dv{\theta}{s}\dv{\vp}{s}=-\frac{2}{\tan\theta}\dv{\theta}{s}\dv{\vp}{s}
    \end{dcases}\]

Vediamo subito alcune soluzioni semplici. Se $\dv{\varphi}{s} (0)=0$ allora $\theta(s)=\dv{\theta}{s} (0)s$, $\varphi(s)=\varphi(0)$. Quindi i "meridiani" sono traiettorie geodetiche. Se $\theta=\frac{\pi}{2}$ e $\dv{\theta}{s} (0)=0$ allora $\theta(s)=\frac{\pi}{2}$, $\varphi(s)=\dv{\varphi}{s} (0)s$ è soluzione, questo è l'"equatore". Vediamo la soluzione generica. Ci sono due integrali del moto, uno è
\[\qty(\dv{\theta}{s})^2+\sin^2\theta\qty(\dv{\varphi}{s})^2=\text{cost$_1$}\]
Per verificarlo facciamo $\dv{s}$ e usiamo le equazioni geodediche
\[2\dv{\theta}{s}\sin\theta\cos\theta\qty(\dv{\varphi}{s})^2+\frac{\sin^2\theta}{\tan\theta}\qty(\dv{\varphi}{s})^2\dv{\theta}{s}=0\]
L'altro è
\[\sin^2\theta\dv{\varphi}{s}=\text{cost$_2$}\]
\[2\sin\theta\cos\theta\dv{\theta}{s}\dv{\vp}{s}-2\frac{\sin^2\theta}{\tan\theta}\dv{\theta}{s}\dv{\vp}{s}=0\]
\[\begin{dcases}
        \dv{\vp}{s}=\frac{\text{cost$_2$}}{\sin^2\theta}\\
        \qty(\dv{\theta}{s})^2+\frac{\text{cost$_2$}^2}{\sin^2\theta}=\text{cost$_1$}
    \end{dcases}\]
\begin{center}
    \begin{tikzpicture}
        \begin{axis}[axis lines=center, xmin=-0.3, xmax=3.8, ymin=-1, ymax=12, xlabel={$\theta$}, ylabel={$V_\text{eff}(\theta)$}, yticklabels={}]
            \addplot[domain=0.14:3,samples=100,color=red]{1/((sin(deg(x))^2))};
            \addplot +[mark=none, dashed, color=black] coordinates {(3.14, 0) (3.14, 12)};
        \end{axis}
    \end{tikzpicture}
    % \hspace{25pt}
    % \begin{tikzpicture}
    %     \begin{axis}[axis lines=center, xmin=-0.3, xmax=6.5, ymin=-0.2, ymax=1.2, xlabel={$\vp$}, ylabel={$\theta$}, yticklabels={}]
    %         \addplot[domain=0:6.28,samples=100,color=red]{(sin(deg(x))^2};
    %     \end{axis}
    % \end{tikzpicture}
\end{center}
% FINIRE DISEGNO
Verifichiamo che la traiettoria si trova su un cerchio massimo
\[(x_1,x_2,x_3)=l\qty\Big(\sin\!\theta \cos\!\vp,\sin\!\theta \sin\!\vp,\cos\!\theta)\]
\[\dv{\theta}(x_1,x_2,x_3)=l\qty(\sin\!\theta \cos\!\vp\dv{\theta}{s}-\sin\!\theta \sin\!\vp\dv{\vp}{s},\cos\!\theta \sin\!\vp\dv{\theta}{s}+\sin\!\theta \cos\!\vp\dv{\vp}{s},-\sin\!\theta\dv{\theta}{s})\]
\[\begin{split}
        \frac{1}{l^2}\va{x}\cross\dv{\va{x}}{s}&=\vu{x}_1\qty(-\sin^2\!\theta \sin\!\vp\dv{\theta}{s}-\cos^2\!\theta \sin\!\vp\dv{\theta}{s}-\sin\!\theta \cos\!\theta \cos\!\vp\dv{\vp}{s})+\\&
        +\vu{x}_2\qty(\cos^2\!\theta \cos\!\vp\dv{\theta}{s}-\cos\!\theta \sin\!\theta \sin\!\vp\dv{\vp}{s}+\sin^2\!\theta \cos\!\vp\dv{\theta}{s})+\\&
        +\vu{x}_3\qty(\sin\!\theta \cos\!\theta \sin\!\vp \cos\!\vp\dv{\theta}{s}+\sin^2\!\theta \cos^2\!\vp\dv{\vp}{s}-\sin\!\theta \cos\!\theta \sin\!\vp \cos\!\vp\dv{\theta}{s}+\sin^2\!\theta \sin^2\!\vp\dv{\vp}{s})=\\&
        =\vu{x}_1\qty(-\sin\!\vp\,\dot{\theta}-\sin\!\theta \cos\!\theta \cos\!\vp\,\dot{\vp})+\vu{x}_2\qty(\cos\!\vp\,\dot{\theta}-\cos\!\theta \sin\!\theta \sin\!\vp\,\dot{\vp})+\vu{x}_3\qty(\sin^2\!\theta\,\dot{\vp})
    \end{split}\]
Tutte le componenti sono costanti del moto
\[\dv{s}\qty(-\sin\!\vp\,\dot{\theta}-\sin\!\theta \cos\!\theta \cos\!\vp\,\dot{\vp})=0 \qquad \dv{s}\qty(\cos\!\vp\dot{\theta}-\cos\!\theta \sin\!\theta \sin\!\vp\,\dot{\vp})=0 \qquad \dv{s}\qty(\sin^2\!\theta\,\dot{\vp})=0\]
corrispondono ai tre momenti angolari conservati.


\section{Effetto di marea}
Un'altra manifestazione della curvatura è nella \textit{deviazione geodetica}. Due geodetiche vicine misurano un'accelerazione nella loro distanza che è proporzionale alla curvatura. Fisicamente questo si chiama anche \textit{effetto di marea}. Ad esempio, andando in un sistema localmente inerziale possiamo cancellare $\Gamma$ e quindi la forza gravitazionale in un punto, ma l'accelerazione relativa di una particella vicina non è sempre cancellabile ed è legata alla curvatura. Prendiamo due due geodetiche $x_1^\mu(s)$ e $x_2^\mu(s)$ infinitamente vicine. $\var x^\mu(s)=x_2^\mu(s)-x_1^\mu(s)$ è il vettore che congiunge le due geodetiche ad ogni $s$ fissato. Entrambe soddisfano $u^\nu D_\nu u^\mu=0$:
\[\dv[2]{x_1^\mu}{s}+\Gamma^\mu_{\nu\rho}(x_1^\lambda)\dv{x_1^\nu}{s}\dv{x_1^\rho}{s}=0\]
\[\dv[2]{\qty(x_1^\mu+\var x^\mu)}{s}+\Gamma^\mu_{\nu\rho}\qty(x_1^\lambda+\var x^\lambda)\dv{\qty(x_1^\nu+\var x^\nu)}{s}\dv{\qty(x_1^\rho+\var x^\rho)}{s}=0\]
facendo la differenza otteniamo
\[\dv[2]{\var{x^\mu}}{s}+\Gamma^\mu_{\nu\rho}\qty(x_1^\lambda)\dv{x_1^\nu}{s}\dv{\var{x_1^\rho}}{s}+\Gamma^\mu_{\nu\rho}(x_1^\lambda)\dv{\var{x^\nu}}{s}\dv{x_1^\rho}{s}+\p_\tau\Gamma^\mu_{\nu\rho}(x_1^\lambda)\var{x^\tau}\dv{x_1^\nu}{s}\dv{x_1^\rho}{s}+\order{\var{x^2}}\]
la derivata seconda covariante è (con $\frac{D}{\dd{s}}=u^\lambda D_\lambda$)
\[\begin{split}
        \frac{D^2\var{x^\mu}}{\dd{s}^2}&=u^\lambda D_\lambda\qty(u^\nu D_\nu\var{x^\mu})=u^\lambda D_\lambda\qty(\dv{\var{x^\mu}}{s}+\Gamma^\mu_{\gamma\rho}\var{x^\gamma}\dv{x^\rho}{s})=\\
        &=\dv[2]{\var{x^\mu}}{s}+\dv{s}\qty(\Gamma^\mu_{\gamma\rho}\var{x^\gamma}\dv{x^\rho}{s})+\Gamma^\mu_{\lambda\tau}\qty(\dv{\var{x^\lambda}}{s}+\Gamma^\lambda_{\gamma\rho}\var{x^\gamma}\dv{x^\rho}{s})\dv{x^\tau}{s}=\\
        &=\dv[2]{\var{x^\mu}}{s}+\Gamma^\mu_{\gamma\rho}\dv{\var{x^\gamma}}{s}\dv{x^\rho}{s}+\Gamma^\mu_{\gamma\rho}\var{x^\gamma}\dv[2]{x^\rho}{s}+\p_\lambda\Gamma^\mu_{\gamma\rho}\dv{x^\lambda}{s}\var{x^\gamma}\dv{x^\rho}{s}+\\
        &\quad+\Gamma^\mu_{\lambda\tau}\dv{\var{x^\lambda}}{s}\dv{x^\tau}{s}+\Gamma^\mu_{\lambda\tau}\Gamma^\mu_{\gamma\rho}\var{x^\gamma}\dv{x^\rho}{s}\dv{x^\tau}{s}\end{split}\]
ora possiamo riscrivere l'equazione sopra
\[\begin{split}\frac{D^2\var{x^\mu}}{\dd{s}^2}-\Gamma^\mu_{\gamma\rho}\var{x^\gamma}\qty(-\Gamma^\rho_{\lambda\tau}\dv{x^\lambda}{s}\dv{x^\tau}{s})-\p_\lambda\Gamma^\rho_{\lambda\tau}\var{x^\gamma}\dv{x^\lambda}{s}\dv{x^\rho}{s}+\\-\Gamma^\mu_{\lambda\tau}\Gamma^\lambda_{\gamma\rho}\var{x^\gamma}\dv{x^\rho}{s}\dv{x^\tau}{s}+\p_\tau\Gamma^\mu_{\nu\rho}\var{x^\tau}\dv{x^\nu}{s}\dv{x^\rho}{s}=0\end{split}\]
ora riarrangiando gli indici
\[\frac{D^2\var{x^\mu}}{\dd{s}^2}+\underbrace{\qty(\p_\tau\Gamma^\mu_{\nu\rho}-\p_\rho\Gamma^\mu_{\tau\nu}+\Gamma^\mu_{\tau\lambda}\Gamma^\lambda_{\nu\rho}-\Gamma^\mu_{\nu\lambda}\Gamma^\lambda_{\tau\rho})}_{-\tensor{R}{^\mu_{\nu\rho\tau}}}\var{x^\tau}\dv{x^\nu}{s}\dv{x^\rho}{s}\]
quindi
\[\frac{D^2\var{x^\mu}}{\dd{s}^2}=\tensor{R}{^\mu_{\nu\rho\tau}}\var{x^\tau}u^\nu u^\rho\]
\textit{effetto di marea} o deviazione geodetica. La curvatura si manifesta quindi in un'accelerazione relativa delle geodetiche.


\section{Identità di Bianchi}
Vediamo ora una relazione fra le derivate covarianti del tensore di Riemann chiamata \textit{identità di Bianchi}\footnote{È la somma delle permutazioni cicliche di $\tau\rho\gamma$}:
\begin{equation}
    D_\tau\tensor{R}{^\mu_{\nu\rho\gamma}}+D_\gamma\tensor{R}{^\mu_{\nu\tau\rho}}+D_\rho\tensor{R}{^\mu_{\nu\gamma\tau}}=0
\end{equation}
Per vedere che è un'identità tensoriale possiamo verificarla nel sistema localmente inerziale, dove
\[D_\tau\tensor{R}{^\mu_{\nu\rho\gamma}}=\p_\tau\qty(\p_\rho\Gamma^\mu_{\nu\gamma}-\p_\gamma\Gamma^\mu_{\nu\rho})\]
Quindi abbiamo
\[\underbrace{\p_\tau\p_\rho\Gamma^\mu_{\nu\gamma}-\p_\tau\p_\gamma\Gamma^\mu_{\nu\rho}}_{D_\tau\tensor{R}{^\mu_{\nu\rho\gamma}}}+\underbrace{\p_\gamma\p_\tau\Gamma^\mu_{\nu\rho}-\p_\gamma\p_\rho\Gamma^\mu_{\nu\tau}}_{D_\gamma\tensor{R}{^\mu_{\nu\tau\rho}}}+\underbrace{\p_\rho\p_\gamma\Gamma^\mu_{\nu\gamma}-\p_\rho\p_\tau\Gamma^\mu_{\nu\gamma}}_{D_\rho\tensor{R}{^\mu_{\nu\gamma\tau}}}=0\]
tutti i termini si cancellano.

Vediamo una contrazione dell'identità di Bianchi ($\tensor{R}{^\mu_{\nu\mu\gamma}}=R_{\nu\gamma}$):
\begin{align*}
  &g^{\nu\rho}\tensor{\delta}{^\tau_\mu}\qty(D_\tau\tensor{R}{^\mu_{\nu\rho\gamma}}+D_\gamma\tensor{R}{^\mu_{\nu\tau\rho}}+D_\rho\tensor{R}{^\mu_{\nu\gamma\tau}})=0\\&g^{\nu\rho}D_\mu\tensor{R}{^\mu_{\nu\rho\gamma}}+D_\gamma\qty(g^{\nu\rho}R_{\nu\rho})-g^{\nu\rho}D_\rho R_{\nu\gamma}=0\\
  &\p_\gamma R-2g^{\rho\nu}D_\rho R_{\nu\gamma}=0
\end{align*}
Questa sarà importante per le equazioni di Einstein. Se definiamo il \textit{tensore di Einstein}
\[G_{\mu\nu}\equiv R_{\mu\nu}-\frac{1}{2}Rg_{\mu\nu}\]
abbiamo
\[g^{\rho\mu}D_\rho G_{\mu\nu}=g^{\rho\mu}D_\rho R_{\mu\nu}-\frac{1}{2}\p_\nu R=0\]
quindi vale
\begin{equation}
    D_\rho G_{\mu\nu}=0
\end{equation}

\subsection{Gradi di libertà}
Calcoliamo i gradi di libertà del tensore $R_{\mu\nu\rho\gamma}$ in dimensione generica $N$. Abbiamo $\binom{N}{2}$ coppie per $\mu\nu$ e $\rho\gamma$. Quindi $\frac{N(N-1)}{2}$ se le due coppie sono uguali. Se sono diverse abbiamo $\frac{N(N-1)}{2}\frac{\qty(\frac{N(N-1)}{2}-1)}{2}=\frac{N(N-1)}{2}\qty(\frac{N^2}{4}-\frac{N}{4}-\frac{1}{2})$ quindi in totale $\frac{N(N-1)}{2}\qty(\frac{N^2}{4}-\frac{N}{4}-\frac{1}{2})$. La somma ciclica ne toglie $\binom{N}{4}=\frac{N(N-1)(N-2)(N-3)}{24}$. Quindi
\[
    \resizebox{1\hsize}{!}{$\frac{N(N-1)}{4}\qty(\frac{N^2}{2}-\frac{N}{2}+1-\frac{(N-1)(N-3)}{6})=
        \frac{N(N-1)}{4}\qty(\frac{N^2}{3}+\frac{N^3}{3})=\frac{N^2(N+1)(N-1)}{12}$}
\]
Confrontiamolo con
\[
    \resizebox{1\hsize}{!}{$\#[\p\p g]-\#[\p\p\p x']=\frac{N^2(N+1)^2}{4}-\frac{N^2(N+1)(N+2)}{6}=\frac{N^2(N+1)}{2}\qty(\frac{N*1}{2}-\frac{N+2}{3})=\frac{N^2(N+1)(N-1)}{12}$}
\]
quindi è sempre lo stesso numero di gradi di libertà.


\section{Esercizi}
Prendiamo una suerficie immersa in tre dimensioni euclidee $\dd{x}^2=\dd{s}^2+\dd{y}^2+\dd{z}^2$. La superficie, con opportune rotazioni e traslazioni, può essere sempre portata localmente nella forma
\[z(x,y)=\frac{x^2}{2\rho_x}\pm\frac{y^2}{2\rho_y}+\ldots\qquad\rho_1,\rho_2>0\]
\begin{center}
    \begin{tikzpicture}[scale=1]
        \begin{axis}[
            axis lines=center,
            xlabel={$x$},ylabel={$y$},zlabel={$z$},
            xtick=\empty,ytick=\empty,ztick=\empty,
            xticklabels={},yticklabels={},zticklabels={},
            colormap/blackwhite
            ]
            % Superficie
            \addplot3 [surf,shader=faceted,draw=black, fill opacity=0.75] {x^2+y^2};
            % Curve blu
            \addplot3[variable=t,mesh,domain=0:5, color=blue] (t,0,t^2);
            \addplot3[variable=t,mesh,domain=0:5, color=blue] (-t,0,t^2) node[above] {$+$};
            \addplot3[variable=t,mesh,domain=0:5, color=blue] (0,t,t^2) node[above] {$+$};
            \addplot3[variable=t,mesh,domain=0:5, color=blue] (0,-t,t^2);
        \end{axis}
    \end{tikzpicture}
    \hspace{25pt}
    \begin{tikzpicture}[scale=1]
        \begin{axis}[
            axis lines=center,
            xlabel={$x$},ylabel={$y$},zlabel={$z$},
            xtick=\empty,ytick=\empty,ztick=\empty,
            xticklabels={},yticklabels={},zticklabels={},
            colormap/blackwhite
            ]
            % Superficie
            \addplot3 [surf,shader=faceted,draw=black,fill opacity=0.75] {x^2-y^2};
            % Curve blu e rossa
            \addplot3[variable=t,mesh,domain=0:5, color=blue] (t,0,t^2) node[above] {$+$};
            \addplot3[variable=t,mesh,domain=0:5, color=blue] (-t,0,t^2);
            \addplot3[variable=t,mesh,domain=0:5, color=red] (0,t,-t^2);
            \addplot3[variable=t,mesh,domain=0:5, color=red] (0,-t,-t^2) node[above] {$-$};
        \end{axis}
    \end{tikzpicture}
    % Stesse immagini ma in bianco e nero
    % \begin{comment}
    %     \begin{tikzpicture}[scale=1]
    %         \begin{axis}[axis lines=center, xlabel={$x$}, ylabel={$y$}, zlabel={$z$}, colormap/blackwhite, xticklabels={},yticklabels={},zticklabels={}]
    %             \addplot3 [surf,shader=flat,draw=black] {x^2+y^2};
    %         \end{axis}
    %     \end{tikzpicture}
    %     \hspace{25pt}
    %     \begin{tikzpicture}[scale=1]
    %         \begin{axis}[axis lines=center, xlabel={$x$}, ylabel={$y$}, zlabel={$z$}, colormap/blackwhite, xticklabels={},yticklabels={},zticklabels={}]
    %             \addplot3 [surf,shader=flat,draw=black] {x^2-y^2};
    %         \end{axis}
    %     \end{tikzpicture}
    % \end{comment}
\end{center}
I due "raggi di curvatura" sono proprio $\rho_x$ e $\rho_y$. La geometria "estrinseca" locale è proprio definita dai due raggi di curvatura e dal segno relativo. La geometria "intrinseca" invece porta meno informazioni, ma è derivata dalla geometria estrinseca. Calcoliamo la metrica indotta sulla superficie
\[\dd{s}^2=\dd{x}^2+\dd{y}^2+\qty(\frac{x}{\rho_x}\dd{x}\pm\frac{y}{\rho_y}\dd{y})^2=\qty(1\pm\frac{x^2}{\rho_x^2}\dd{x}^2)\pm2\frac{xy}{\rho_x\rho_y}\dd{x}\dd{y}+\qty(1+\frac{y^2}{\rho_y^2})\dd{y}^2\]
\[\qty[g_{\mu\nu}]=\mqty(1+\frac{x^2}{\rho_x^2}&\pm2\frac{xy}{\rho_x\rho_y}\\\pm2\frac{xy}{\rho_x\rho_y}&1+\frac{y^2}{\rho_y^2})\qquad\text{con}\quad x^\mu=(x,y)\]
calcoliamo la curvatura nel punto $(x,y)=(0,0)$ vale $\Gamma^\mu_{\nu\rho}=0$ perché le derivate della metrica sono nulle. Quindi
\[R_{\mu\nu\rho\gamma}=\frac{1}{2}\qty\Big(\p_\mu\p_\gamma g_{\nu\rho}-\p_\mu\p_\rho g_{\nu\gamma}+\p_\nu\p_\rho g_{\mu\gamma}-\p_\nu\p_\gamma g_{\mu\rho})\]
L'unico non nullo è $R_{xyxy}$ e i suoi scambi
\[R_{xyxy}=\frac{1}{2}\qty\Big(\p_x\p_y g_{yx}-\p_x\p_x g_{yy}+\p_y\p_x g_{xy}-\p_y\p_y g_{xx})=\pm\frac{1}{\rho_x\rho_y}\]
La curvatura scalare è $R=\frac{\pm2}{\rho_x\rho_y}$. La curvatura "intrinseca" è quindi sensibile solo al prodotto dei due raggi di curvatura $\rho_x$ e $\rho_y$.


\begin{comment}
    \begin{tikzpicture}[scale=1.5]
        \begin{axis}[axis lines=center,axis on top,xlabel={$x$}, ylabel={$y$}, zlabel={$ct$},domain=0:1,y domain=0:2*pi,xmin=-1.5, xmax=1.5,ymin=-1.5, ymax=1.5,colormap/blackwhite, samples=10,samples y=40,z buffer=sort]
            % [axis lines=center,axis on top,xlabel={$x$}, ylabel={$y$}, zlabel={$z$},domain=0:1,y domain=0:2*pi,xmin=-1.5, xmax=1.5,ymin=-1.5, ymax=1.5, zmin=0.0,mesh/interior colormap={blueblack}{color=(black) color=(blue)},colormap/blackwhite, samples=10,samples y=40,z buffer=sort]
            \addplot3[surf] ({x*cos(deg(y))},{x*sin(deg(y))},{x});
            \addplot3[surf] ({x*cos(deg(y))},{x*sin(deg(y))},{-x});
        \end{axis}
    \end{tikzpicture}

    \begin{tikzpicture}[scale=1.5]
        \begin{axis}[axis lines=center,
            axis on top,
            xtick=\empty,
            ytick=\empty,
            ztick=\empty,
            xrange=-2:2,
            yrange=-2:2
            ]
            % plot
            \addplot3[domain=-1:1,y domain=-pi:pi,colormap/viridis,surf]
            ({x*cos(deg(y))},{x*sin(deg(y))},{x});
            \addplot3[domain=-1:1,y domain=-pi:pi,colormap/viridis,mesh]
            ({x*cos(deg(y))},{x*sin(deg(y))},{-x});
        \end{axis}
    \end{tikzpicture}
\end{comment}

\begin{comment}
    \begin{tikzpicture}[scale=0.6]
        \begin{axis}[axis lines=center,
            axis on top,
            set layers=default,
            xtick=\empty,
            ytick=\empty,
            ztick=\empty,
            xrange=-2:2,
            yrange=-2:2,
            unit vector ratio=1 1 1,% <- HERE (taken from Phelype Oleinik's deleted answer)
            scale=3 %<- added to compensate for the downscaling
            % resulting from unit vector ratio=1 1 1
            ]
            % plot
            \addplot3[domain=0:1,y domain=0:2*pi,colormap/viridis,surf]
            ({x*cos(deg(y))},{x*sin(deg(y))},{x});
            % \addplot3[domain=-1:1,y domain=-pi:0,colormap/viridis,surf,on layer=axis foreground] ({x*cos(deg(y))},{x*sin(deg(y))},{-x});
            % \addplot3[domain=0:1,y domain=-pi:pi,colormap/viridis,surf,on layer=axis foreground] ({x*cos(deg(y))},{x*sin(deg(y))},{-x});
            \addplot3[domain=0:1,y domain=0:2*pi,colormap/viridis,mesh]
            ({x*cos(deg(y))},{x*sin(deg(y))},{-x});
        \end{axis}
    \end{tikzpicture}
\end{comment}