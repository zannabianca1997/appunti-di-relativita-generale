\chapter[Equazioni di Einstein]{Equazioni di Einstein}

Nel limite non relativistico chiamiamo $\order{\frac{v}{c}}=\varepsilon$. La metrica può essere portata nella forma $g_{\mu\nu}=\eta_{\mu\nu}+h_{\mu\nu}$ con $\order{h_{\mu\nu}}=\varepsilon^2$ o più piccolo. Inoltre tutte le derivate sono di ordine\footnote{$\frac{\p_0}{\p_i}=\pdv{ct}\qty(\pdv{x})^{-1}=\frac{\p x}{c\p t}=\frac{v}{c}$} $\order{\frac{\p_0}{\p_i}}=\varepsilon$. Nel limite newtoniano dobbiamo ottenere $\va{F}=-m\grad\phi$ e $\Delta\phi=4\pi G_N\rho$. L'equazione delle particelle abbiamo visto che è consistente con il limite non relativistico se $h_{00}\simeq\frac{2\phi}{c^2}$. Questo è, a posteriori, consistente con $\order{h}=\varepsilon^2$ visto che $\order{\phi}=\order{v^2}$ in teoria newtoniana. 

\section{Equazioni di Einstein}
L'equazioni di Einstein sono la versione relativistica di $\Delta\phi=4\pi G_N\rho_m$. $c^2\rho_m$ ora è il termine $T^{00}$ di un tensore energia-impulso $T^{\mu\nu}$ con $\order{T^{ij}}=\varepsilon \order{T^{0i}}=\varepsilon^2\order{T^{00}}$. Quindi dobbiamo trovare una equazione tensoriale $F_{\mu\nu}=T_{\mu\nu}$ dove $F_{\mu\nu}$ è un qualunque tensore $(0,2)$ simmetrico costruito con la metrica e le sue derivate.

\subsection{Derivazione}
Nel limite non relativistico dobbiamo avere $F_{00}=\frac{c^2}{4\pi G_N}\Delta\phi=\frac{c^4}{8\pi G_N}\partial_i\partial_ih_{00}$. Possiamo decomporre 
\[F_{\mu\nu}=F_{\mu\nu}^{(0)}+F_{\mu\nu}^{(2)}+\ldots+F_{\mu\nu}^{(n)}\]
dove $n$ è il numero di derivate della metrica. Ad esempio
\[F^{(0)}_{\mu\nu}=\alpha g_{\mu\nu} \qquad F_{\mu\nu}^{(2)}=\beta R_{\mu\nu}+\gamma g_{\mu\nu}R \qquad \text{etc.}\]
Per $F_{\mu\nu}^{(0)}$ e $F_{\mu\nu}^{(2)}$ quelli appena scritti sono i termini più generali.

\subsection{Caso $F_{\mu\nu}^{(0)}=0$}
Assumiamo per ora $F_{\mu\nu}=F_{\mu\nu}^{(2)}$, quindi abbiamo termini omogenei di grado $2$ nel numero di derivate: una derivata seconda $\partial\partial gg\ldots g$ oppure due derivate prime $\partial g\partial gg\ldots g$. L'equazione $\beta R^{\mu\nu}+\gamma g^{\mu\nu}R=T^{\mu\nu}$, applicando $D_\mu$ dà
\[\beta D_\mu R^{\mu\nu} + \gamma g^{\mu\nu}D_{\mu}R = D_{\mu}T^{\mu\nu} = 0\]
usando la forma contratta delle identità di Bianchi $D_\mu R^{\mu\nu}=\frac{1}{2}g^{\mu\nu}\partial_\mu R$ abbiamo
\[\left(\frac{\beta}{2}+\gamma\right)g^{\mu\nu}\partial_\mu R = 0\]
Se $\gamma\neq-\frac{1}{2}\beta$ dobbiamo avere $\partial_\mu R = 0$ ma siccome\footnote{$\tensor{T}{^\mu_\mu}=g^{\mu\nu}T_{\mu\nu}=g^{\mu\nu}F_{\mu\nu}^{(2)}=g^{\mu\nu}\qty(\beta R_{\mu\nu}+\gamma g_{\mu\nu}R)=\beta\underbrace{g^{\mu\nu}R_{\mu\nu}}_{R}+\gamma\underbrace{g^{\mu\nu}g_{\mu\nu}}_{4}R=\qty(\beta+4\gamma)R$} $R(\beta+4\gamma)=\tensor{T}{^\mu_\mu}$ questo implicherebbe $\p_\mu \tensor{T}{^\nu_\nu}=0$. Avere $\tensor{T}{^\mu_\mu}=\text{cost}$ in tutto lo spazio non è sicuramente una condizione accettabile per tutte le possibili forme di materia. Quindi dobbiamo avere $\gamma=-\frac{1}{2}\beta$ e quindi
\[\beta\left(R_{\mu\nu}-\frac{1}{2}g_{\mu\nu}R\right)=T_{\mu\nu}\]
cioè
\[G_{\mu\nu}=\frac{1}{\beta}T_{\mu\nu}\]
dove $G_{\mu\nu}\equiv R_{\mu\nu}-\frac{1}{2}g_{\mu\nu}R$ è il \textit{tensore di Einstein}.

\subsubsection{Limite newtoniano per trovare $\beta$}
Ora facciamo il limite newtoniano ($g^{\mu\nu}=\eta^{\mu\nu}+h^{\mu\nu}$, $\abs{h^{\mu\nu}}\ll1$) per fissare la costante $\beta$. Calcoliamo la curvatura al prim'ordine in $h_{\mu\nu}$
\[\tensor{R}{^\mu_{\nu\rho\gamma}}\simeq\partial_\rho\Gamma^\mu_{\nu\gamma}-\partial_\gamma\Gamma^\mu_{\nu\rho}\]
($\Gamma\Gamma$ sono $\order{h^2_{\mu\nu}}$). Inoltre
\[\Gamma^\mu_{\nu\gamma}\simeq\frac{1}{2}\eta^{\mu\lambda}\qty\Big(\partial_\nu h_{\gamma\lambda}+\partial_\gamma h_{\nu\lambda}-\partial_\lambda h_{\nu\gamma})\]
quindi
\[\tensor{R}{^\mu_{\nu\rho\gamma}}\simeq\frac{1}{2}\eta^{\mu\lambda}\qty\Big(\partial_\rho\partial_\nu h_{\gamma\lambda} - \partial_\rho\partial_\lambda h_{\nu\gamma} - \partial_\gamma\partial_\nu h_{\rho\lambda} + \partial_\gamma\partial_\lambda h_{\nu\rho})\]
\begin{equation}
	R_{\nu\gamma}\simeq\frac{1}{2}\qty\Big(\p_\mu\p_\nu\tensor{h}{_\gamma^\mu}-\p_\mu\p^\mu h_{\nu\gamma}-\p_\gamma\p_\nu h+\p_\gamma\p_\mu\tensor{h}{_\nu^\mu})
\end{equation}
dove $h=\tensor{h}{^\mu_\mu}$ e muoviamo gli indici con $\eta^{\mu\nu}$. Ora usiamo il fatto che $\partial_i$ sono dominanti rispetto a $\partial_0$ e abbiamo
\[R_{00}\simeq\frac{1}{2}\partial_i\partial_ih_{00}\]
Da $T_{ij}\ll T_{00}$ e $G_{ij}=\frac{1}{\beta}T_{ij}$ abbiamo $R_{ij}\simeq\frac{1}{2}g_{ij}R$
\[R\simeq \eta^{00}R_{00}+\eta^{ij}R_{ij}=R_{00}+\frac{3}{2}R\Longrightarrow R\simeq-2R_{00}\]
quindi
\[G_{00}=R_{00}-\frac{1}{2}g_{00}R\simeq 2R_{00}\simeq\p_i\p_ih_{00}\]
\[\p_i\p_i\qty(\frac{2\phi}{c^2})=\frac{1}{\beta}c^2\rho\implies\frac{8\pi G_N}{c^4}=\frac{1}{\beta}\]
quindi l'\textit{equazioni di Einstein} sono
\begin{equation}\label{9.1}
	\boxed{R_{\mu\nu}-\frac{1}{2}g_{\mu\nu}R=\frac{8\pi G_N}{c^4}T_{\mu\nu}}
\end{equation}

Facendo la traccia con $g^{\mu\nu}$ otteniamo $-R=\frac{8\pi G_N}{c^2}\tensor{T}{^\mu_\mu}$ quindi possiamo riscrivere l'equazione come
\[R_{\mu\nu}=\frac{8\pi G_N}{c^4}\qty(T_{\mu\nu}-\frac{1}{2}g_{\mu\nu}\tensor{T}{^\rho_\rho})\]
questa forma è equivalente a quella sopra. Inoltre notiamo che
\[T_{\mu\nu}=0\implies R_{\mu\nu}=0\]
quindi nel vuoto il tensore di Ricci si annulla.

\subsubsection{Risoluzione}
L'equazioni di Einstein sono equazioni alle derivate seconde non lineari. La non linearità è dovuta al fatto che la gravità "interagisce con se stessa". Abbiamo visto che l'energia e l'impulso delle particelle non si conservano in un campo gravitazionale. Quindi a sua volta il campo gravitazionale deve contenere energia ed impulso e quindi essere una sorgente di se stesso. Questo si manifesta nella non linearità delle equazioni. Le equazioni sono $10$ e con queste dobbiamo determinare le $10$ componenti della metrica $g_{\mu\nu}$.

Prendiamo una equazione simile ma molto più semplice: l'equazione delle onde $\p_\mu\p^\mu\phi=0$ $\p_0^2\phi=\p_i^2\phi$. Per risolverla dobbiamo decidere il campo $\phi$ e la derivata $\p_0\phi$ su tutta una sezione spaziale $x^0=x^{0*}$. Dati $\phi\qty(x^{0*},x^i)$ e $\p_0\phi\qty(x^{0*},x^i)$, usando $\p_\mu\p^\mu\phi=0$ possiamo ottenere l'unica soluzione $\phi(x^\mu)$ in tutto lo spazio-tempo. Questo tipo di PDE's si chiamano \textit{iperboliche}. Le equazioni di Einstein sono $10$ equazioni alle derivate seconde per ottenere le $10$ componenti della metrica. Specificare $g_{\mu\nu}$ e $\p_0g_{\mu\nu}$ in una sezione $x^0=\text{cost}$ però non è il modo giusto di formulare il problema di Cauchy. $G^{\mu\nu}=\frac{8\pi G_n}{c^2}T^{\mu\nu}$ non sono tutte equazioni contenenti $\p_0\p_0g$. Prendiamo l'identità di Bianchi $D_\mu G^{\mu\nu}=0$
\[\p_0G^{0\mu}=-\p_iG^{i\mu}-\Gamma^\mu_{\mu\lambda}G^{\lambda\nu}-\Gamma^\nu_{\mu\lambda}G^{\mu\lambda}\]
siccome il pezzo a destra contiene al massimo $\p_0^2g$ termini, $G^{0\mu}$ non può contenere $\p_0^2g$ ma al massimo termini $\p_0g$. Quindi le $4$ equazioni $G^{0\mu}=\frac{8\pi G_N}{c^2}T^{0\mu}$ non sono equazioni di tipo iperbolico, ma $4$ condizioni che $g_{\mu\nu}$ e $\p_0g_{\mu\nu}$ devono soddisfare. Non possiamo quindi specificare tutte le $g_{\mu\nu}$ e $\p_0g_{\mu\nu}$ liberamente. Le equazioni "dinamiche" sono quindi solo $G^{ij}=\frac{8\pi G_N}{c^2}T^{ij}$, cioè $6$ e non $10$. Sappiamo infatti che specificare $g_{\mu\nu}$ e $\p_0g_{\mu\nu}$ ad un $x^0=\text{cost}$ non potrà mai dare un'unica soluzione delle equazioni. $4$ funzioni date dai possibili cambi di coordinate $x'^\mu(x^\rho)$, che lasciano invariata la sezione di partenza, possono dare una famiglia di soluzioni che soddisfano le equazioni e hanno le stesse condizioni iniziali. Per avere una soluzione univoca dobbiamo in qualche modo "fissare" il sistema di riferimento, il che è analogo a scegliere una "gauge" in elettromagnetismo.


\subsection{Caso $F_{\mu\nu}^{(0)}\neq0$}
Torniamo ora alla scelta $F_{\mu\nu}=F_{\mu\nu}^{(2)}$. Possiamo rilassare questa condizione, prendiamo $F_{\mu\nu}=F_{\mu\nu}^{(0)}+F_{\mu\nu}^{(2)}$. Ora abbiamo $3$ termini
\[F_{\mu\nu}=\alpha g_{\mu\nu}+\beta R_{\mu\nu}+\gamma g_{\mu\nu}R\]
La condizione $D_\mu F^{\mu\nu}=0$ fissa $\gamma=-\frac{1}{2}\beta$ e lascia $\alpha$ arbitrario. Abbiamo quindi
\[\alpha g_{\mu\nu}+\beta\qty(R_{\mu\nu}-\frac{1}{2}g_{\mu\nu}R)=T_{\mu\nu}\]
Nel limite non relativistico
\[\alpha\qty(1+h_{00})+\beta\p_i\p_ih_{00}=\rho c^2\qquad\text{con}\quad h_{00}=\frac{2\phi}{c^2}\]
nota che $\alpha/\beta=\mp1/l^2$ dove $l$ è una lunghezza.

Possiamo quindi ottenere il limite newtoniano solo se $\frac{1}{\beta}=\frac{8\pi G_N}{c^4}$ e se $l=\sqrt{\mp\beta/\alpha}$ è sufficiente grande. L'\textit{equazioni di Einstein} diventano
\begin{equation}
	\boxed{R_{\mu\nu}-\frac{1}{2}g_{\mu\nu}R\mp\frac{1}{l^2}g_{\mu\nu}=\frac{8\pi G_N}{c^4}T_{\mu\nu}}
\end{equation}
$\mp\frac{1}{l^2}$ è detta \textit{costante cosmologica}. Il termine $\mp\frac{1}{l^2}g_{\mu\nu}$ diventa importante solo quando il raggio di curvatura diventa paragonabile con $l$.


\section{Azione gravitazionale (di Einstein-Hilbert)}
Vediamo ora come costruire l'azione gravitazionale. Non possiamo avere\footnote{La motivazione per cui non possiamo avere uno scalare con $g$ e $\p g$ è che $g$ è un tensore con due indici, pertanto ha due componenti che trasformano da un sistema di riferimento ad un altro. $\p g$ è un tensore con almento un indice (due si possono contrarre), quindi una componente trasforma. Mentre $\p\p g$, se gli indici sono a due a due uguali, si possono cotrarre, come in $R$, e abbiamo uno scalare.} uno scalare con solo $g$ e $\p g$. Il primo scalare non banale è la curvatura scalare $R$ che però contiene anche termini $\p\p g$. Possiamo però dimostrare che i termini contenenti $\p\p g$ si riscrivono come un termine di bordo.
\[\underaccent{\tilde}{S}=\int\sqrt{-g}\dd{\Omega}R\qquad\parbox{10em}{(l'azione sarà $\S=\alpha\underaccent{\tilde}{S}$ con $\alpha$ da determinare)}\]
\[\sqrt{-g}R=\sqrt{-g}g^{\nu\gamma}\tensor{R}{^\mu_{\nu\mu\gamma}}=\sqrt{-g}g^{\nu\gamma}\qty\Big(\underbrace{\p_\mu\Gamma^\mu_{\nu\gamma}-\p_\gamma\Gamma^\mu_{\mu\nu}}_{(1)}+\Gamma^\mu_{\lambda\mu}\Gamma^\lambda_{\gamma\nu}-\Gamma^\mu_{\lambda\gamma}\Gamma^\lambda_{\mu\nu})\]
$(1)$: questi contengono $\p\p g$, quindi li riscriviamo come segue
\[\p_\mu\qty(\sqrt{-g}g^{\nu\gamma}\Gamma^\mu_{\nu\gamma})\underbrace{-\p_\mu\qty(\sqrt{-g}g^{\nu\gamma})\Gamma^\mu_{\nu\gamma}}_{(2)}-\p_\gamma\qty(\sqrt{-g}g^{\nu\gamma}\Gamma^\mu_{\mu\nu})\underbrace{+\p_\gamma\qty(\sqrt{-g}g^{\nu\gamma})\Gamma^\mu_{\mu\nu}}_{(2)}\]
$(2)$: questi contribuiscono all'equazione del moto e dipendono solo da $g$ e da $\p g$. Quindi l'azione è della forma
\[\int\sqrt{-g}\dd{\Omega}R=\int\sqrt{-g}\dd{\Omega}G(g,\p g)+\text{bordo}\]
dove
\[G(g,\p g)=\frac{1}{\sqrt{-g}}\qty\Big(-\p_\mu\qty(\sqrt{-g}g^{\nu\gamma})\Gamma^\mu_{\nu\gamma}+\p_\gamma\qty(\sqrt{-g}g^{\nu\gamma})\Gamma^\mu_{\mu\nu})+g^{\nu\gamma}\qty(\Gamma^\mu_{\lambda\mu}\Gamma^\lambda_{\gamma\nu}-\Gamma^\mu_{\lambda\gamma}\Gamma^\lambda_{\mu\nu})\]
Questo ci assicura che dalla variazione otterremo equazioni di secondo grado nelle derivate.

Facciamo ora la variazione di $\underaccent{\tilde}{S}$
\[\var{\St}=\int\dd{\Omega}\qty(\var{\sqrt{-g}}R+\sqrt{-g}\var{R})\]
\[\begin{split}&\var{\sqrt{-g}}=-\frac{1}{2}\frac{1}{\sqrt{-g}}\var{g}=\frac{1}{2}\sqrt{-g}g^{\mu\nu}\var{g_{\mu\nu}}=-\frac{1}{2}\sqrt{-g}g_{\mu\nu}\var{g^{\mu\nu}}\\&\var{R}=\var{g^{\mu\nu}}R_{\mu\nu}+g^{\mu\nu}\var{R_{\mu\nu}}\end{split}\]
\footnote{dove abbiamo usato $g_{\mu\nu}g^{\mu\nu}=1\implies g_{\mu\nu}\var{g^{\mu\nu}}+g^{\mu\nu}\var{g_{\mu\nu}}=0$} Quindi
\[\begin{split}\var{\St}&=\int\dd{\Omega}\qty(-\frac{1}{2}\sqrt{-g}g_{\mu\nu}\var{g^{\mu\nu}}R+\sqrt{-g}\var{g^{\mu\nu}}R_{\mu\nu}+\sqrt{-g}g^{\mu\nu}\var{R_{\mu\nu}})=\\&=\int\sqrt{-g}\dd{\Omega}\underbrace{\qty(R_{\mu\nu}-\frac{1}{2}g_{\mu\nu}R)}_{G_{\mu\nu}}\var{g^{\mu\nu}}+\int\sqrt{-g}\dd{\Omega}g^{\mu\nu}\var{R_{\mu\nu}}\end{split}\]
Per calcolare $\var{R_{\mu\nu}}$ è conveniente andare nel sistema localmente inerziale (dove $\p g=0\implies\Gamma=0$).
\[\var{R_{\mu\nu}}=\p_\lambda\var{\Gamma^\lambda_{\mu\nu}-\p_\nu\var{\Gamma^\lambda_{\lambda\mu}}}\]
quindi
\[\begin{split}&g^{\mu\nu}\var{R_{\mu\nu}}=g^{\mu\nu}\p_\lambda\var{\Gamma^\lambda_{\mu\nu}}-g^{\mu\lambda}\p_\lambda\var{\Gamma^\nu_{\nu\mu}}=\\&=\p_\lambda\qty(g^{\mu\nu}\var{\Gamma^\lambda_{\mu\nu}}-g^{\mu\lambda}\var{\Gamma^\nu_{\nu\mu}})=\hspace{40pt}(\p g=0)\\&=\p_\lambda V^\lambda\hspace{25pt}\text{dove }V^\lambda=g^{\mu\nu}\var{\Gamma^\lambda_{\mu\nu}}-g^{\mu\lambda}\var{\Gamma^\nu_{\nu\mu}}\end{split}\]
quindi in un sistema arbitrario (usando la \ref{6.3})
\[g^{\mu\nu}\var{R_{\mu\nu}}=D_\lambda V^\lambda=\frac{1}{\sqrt{-g}}\p_\lambda\qty(\sqrt{-g}V^\lambda)\]
nota che $V^\lambda$ è un vettore perché $\var{\Gamma^\mu_{\nu\rho}}$ è un tensore. La differenza di due connessioni diverse è un tensore $(1,2)$ perché i termini di trasformazione $\pdv{x'^\mu}{x^\lambda}\pdv{x^\lambda}{x'^\nu}{x'^\rho}$ si cancellano. Quindi
\[\var{\St}=\int\sqrt{-g}\dd{\Omega}G_{\mu\nu}\var{g^{\mu\nu}}+\underbrace{\int\dd{\Omega}\p_\lambda\qty(\sqrt{-g}V^\lambda)}_{\text{termine di bordo}}\]
Abbiamo quindi ottenuto il tensore di Einstein $G_{\mu\nu}$, cioè un pezzo delle equazioni che vogliamo riottenere con l'azione. Dobbiamo ora vedere cosa produce $\fdv{g^{\mu\nu}}$ nella parte di azione della materia.


\subsection{Campo scalare libero}
Prendiamo il caso di un campo scalare libero (massivo)
\[\S=\int\sqrt{-g}\dd{\Omega}\qty(\frac{1}{2}g^{\mu\nu}\p_\mu\phi\p_\nu\phi-\frac{m^2}{2}\phi^2)\]
\[\var{\S}=\int\sqrt{-g}\dd{\Omega}\qty(\frac{1}{2}\var{g^{\mu\nu}}\p_\mu\phi\p_\nu\phi-\frac{1}{2}g_{\mu\nu}\var{g^{\mu\nu}}\qty(\frac{1}{2}g^{\gamma\tau}\p_\gamma\phi\p_\tau\phi-\frac{m^2}{2}\phi^2))\]
avevamo visto che
\[\frac{T_{\mu\nu}}{c}=\p_\mu\phi\p_\nu\phi-g_{\mu\nu}\qty(\frac{1}{2}g^{\gamma\tau}\p_\gamma\phi\p_\tau\phi-\frac{m^2}{2}\phi^2)\]
quindi
\[\boxed{\var{\S}=\int\sqrt{-g}\dd{\Omega}\frac{1}{2}\frac{T_{\mu\nu}}{c}\var{g^{\mu\nu}}}\]

\subsubsection{Campo elettromagnetico}
Prendiamo il caso del campo elettromagnetico
\[\S_\text{em}=-\frac{1}{16\pi c}\int\sqrt{-g}\dd{\Omega}g^{\mu\nu}g^{\rho\tau}F_{\mu\rho}F_{\nu\tau}\]
\[\var{\S}_\text{em}=-\frac{1}{16\pi c}\int\sqrt{-g}\dd{\Omega}\qty(2\var{g^{\mu\nu}}g^{\rho\tau}F_{\mu\rho}F_{\nu\tau}-\frac{1}{2}g_{\mu\nu}\var{g^{\mu\nu}}\qty(F^{\rho\gamma}F_{\rho\gamma}))\]
usando
\[T_{\mu\nu}=-\frac{1}{4\pi}F_{\mu\rho}\tensor{F}{_\nu^\rho}+\frac{1}{16\pi}g_{\mu\nu}F_{\rho\gamma}F^{\rho\gamma}\]
abbiamo
\[\var{\S}_\text{em}=\int\sqrt{-g}\dd{\Omega}\frac{1}{2}\frac{T_{\mu\nu}}{c}\var{g^{\mu\nu}}\]
proprio come sopra per $\phi$. Vedremo che questa formula vale in generale.


\subsubsection{Campo gravitazionale}
Per il campo gravitazionale abbiamo quindi
\[\S=\alpha\St+\S_\text{mat}=\alpha\int\sqrt{-g}\dd{\Omega}R+\S_\text{mat}\]
\[\var{\S}=\int\sqrt{-g}\dd{\Omega}\qty(\alpha G_{\mu\nu}+\frac{1}{2}\frac{T_{\mu\nu}}{c})\var{g^{\mu\nu}}\]
quindi per riottenere le equazioni di Einstein dobbiamo porre l'integrando uguale a 0 e quindi scegliere $-\frac{1}{2c\alpha}=\frac{8\pi G_N}{c^4}\implies\alpha=-\frac{c^3}{16\pi G_N}$. 

L'\textit{azione di Einstein-Hilbert} è quindi
\begin{equation}
	\boxed{\S_\text{EH}=-\frac{c^3}{16\pi G_N}\int\sqrt{-g}\dd{\Omega}R}
\end{equation}
Aggiungendo una costante $2\Lambda$
\[\S=-\frac{c^3}{16\pi G_N}\int\sqrt{-g}\dd{\Omega}\qty\big(R+2\Lambda)+\S_\text{mat}\]
otteniamo
\[G_{\mu\nu}-\Lambda g_{\mu\nu}=\frac{8\pi G_N}{c^4}T_{\mu\nu}\]
cioè le equazioni modificate con la costante cosmologica $\Lambda=\pm1/l^2$. Possiamo anche interpretarla come
\[G_{\mu\nu}=\frac{8\pi G_N}{c^4}\qty(T_{\mu\nu}+g_{\mu\nu}\frac{\Lambda c^4}{8\pi G_N})\]
quindi come un particolare tipo di materia con
\[T_{\mu\nu}^\text{cc}=g_{\mu\nu}\frac{\Lambda c^4}{8\pi G_N}\]
un potenziale costante ha proprio questo tipo di tensore energia-impulso. Un termine costante, cioè un'\textit{energia di vuoto}, non ha effetti fisici in relatività ristretta, questo perché non influisce sulle equazioni del moto. In relatività generale però si accoppia alla metrica tramite la $\sqrt{-g}$ nella forma volume.

Abbiamo verificato, almeno in alcuni casi, che data $\S_\text{mat}=\int\sqrt{-g}\dd{\Omega}\frac{\l}{c}$, con $\l$ densità lagrangiana, il tensore energia-impulso è
\begin{equation}
	\boxed{T_{\mu\nu}=\frac{2}{\sqrt{-g}}\qty(\fdv{\qty(\sqrt{-g}\l)}{g^{\mu\nu}}-\p_\lambda\fdv{\qty(\sqrt{-g}\l)}{\p_\lambda g^{\mu\nu}})}
\end{equation}
Questo metodo per ottenere $T_{\mu\nu}$ funziona sempre, inoltre fornisce automaticamente un oggetto simmetrico. Possiamo dimostrare che $D_\mu T^{\mu\nu}=0$ discende direttamente dalla invarianza dell'azione sotto \textit{diffeomorfismi}. Per questo basta che $\l$ sia uno scalare, funzione dei campi di materia e della metrica, senza dipendenza esplicita dal punto $x^\mu$ dello spazio-tempo.


\section{Cambio di coordinate}
\subsection{Vettori di Killing $\xi$}
I diffeomorfismi che vogliamo studiare sono generali cambi di coordinate, però interpretati come "trasformazioni attive". Prendiamo un cambio di coordinate $x^\mu\ra x'^\mu(x^\rho)$. La metrica trasforma come
\[g'_{\mu\nu}\qty(\xrp)=\pdv{x^\lambda}{x'^\mu}\pdv{x^\tau}{x'^\nu}g_{\lambda\tau}(x^\rho)\]
A livello infinitesimo possiamo scrivere
\[x'^\mu\simeq x^\mu+\xi^\mu(x^\rho)\]
dove $\xi^\mu(x^\rho)$ è un certo campo vettoriale. Abbiamo quindi
\[\pdv{x'^\mu}{x^\nu}\simeq\tensor{\delta}{^\mu_\nu}+\pdv{\xi^\mu}{x^\nu}\qquad\text{e}\qquad\pdv{x^\nu}{x'^\mu}\simeq\tensor{\delta}{^\nu_\mu}-\pdv{\xi^\nu}{x^\mu}\]
quindi la metrica nel punto $x'^\mu$ è
\[g'_{\mu\nu}(x^\rho+\xi^\rho)\simeq g_{\mu\nu}(x^\rho)-g_{\mu\lambda}(x^\rho)\p_\nu\xi^\lambda-g_{\lambda\nu}(x^\rho)\p_\mu\xi^\lambda(x^\rho)\]
Quando facciamo un diffeomorfismo, cioè una trasformazione "attiva", ci interessa confrontare i campi nello "stesso" punto ($g'(x^\rho+\xi^\rho)\simeq g'(x^\rho)+\p_\lambda g'_{\mu\nu}\xi^\lambda$).
\[g'_{\mu\nu}\simeq g_{\mu\nu}-g_{\mu\lambda}\p_\nu\xi^\lambda-g_{\lambda\nu}\p_\mu\xi^\lambda-\p_\lambda g_{\mu\nu}\xi^\lambda\]
Una metrica è detta \textit{invariante} sotto il diffeomorfismo definito da $\xi^\mu$ se $g'_{\mu\nu}=g_{\mu\nu}$. In questo caso $\xi^\mu$ è chiamato \textit{vettore di Killing}.

Nota che
\[\begin{split}D_\mu\xi_\nu+D_\nu\xi_\mu&=\p_\mu\xi_\nu+\p_\nu\xi_\mu-2\Gamma^\lambda_{\mu\nu}\xi_\lambda=\\&\resizebox{.83\hsize}{!}{$=g_{\nu\lambda}\p_\mu\xi^\lambda+\p_\mu g_{\nu\lambda}\xi^\lambda+g_{\mu\lambda}\p_\nu\xi^\lambda+\p_\nu g_{\mu\nu}\xi^\lambda-g^{\lambda\gamma}\qty(\p_\mu g_{\gamma\nu}+\p_\nu g_{\mu\gamma}-\p_\gamma g_{\mu\nu})g_{\lambda\tau}\xi^\tau=$}\\&=g_{\nu\lambda}\p_\mu\xi^\lambda+g_{\mu\lambda}\p_\nu\xi^\lambda+\p_\lambda g_{\mu\nu}\xi^\lambda\end{split}\]
quindi
\begin{equation}\label{9.2}
	\boxed{g'_{\mu\nu}=g_{\mu\nu}-D_\mu\xi_\nu-D_\nu\xi_\mu}
\end{equation}
L'equazione di Killing $D_\mu\xi_\nu+D_\nu\xi_\mu=0$ esprime l'invarianza della metrica sotto il diffeomorfismo generato infinitesimamente da $\xi^\mu$.

In generale abbiamo quindi
\[\var{g^{\mu\nu}}=D^\mu\xi^\nu+D^\nu\xi^\mu\qquad\text{e}\qquad\var{g_{\mu\nu}}=-D_\mu\xi_\nu-D_\nu\xi_\mu\]


\subsection{Esempi}
\subsubsection{Variazione di $\S_\text{mat}$}
Ora facciamo la variazione di $\S_\text{mat}$ sotto diffeomorfismo
\[\var{\S_\text{mat}}=\fdv{\S_\text{mat}}{g^{\mu\nu}}\var{g^{\mu\nu}}+\fdv{\S_\text{mat}}{\phi}\var{\phi}\]
se calcoliamo il risultato su soluzioni delle equazioni del moto abbiamo $\fdv{\S_\text{mat}}{\phi}=0$. Quindi conta solo la variazione rispetto alla metrica. Abbiamo quindi
\[\var{\S_\text{mat}}=\int\sqrt{-g}\dd{\Omega}\frac{T_{\mu\nu}}{2c}\var{g^{\mu\nu}}\]
\[\begin{split}\var{\S_\text{mat}}&=\frac{1}{c}\int\sqrt{-g}\dd{\Omega}T_{\mu\nu}D^\mu\xi^\nu=\\&=\frac{1}{c}\int\sqrt{-g}\dd{\Omega}\qty\big(D^\mu\qty(T_{\mu\nu}\xi^\nu))-D^\mu T_{\mu\nu}\xi^\nu=\\&=\underbrace{\frac{1}{c}\int\dd{\Omega}\p^\mu\qty(\sqrt{-g}T_{\mu\nu}\xi^\nu)}_{(3)}-\frac{1}{2}\int\sqrt{-g}\dd{\Omega}D^\mu T_{\mu\nu}\xi^\nu\end{split}\]
$(3)$: questo è un termine di bordo ed è $0$ se prendiamo $\xi^\mu\ra0$ all'infinito.

Quindi $\var{\S_\text{mat}}=0\ \forall\xi^\mu \implies D^\mu T_{\mu\nu}=0$. $D^\mu T_{\mu\nu}=0$ è quindi una conseguenza dell'invarianza di $\S_\text{mat}$ sotto diffeormorfismi (sempre se calcolata su soluzioni delle equazioni del moto).


\subsubsection{$T_{\mu\nu}$ per un fluido di particelle}
Vediamo un altro esempio in cui possiamo verificare il metodo appena discusso per ottenere $T_{\mu\nu}$. Consideriamo un fluido di particelle libere $T^{\mu\nu}=mc^2N^\mu u^\nu$. Per una singola particella $\S_\text{mat}=-mc\int\sqrt{g_{\mu\nu}\dd{x^\mu}\dd{x^\nu}}$. Facciamo la variazione rispetto alla metrica
\[\var{\S_\text{mat}}=-mc\int\frac{1}{2}\frac{\var{g_{\mu\nu}}\dd{x^\mu}\dd{x^\nu}}{\sqrt{g_{\mu\nu}\dd{x^\mu}\dd{x^\nu}}}=-\int\frac{mc}{2}u^\mu u^\nu\var{g_{\mu\nu}}\dd{s}\]
\[\var{\S_\text{mat}}=\int\sqrt{-g}\dd{\Omega}\frac{T_{\mu\nu}}{2c}\var{g^{\mu\nu}}=-\int\sqrt{-g}\dd{\Omega}\frac{T^{\mu\nu}}{2c}\var{g_{\mu\nu}}\]
da cui otteniamo
\[T^{\mu\nu}=mc^2N^\mu u^\nu\]

Torniamo a discutere l'azione scritta in termini di $G(g,\p g)$. Abbiamo visto che
\[G(g,\p g) = \underbrace{\frac{1}{\sqrt{-g}}\qty\Big(-\p_\mu\qty(\sqrt{-g}g^{\mu\nu})\Gamma^\mu_{\nu\gamma}+\p_\gamma\qty(\sqrt{-g}g^{\nu\gamma})\Gamma^\mu_{\mu\nu})}_{(4)}+g^{\nu\gamma}\qty(\Gamma^\mu_{\lambda\mu}\Gamma^\lambda_{\gamma\nu}-\Gamma^\mu_{\lambda\gamma}\Gamma^\lambda_{\mu\nu})\]
$(4)$: riscriviamo questi nel seguente modo:
\[+\frac{1}{2}g_{\lambda\tau}\p_\mu g^{\lambda\tau}g^{\nu\gamma}\Gamma^\mu_{\nu\gamma}-\p_\mu g^{\nu\gamma}\Gamma^\mu_{\nu\gamma}-g^{\rho\gamma}\Gamma^\nu_{\rho\gamma}\Gamma^\mu_{\mu\nu}\]
usiamo $\p_\mu g^{\lambda\tau}=-\Gamma^\lambda_{\mu\rho}g^{\rho\tau}-\Gamma^\lambda_{\mu\rho}g^{\lambda\rho}$
\[-\frac{1}{2}\qty(\delta^\rho_\lambda\Gamma^\lambda_{\mu\rho}+\delta^\rho_\tau\Gamma^\tau_{\mu\rho})g^{\nu\gamma}\Gamma^\mu_{\nu\gamma}+\qty(\Gamma^\nu_{\mu\rho}g^{\rho\gamma}+\Gamma^\gamma_{\mu\rho}g^{\rho\nu})\Gamma^\mu_{\nu\gamma}-g^{\rho\gamma}\Gamma^\nu_{\rho\gamma}\Gamma^\mu_{\mu\nu}=2g^{\nu\gamma}\qty(\Gamma^\mu_{\lambda\gamma}\Gamma^\lambda_{\mu\nu}-\Gamma^\mu_{\lambda\mu}\Gamma^\lambda_{\gamma\nu})\]
quindi
\[G(g,\p g)=g^{\nu\gamma}\qty(\Gamma^\mu_{\lambda\gamma}\Gamma^\lambda_{\mu\nu}-\Gamma^\mu_{\lambda\mu}\Gamma^\lambda_{\gamma\nu})\]



